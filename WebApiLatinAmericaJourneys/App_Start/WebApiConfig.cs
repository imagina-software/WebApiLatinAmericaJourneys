﻿using System.Web.Http;
using System.Web.Http.Cors;
using WebApiLatinAmericaJourneys.Controllers;
using WebApiLatinAmericaJourneys.Utility;

namespace WebApiLatinAmericaJourneys
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Configuración de rutas y servicios de API

            // Web API configuration and services
            //var cors = new EnableCorsAttribute("http://159.65.107.241,http://localhost:4200", "*", "*");
            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            // Web API configuration and services  
            //config.Filters.Add(new RequireHttpsAttribute());
        }
    }
}
