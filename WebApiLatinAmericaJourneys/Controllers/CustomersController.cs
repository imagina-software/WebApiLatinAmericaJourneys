﻿using System.Linq;
using System.Web.Http;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys;
using WebApiLatinAmericaJourneys.Repository.Wallet;
using WebApiLatinAmericaJourneys.Repository.Data;
using System.Net.Http;
using System.Net;
using System;
using System.Collections.Generic;
using System.Text;
using System.Web.Configuration;
using ws = WebApiLatinAmericaJourneys.Ws_SendGrid;
using System.Security.Cryptography;
using System.Web.UI.WebControls;
using WebApiLatinAmericaJourneys.Utility;
using WebApiLatinAmericaJourneys.Models.Visa;
using WebApiLatinAmericaJourneys.Repository.Visa;
using System.Web.Http.Cors;
using System.Configuration;
using System.Threading.Tasks;

namespace WebApiLatinAmericaJourneys.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [Authorize]
    [RoutePrefix("api/customers")]
    public class CustomersController : ApiController
    {

        FichaPropuestaAccess objPropuesta = new FichaPropuestaAccess();
        Servicio objServicio = new Servicio();

        [HttpPost]
        public IHttpActionResult GetId(int id)
        {
            var customerFake = "customer-fake";
            return Ok(customerFake);
        }

        [HttpPost]
        [Route("demo")]
        public IHttpActionResult GetAll()
        {
         
            var customersFake = new string[] { "customer-1", "customer-2", "customer-3", "customer-4" };
            return Ok(customersFake);
        }


        [HttpPost]
        [Route("dropbox")]
        public IHttpActionResult Dropbox()
        {

            WebApiLatinAmericaJourneys.Utility.Dropbox objDropox = new WebApiLatinAmericaJourneys.Utility.Dropbox();
            objDropox.Conexion();

            var customersFake = new string[] { "customer-1", "customer-2", "customer-3", "customer-4" };
            return Ok(customersFake);
        }

        [HttpPost]
        [Route("login")]
        public IHttpActionResult LoginCliente(ClienteRequest cli) {

            ClienteResponse objClienteRS = new ClienteResponse();

            LoginAccess objLogin = new LoginAccess();
            var lstCliente = objLogin.LeerCliente(cli.emailCliente, cli.passwordCliente);

            if (lstCliente.Count() > 0)
            {
                objClienteRS.loginSuccess = true;
                objClienteRS.codCliente = lstCliente.FirstOrDefault().codCliente;
                objClienteRS.emailCliente = lstCliente.FirstOrDefault().emailCliente.Trim();
                objClienteRS.nomCliente = lstCliente.FirstOrDefault().nomCliente;
                objClienteRS.apePaterno = lstCliente.FirstOrDefault().apePaterno;
                objClienteRS.apeMaterno = lstCliente.FirstOrDefault().apeMaterno;
                objClienteRS.tipoIdioma = lstCliente.FirstOrDefault().tipoIdioma;
				objClienteRS.video_url = lstCliente.FirstOrDefault().video_url;
			}
            else
            {
                objClienteRS.loginSuccess = false;
                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No se encontro el cliente.")
                };
                throw new HttpResponseException(message);
            }

            return Ok(objClienteRS);

        }

        /*************************************************************************************************************************/
        /*************************************************************************************************************************/
        #region  INICIO DEL PROCESO DE OBTENER ITINERARIO DE VIAJE
       
        [HttpPost]
        [Route("GetPropuesta")]
        public IHttpActionResult GetPropuesta(PropuestaRequest Pro)
        {
            LPropuesta objPropuesta = new LPropuesta();
            var lstPropuesta = objPropuesta.LeerPropuesta(Convert.ToInt32(Pro.codCliente), Pro.zontaVenta);

            if (lstPropuesta.Count() > 0)
            {

                return Ok(lstPropuesta.ToList());

            }
            else
            {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No se encontro la Propuesta.")
                };
                throw new HttpResponseException(message);
            }



        }

        [HttpPost]
        [Route("GetItinerario")]
        public IHttpActionResult GetItinerario(EItinerario Iti)
        {
            LItinerario objItinerario = new LItinerario();
            var lstItinerario = objItinerario.LeerItinerario(Iti.nroPedido, Iti.nroPropuesta, Iti.nroVersion);

            if (lstItinerario.Count() > 0)
            {

                return Ok(lstItinerario.ToList());

            }
            else
            {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No se encontro el Itinerario.")
                };

                throw new HttpResponseException(message);
            }


        }

        [HttpPost]
        [Route("GetURL")]
        public IHttpActionResult GetURL(ClienteRequest cli)
        {
            string URL = Data.StrUrl;

            ClienteResponse objClienteRS = new ClienteResponse();

            LoginAccess objLogin = new LoginAccess();
            var lstCliente = objLogin.LeeIDCliente(Int32.Parse(cli.codigoCliente));

            if (lstCliente.Count() > 0)
            {

                objClienteRS.idCliente = lstCliente.FirstOrDefault().idCliente;
                URL = URL + "/" + objClienteRS.idCliente;
            }
            else
            {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No se encontro el IDcliente.")
                };
                throw new HttpResponseException(message);
            }

            return Ok(URL);

        }

        [HttpPost]
        [Route("GetImageTour")]
        public IHttpActionResult GetImageTour(PlantillaTourRequest Pla)
        {


            PlantillaTourResponse objPlantillaTour = new PlantillaTourResponse();
            LoginAccess objPlantilla = new LoginAccess();

            var lstImagenTour = objPlantilla.LeeImageTour(Int32.Parse(Pla.nroPedido), Int32.Parse(Pla.nroPropuesta), Int32.Parse(Pla.nroVersion));

            if (lstImagenTour.Count() > 0)
            {

                return Ok(lstImagenTour.ToList());

            }
            else
            {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("No se encontro la Plantilla para el Tour.")
                };

                throw new HttpResponseException(message);
            }

        }

        // FIN DEL PROCESO DE OBTENER ITINERARIO DE VIAJE
        #endregion

        /*************************************************************************************************************************/
        /*************************************************************************************************************************/
        #region INICIO DEL PROCESO DE RECUPERACION DE CONTRASEÑA

        /*SRC_008*/
        [HttpPost]
        [Route("GetAcceso")]
        public async Task<IHttpActionResult> GetAccesoAsync(AccesoRequest acc)
        {
            //string Mensaje;
            ClienteResponse objClienteRS = new ClienteResponse();
            LoginAccess objAcceso = new LoginAccess();
            Email objEmail = new Email();
            LoginAccess objLogin = new LoginAccess();

            var lstCliente = objAcceso.LeerCorreo(acc.emailCliente);

            int longitud = 6;
            Guid miGuid = Guid.NewGuid();
            string token = Convert.ToBase64String(miGuid.ToByteArray());
            token = token.Replace("=", "").Replace("+", "").Replace("/", "");

            string codigoTemporal = token.Substring(0, longitud);

            if (lstCliente.Count() > 0)
            {

                objClienteRS.claveCliente = lstCliente.FirstOrDefault().claveCliente.Trim();

                acc.cuerpo = ConstantesWeb.STR_EMAIL_CUERPO_CAMBIO_CLAVE + codigoTemporal;

                var msjTmp = objLogin.ActualizarCodigoTemporal( codigoTemporal,ConstantesWeb.CHR_ESTADO_ACTIVO,acc.emailCliente);


                if (msjTmp.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
                {

                    //objEmail.EnviarCorreoSendGrid(ConstantesWeb.STR_NOMBRE_EMISOR,
                    //    ConstantesWeb.STR_CORREO_EMISOR,
                    //    acc.emailCliente, 
                    //    ConstantesWeb.STR_EMAIL_ASUNTO_CAMBIO_CLAVE,
                    //    acc.emailCliente, 
                    //    codigoTemporal,
                    //    lstCliente.FirstOrDefault().tipoIdioma.ToString(),
                    //    ConstantesWeb.STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE);

                    await objEmail.EnviarCorreoMailJetAsync(
                        ConstantesWeb.STR_NOMBRE_EMISOR,
                        ConstantesWeb.STR_CORREO_EMISOR,
                        acc.emailCliente,
                        ConstantesWeb.STR_EMAIL_ASUNTO_CAMBIO_CLAVE,
                        acc.emailCliente,
                        codigoTemporal,
                        lstCliente.FirstOrDefault().tipoIdioma.ToString(),
                        ConstantesWeb.STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE
                );

                    objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
                    objClienteRS.msg = "ok, se envió el correo satisfactoriamente";
                }
                else {
                    objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                    objClienteRS.msg = msjTmp;
                }          

                return Ok(objClienteRS);
            }
            else
            {
                objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                objClienteRS.msg = "Por favor ingrese el correo con el que hizo su suscripcion ";


                return Ok(objClienteRS);

            }
        }


        [HttpPost]
        [Route("ActualizarContrasena")]
        public IHttpActionResult ActualizarContrasena(ActualizarContrasenaRequest ContraseñaRQ)
        {
            LoginAccess objLogin = new LoginAccess();
            ActualizarContrasenaResponse objContraseñaRsp = new ActualizarContrasenaResponse();

            var msj = objLogin.ActualizarContraseña(ContraseñaRQ.CodCliente, ContraseñaRQ.ContrasenaNueva,ContraseñaRQ.CodigoTemporal);


            if (msj.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
            {
                objContraseñaRsp.status = "ok";
                return Ok(objContraseñaRsp);
            }
            else
            {
                objContraseñaRsp.status = "fail";
                return Ok(objContraseñaRsp);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("Verificar no se ha registrado el mensaje.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        // ------------FIN DEL PROCESO DE RECUPERACION DE CONTRASEÑA

        #endregion

        /*************************************************************************************************************************/
        /*************************************************************************************************************************/
        #region INICIO  PROYECTO FASE I DE ITINERARIO Y CALIFICACION DE VIAJE CON LAS NUEVAS ESPECIFICACIONES

        /*SRC_007*/
  //      [HttpPost]
  //      [Route("GetPropuestaViaje")]
  //      public IHttpActionResult GetPropuestaViaje(PropuestaRequest Pro)
  //      {
  //          LPropuesta objPropuesta = new LPropuesta();
  //          LoginAccess objPlantilla = new LoginAccess();
  //          LItinerario objItinerario = new LItinerario();

  //          //string propuesta="";
  //          //string version="";l

  //          ItinerarioResponse objTourResponse = new ItinerarioResponse();

  //          var lstPropuesta = objPropuesta.LeerPropuestaViaje(Convert.ToInt32(Pro.codCliente),Pro.codEstado);


  //          //foreach (var item in lstPropuesta) {

  //          //    if (item.NroPrograma.Trim() == Pro.idpaquete) {

  //          //        propuesta = item.NroPropuesta;
  //          //        version = item.NroVersion;
                
  //          //    }
            
            
  //          //}

		//	if (lstPropuesta.Count() == 0)
		//	{
		//		objTourResponse.status = "fail";
		//	}
		//	else
		//	{
  //              var lstBanner = objPlantilla.LeeImage(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(Pro.idpaquete), Int32.Parse(lstPropuesta.FirstOrDefault().NroVersion));
  //              var lstItinerario = objItinerario.LeerItinerarioViaje(lstPropuesta.FirstOrDefault().NroPedido, Pro.idpaquete, lstPropuesta.FirstOrDefault().NroVersion);

  //              //var lstBanner = objPlantilla.LeeImage(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(Pro.idpaquete), Int32.Parse(version));
  //              //var lstItinerario = objItinerario.LeerItinerarioViaje(lstPropuesta.FirstOrDefault().NroPedido, Pro.idpaquete, version);


  //              if (lstPropuesta.Count() > 0)
		//		{
		//			objTourResponse.status = "ok";
		//			objTourResponse.main = lstPropuesta.ToList();
		//			objTourResponse.banner = lstBanner.ToList();
		//			objTourResponse.itinerario = lstItinerario.ToList();

		//			return Ok(objTourResponse);

		//		}
		//		else
		//		{
		//			objTourResponse.status = "fail";

		//			return Ok(objTourResponse);
		//			//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
		//			//{
		//			//    Content = new StringContent("No se encontro la Propuesta.")
		//			//};
		//			//throw new HttpResponseException(message);
		//		}



		//	}

		//	return Ok(objTourResponse);


		//	//var json = new JavaScriptSerializer().Serialize(objTourResponse);
		//	//string output =  JsonConvert.SerializeObject(objTourResponse);

		//	//string json = JsonConvert.SerializeObject(objTourResponse, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });


		//}

        /*SRC_011*/
        [HttpPost]
        [Route("CalificarViaje")]
        public IHttpActionResult CalificarViaje(CalificaRequest Cal)
        {
            LCalifica objCalifica = new LCalifica();
            List<CalificaResponse> lstToken = new List<CalificaResponse>();

            var Registro = objCalifica.RegistrarCalificaViaje(Int32.Parse(Cal.nroPedido), Int32.Parse(Cal.nroPropuesta), Int32.Parse(Cal.nroVersion), Int32.Parse(Cal.stars), Cal.comment);

            if (Registro.registro > 0)
            {

                CalificaResponse fToken = new CalificaResponse
                {
                    status = Registro.status
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());


            }
            else
            {

                CalificaResponse fToken = new CalificaResponse
                {
                    status = Registro.status
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent(Registro.status)
                //};

                //throw new HttpResponseException(message);
            }

        }

        /*SRC_014*/
        [HttpPost]
        [Route("RegistrarFoto")]
        public IHttpActionResult RegistrarFoto(FotoRegistroRequest Cal)
        {
            LFotoRegistro objCalifica = new LFotoRegistro();
            List<FotoRegistroResponse> lstToken = new List<FotoRegistroResponse>();

            var Registro = objCalifica.RegistrarFoto(Int32.Parse(Cal.nroPedido), Int32.Parse(Cal.nroPropuesta), Int32.Parse(Cal.nroVersion), Cal.fototitulo, Cal.fotocomment, Cal.fotocontent);

            if (Registro.status == "OK")
            {

                FotoRegistroResponse fToken = new FotoRegistroResponse
                {
                    status = Registro.status,
                    idfoto= Registro.idfoto
                    
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());


            }
            else
            {

                FotoRegistroResponse fToken = new FotoRegistroResponse
                {
                    status = "fail"
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent(Registro.status)
                //};

                //throw new HttpResponseException(message);
            }

        }

        /*SRC_013*/
        [HttpPost]
        [Route("GetFotoConsulta")]
        public IHttpActionResult GetFotoConsulta(FotoConsultaRequest Pro)
        {
           
            LFotoConsulta objItinerario = new LFotoConsulta();
            FotoConsultaResponse objTourResponse = new FotoConsultaResponse();

            var lstItinerario = objItinerario.LeeFotoConsulta(Int32.Parse(Pro.nroPedido), Int32.Parse(Pro.nroPropuesta), Int32.Parse(Pro.nroVersion));

            if (lstItinerario.Count() > 0)
            {
                objTourResponse.status = "ok";
                objTourResponse.Fotos = lstItinerario.ToList();
                return Ok(objTourResponse);
            }
            else
            {

                objTourResponse.status = "fail";
                objTourResponse.Fotos = lstItinerario.ToList();
                return Ok(objTourResponse);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro la lista de Fotos.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        // FIN DEL PROCESO DE ITINERARIO DE VIAJE CON LAS NUEVAS ESPECIFICACIONES
        #endregion

        //************************************************************************************************************************/


        //************************************************************************************************************************/
        //DIVISION PARA IDENTIFICAR  LOS METODOS PARA LA NUEVA FUNCIONALIDAD DE LA MEMBRESIA WALLET  27/04/2020 JLFA DESARROLLADOR
        //************************************************************************************************************************/
        #region INICIO DEL PROCESO DE MEMBRESIA WALLET 

        /*SRC_006*/
        [HttpPost]
        [Route("GetLogin")]
        public IHttpActionResult GetLogin(LoginWRequest Acc)
        {
            Llogin objLoginW = new Llogin();

            var lstLogin = objLoginW.LeerUsuario(Acc.uid, Acc.pass,Acc.usernameJWT,Acc.passwordJWT);
			

			List<LoginWResponse> lstLoginGG = new List<LoginWResponse>();


            if (lstLogin.Count() > 0)
            {
				var lVisto = objLoginW.ActualizarValor(Acc.uid);
				return Ok(lstLogin);
            }
            if (lstLogin.Count() == 0)
            {
				var lVisto = objLoginW.ActualizarValor(Acc.uid);
				return Ok(lstLogin);
            }
            else
            {

                LoginWResponse fLoginResponse = new LoginWResponse
                {
                    status="fail"
                };


                lstLoginGG.Add(item: fLoginResponse);
                return Ok(lstLogin.FirstOrDefault());


                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("Ingrese su usuario y clave correctos")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-109*/
        [HttpPost]
        [Route("GetWallet")]
        public IHttpActionResult GetWallet(WalletRequest Wall)
        {
            LWallet objWallet = new LWallet();
            WalletResponse respWallet = new WalletResponse();

            var lstWallet = objWallet.LeerWallet(Wall.user_id);
            List<WalletResponse> lstWalletGG = new List<WalletResponse>();




            if (lstWallet.Count() > 0)
            {
                return Ok(lstWallet);
            }
            else
            {



                WalletResponse fWallet = new WalletResponse
                {
                    status = "fail"
                };


                lstWalletGG.Add(item: fWallet);
                return Ok(lstWalletGG.FirstOrDefault());



                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontra los movimientos registrados.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-106*/
        [HttpPost]
        [Route("GetBeneficios")]
        public IHttpActionResult GetBeneficios(BeneficioRequest idioma)
        {
            LBeneficios objBeneficios = new LBeneficios();
            BeneficiosResponse objBeneficiosRes = new BeneficiosResponse();

            var lstBeneficios = objBeneficios.LeerBeneficios(idioma.idioma);
            var lstBeneficiosDetalle = objBeneficios.LeerBeneficiosDetalle(idioma.idioma);


            if (lstBeneficios.Count() > 0)
            {
                objBeneficiosRes.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
                objBeneficiosRes.beneficios = lstBeneficios.ToList();
                objBeneficiosRes.beneficiosdet = lstBeneficiosDetalle.ToList();


                return Ok(objBeneficiosRes);
            }
            else
            {


                objBeneficiosRes.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;

                return Ok(objBeneficiosRes);
                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontra Los Beneficios registrados.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-107*/
        [HttpPost]
        [Route("GetEncuesta")]
        public IHttpActionResult GetEncuesta(EncuestaRequest Enc)
        {
            LEncuesta objEncuesta = new LEncuesta();
            EncuestaResponse objMarcado = new EncuestaResponse();
            List<EncuestaResponse> lstEncuestaGG = new List<EncuestaResponse>();


            foreach (Marcado p in Enc.marcado.ToList())
            {
                objEncuesta.RegistrarEncuesta(Enc.user_id, p.pregunta, p.respuesta);

            }
			objEncuesta.ActualizarValor(Enc.user_id);
			var lstEncuestas = objEncuesta.LeerEncuesta(Enc.user_id,Enc.idioma);

            if (lstEncuestas.FirstOrDefault().status.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
            {

                objMarcado.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
                objMarcado.msg = lstEncuestas.FirstOrDefault().msg;
                objMarcado.msgIngles = lstEncuestas.FirstOrDefault().msgIngles;

				

				return Ok(objMarcado);
				
			}
            else
            {

                objMarcado.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                objMarcado.msg = lstEncuestas.FirstOrDefault().msg;
                objMarcado.msgIngles = lstEncuestas.FirstOrDefault().msgIngles;



                return Ok(objMarcado);

                //EncuestaResponse fEncuesta = new EncuestaResponse
                //{
                //    status = "fail"
                //};


                //lstEncuestaGG.Add(item: fEncuesta);
                //return Ok(lstEncuestaGG.FirstOrDefault());


                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("Lo sentimos vuelve a intentar mas adelante.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-107*/
        [HttpPost]
        [Route("GetEncuestaPreguntas")]
        public IHttpActionResult GetEncuestaPreguntas(VideoRequest idioma)
        {
            LEncuesta objEncuesta = new LEncuesta();
            EncuestaResponse objEncuestaResp = new EncuestaResponse();

            var lstPreguntas= objEncuesta.LeerEncuestaPregunta(idioma.idioma);

            IEnumerable<VideoEncuesta> lstVideoPregunta;

             lstVideoPregunta = objEncuesta.ObtenerVideo(idioma.idioma);

            if (lstPreguntas.Count() > 0)
            {
                objEncuestaResp.status = "ok";
                objEncuestaResp.videoEncuesta = lstVideoPregunta.FirstOrDefault().videoEncuesta.ToString();
				//objEncuestaResp.video = lstVideoPregunta.FirstOrDefault;
                objEncuestaResp.Preguntas = lstPreguntas.ToList();
              //  objEncuestaResp.VideoEncuesta = lstPreguntas.FirstOrDefault().VideEncuesta;


                return Ok(objEncuestaResp);
            }
            else
            {


                objEncuestaResp.status = "fail";


                return Ok(objEncuestaResp);
                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontra Los Beneficios registrados.")
                //};
                //throw new HttpResponseException(message);
            }

        }



        /*SRC-102*/
        [HttpPost]
        [Route("RegistrarNuevoViaje")]
        public IHttpActionResult RegistrarNuevoViaje(NuevoViajeRequest Nev)
        {
            LNuevoViaje objNuevoViaje = new LNuevoViaje();
            NuevoViajeResponse objNViaje = new NuevoViajeResponse();
            var CodViaje = objNuevoViaje.RegistrarNuevoViaje(Nev.user_id, Nev.rsp1, Nev.rsp2, Nev.rsp3, Nev.rsp4);
            //foreach (Incluye p in Nev.incluye.ToList())
            //{
            //	objNuevoViaje.RegistrarViajeros(Convert.ToString(CodViaje), p.nombre, p.apPaterno, p.apMaterno, p.edad);
            //}
            NuevoViajeResponse fencuesta = new NuevoViajeResponse();
            List<NuevoViajeResponse> lstNuevoViaje = new List<NuevoViajeResponse>();
            fencuesta.status = "OK";
            fencuesta.idsol = Convert.ToString(CodViaje);
            lstNuevoViaje.Add(item: fencuesta);

            if (lstNuevoViaje.Count() > 0)
            {
                return Ok(fencuesta);
            }
            else
            {



                fencuesta.status = "fail";

                return Ok(fencuesta);

            }

        }


		/*SRC-108*/
		[HttpPost]
        [Route("GetPropuestaViaje2")]
        public IHttpActionResult GetPropuestaViaje2(PropuestaRequest Pro)
        {
            LPropuesta objPropuesta = new LPropuesta();
    
            ItinerarioResponse2 objTourResponse = new ItinerarioResponse2();

            var codCliente = objPropuesta.ObtenerCodClientexAcceso(Pro.codUsuario);

            var lstPublicacion = objPropuesta.LeeUltimaPublicacion(codCliente);
            var lstProgramaGG = objPropuesta.ObtenerListadoPropuesta(lstPublicacion.FirstOrDefault().nroPedido, lstPublicacion.FirstOrDefault().flagIdioma);
      

            if (lstProgramaGG.Count() > 0)
            {
                objTourResponse.status = "ok";
                objTourResponse.viajes = lstProgramaGG.ToList();
        

                return Ok(objTourResponse);

            }
            else
            {


                objTourResponse.status = "ok";

                return Ok(objTourResponse);


            }



        }

        
        /*SRC-110*/
        [HttpPost]
        [Route("GetDestinos")]
        public IHttpActionResult GetDestinos()
        {
            LDestinos objDestino= new LDestinos();

            Destino2Response objDestinoRsp = new Destino2Response();


            var lstDestinos = objDestino.ObtenerDestinos();


            if (lstDestinos.Count() > 0)
            {

                objDestinoRsp.status = "ok";
                objDestinoRsp.destinos = lstDestinos.ToList();


                return Ok(objDestinoRsp);

            }
            else
            {

                objDestinoRsp.status = "fail";

                return Ok(objDestinoRsp);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro los destinos.")
                //};
                //throw new HttpResponseException(message);
            }



        }

        /*SRC-114*/
        [HttpPost]
        [Route("GetDestinosDetalle")]
        public IHttpActionResult GetDestinosDetalle(Destino2Request Des)
        {
            LDestinos objDestino = new LDestinos();



            DestinoDetalleResponse objDestinoResponse = new DestinoDetalleResponse();


            var lstDestinoDetalle= objDestino.ObtenerDestinosDetalle(Des.destino_id, Des.idioma, Des.codCliente);
            var lstPaises = objDestino.ObtenerPaises(Des.destino_id, Des.idioma);


            if (lstDestinoDetalle.Count() > 0)
            {
                objDestinoResponse.status = "ok";
                objDestinoResponse.info = lstPaises.Where(p=>p.destino_id.Equals(Des.destino_id)).ToList();
                objDestinoResponse.fotos = lstDestinoDetalle.ToList();


                return Ok(objDestinoResponse);

            }
            else
            {
                objDestinoResponse.status = "fail";
                return Ok(objDestinoResponse);


                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro el detalle de los destinos.")
                //};
                //throw new HttpResponseException(message);
            }



        }

        /*SRC-105*/
        [HttpPost]
        [Route("RegistrarMensajeChat")]
        public IHttpActionResult RegistrarMensajeChat(MensajeChatRequest Msj)
        {
            LMensajeChat objMensaje = new LMensajeChat();
            MensajeChatResponse objMensajeRsp = new MensajeChatResponse();

            var msj= objMensaje.RegistrarMensajeChat(Msj.codcliente,Msj.viaje,Msj.msg);

			objMensajeRsp.status = "ok";
			objMensajeRsp.fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss");


			if (msj.Equals(msj))
            {
                return Ok(objMensajeRsp);
            }
            else
            {
                objMensajeRsp.status = "fail";


                return Ok(objMensajeRsp);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("Verificar no se ha registrado el mensaje.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-105*/
        [HttpPost]
        [Route("GetMensajeChat")]
        public IHttpActionResult GetMensajeChat(MensajeChatRequest Msj)
        {
            LMensajeChat objMensaje = new LMensajeChat();
             MensajeChatResponse objMsjResponse = new MensajeChatResponse();

            var lstMensaje = objMensaje.ListarMensajeChat(Msj.codcliente, Convert.ToInt32(Msj.viaje));

            if (lstMensaje.Count() > 0)
            {
                objMsjResponse.status = "ok";
                objMsjResponse.mensajes = lstMensaje.ToList();
                return Ok(objMsjResponse);
            }
            else
            {

                objMsjResponse.status = "fail";
                //objMsjResponse.mensajes = "No se encontro los mensajes";
                return Ok(objMsjResponse);



                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro los mensajes.")
                //};
                //throw new HttpResponseException(message);
            }

        }



        /*SRC-115*/
        [HttpPost]
        [Route("RegistrarMensajeChatGeneral")]
        public IHttpActionResult RegistrarMensajeChatGeneral(MensajeChatRequest Msj)
        {
            LMensajeChat objMensaje = new LMensajeChat();
            MensajeChatResponse objMensajeRsp = new MensajeChatResponse();

            var msj = objMensaje.RegistrarMensajeChatGeneral(Msj.codcliente, Msj.msg);

            objMensajeRsp.status = "ok";

            if (msj.Equals(msj))
            {
                return Ok(objMensajeRsp);
            }
            else
            {
                objMensajeRsp.status = "fail";


                return Ok(objMensajeRsp);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("Verificar no se ha registrado el mensaje.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-115*/
        [HttpPost]
        [Route("GetMensajeChatGeneral")]
        public IHttpActionResult GetMensajeChatGeneral(MensajeChatRequest Msj)
        {
            LMensajeChat objMensaje = new LMensajeChat();
            MensajeChatResponse objMsjResponse = new MensajeChatResponse();

            var lstMensaje = objMensaje.ListarMensajeChatGeneral(Msj.codcliente);

            if (lstMensaje.Count() > 0)
            {
                objMsjResponse.status = "ok";
                objMsjResponse.mensajes = lstMensaje.ToList();
                return Ok(objMsjResponse);
            }
            else
            {

                objMsjResponse.status = "fail";
                //objMsjResponse.mensajes = "No se encontro los mensajes";
                return Ok(objMsjResponse);



                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro los mensajes.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC-111*/
        [HttpPost]
        [Route("GetVideo")]
        public IHttpActionResult GetVideo(VideoRequest Vid)
        {
            LVideo objVideo = new LVideo();
            VideoResponse objVideoRsp = new VideoResponse();

            var lstVideos = objVideo.ListarVideos(Convert.ToInt32(Vid.Id_usuario));

            if (lstVideos.Count() > 0)
            {
                objVideoRsp.status = "ok";
                objVideoRsp.Videos = lstVideos.ToList();
                return Ok(objVideoRsp);
            }
            else
            {
                objVideoRsp.status = "fail";
                return Ok(objVideoRsp);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro los videos.")
                //};
                //throw new HttpResponseException(message);
            }

        }

        /*SRC -024*/
        [HttpPost]
        [Route("RegistraCotizacion")]
        public IHttpActionResult RegistraCotizacion(CotizaRequest cot)
        {
            Cotiza objCotiza = new Cotiza();
            List<CotizaResponse> lstToken = new List<CotizaResponse>();

            var Registro = objCotiza.RegistrarCotizacion(cot.paquete_id, cot.nombres, cot.email, cot.telefono, cot.pais, cot.preferencias, cot.fecha, cot.adt, cot.chd);

            if (Registro.registro > 0)
            {

                CotizaResponse fToken = new CotizaResponse
                {
                    status = Registro.status
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());


            }
            else
            {


                CotizaResponse fToken = new CotizaResponse
                {
                    status = "fail"
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());
                //            var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //	Content = new StringContent(Registro.status)
                //            };

                //            throw new HttpResponseException(message);
            }

        }

        /*SRC -104*/
        [HttpPost]
        [Route("ListaMisDestinos")]
        public IHttpActionResult ListaMisDestinos(DestinosRequest Des)
        {

            LDestino objDestino = new LDestino();

            DestinoResponse objDestinoRes = new DestinoResponse();

            var lstDestino = objDestino.LeerMisDestino(Des.user);


            if (lstDestino.Count() > 0)
            {
                objDestinoRes.status = "ok";
                objDestinoRes.Destinos = lstDestino.ToList();

                return Ok(objDestinoRes);

            }
            else
            {

                objDestinoRes.status = "fail";
                return Ok(objDestinoRes);


                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //    Content = new StringContent("No se encontro el Destino.")
                //};

                //throw new HttpResponseException(message);
            }


        }


		/*SRC-015*/
		[HttpDelete]
		[Route("EliminarFoto")]
		public IHttpActionResult EliminarFoto(EliminaFotoRequest fot)
		{
					   
			EliminaFoto objEliminaFt = new EliminaFoto();

			List<EliminaFotoResponse> lstToken = new List<EliminaFotoResponse>();

			var lstRespuesta = objEliminaFt.EliminarFoto(fot.idfoto);
					   			 
			if (lstRespuesta.registro > 0)
			{

				EliminaFotoResponse fToken = new EliminaFotoResponse
				{
					status = lstRespuesta.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());				

			}
			
			else
			{

                EliminaFotoResponse fToken = new EliminaFotoResponse
                {
                    status = "fail"
                };

                lstToken.Add(item: fToken);

                return Ok(lstToken.FirstOrDefault());

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //	Content = new StringContent("No se pudo eliminar foto")
                //};
                //throw new HttpResponseException(message);
            }

		}



		/*SRC-021*/
		[HttpPost]
		[Route("GetExplora")]
		public IHttpActionResult GetExplora()
		{
			LDestinos objDestinoLat = new LDestinos();

			DestinoLatResponsive objDestinoRsp = new DestinoLatResponsive();

			var lstDestinos = objDestinoLat.ObtenerPaisesLat();


			if (lstDestinos.Count() > 0)
			{

				objDestinoRsp.status = "ok";
				objDestinoRsp.destinos = lstDestinos.ToList();


				return Ok(objDestinoRsp);

			}
			else
			{
				objDestinoRsp.status = "fail";
				return Ok(objDestinoRsp);

				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontro los destinos.")
				//};
				//throw new HttpResponseException(message);
			}



		}

		/*SRC -SRC_022*/
		[HttpPost]
		[Route("GetExploraDestinos")]
		public IHttpActionResult GetExploraDestinos(PaisDestinoRequest Id_destino)
		{

			LDestino objDestino = new LDestino();

			PaisDestinoResponse objPaisDestinoRes = new PaisDestinoResponse();

			var lstDestino = objDestino.ObtienePaisDestino(Id_destino.id_destino);


			if (lstDestino.Count() > 0)
			{
                objPaisDestinoRes.status = "ok";
                objPaisDestinoRes.paisDestinos = lstDestino.ToList();

                return Ok(objPaisDestinoRes);

			}
			else
			{
                objPaisDestinoRes.status = "fail";
                return Ok(objPaisDestinoRes);

                //var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                //{
                //	Content = new StringContent("No se encontro el Destino.")
                //};

                //throw new HttpResponseException(message);
            }


        }



		// FIN DEL  PROCESO DE MEMBRESIA WALLET 
		#endregion

		//************************************************************************************************************************/
		//************************************************************************************************************************/


		//************************************************************************************************************************/
		//DIVISION PARA IDENTIFICAR  LOS METODOS PARA LA NUEVA FUNCIONALIDAD DE COMENTARIOS
		//************************************************************************************************************************/

		#region COMENTARIOS

		/*SRC-037*/
		[HttpPost]
		[Route("GetComentario")]
		public IHttpActionResult GetComentario(ComentarioRequest Coment)
		{
			Lcomentario objComent= new Lcomentario();
			ComentarioResponse respComent = new ComentarioResponse();

			var lstComent = objComent.ObtieneComentario(Coment.id_usuario);

			//respComent.comentarios = lstComent.ToList();

			if (lstComent.Count() > 0)
			{
				//return Ok(respComent);
				respComent.status = "ok";
				respComent.comentarios = lstComent.ToList();

				return Ok(respComent);
			}
			else
			{

				respComent.status = "fail";
				return Ok(respComent);


				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontra.")
				//};
				//throw new HttpResponseException(message);
			}

		}


		/*SRC-037*/
		[HttpPost]
		[Route("RegistrarComentario")]
		public IHttpActionResult RegistrarComentario(ComentNewRequest Com)
		{
			Lcomentario objComent = new Lcomentario();
			List<ComentNewResponse> lstToken = new List<ComentNewResponse>();

			var Registro = objComent.RegistrarComentario(Com.comentario,Com.idUsuario );

			if (Registro.registro > 0)
			{

				ComentNewResponse fToken = new ComentNewResponse
				{
					status = Registro.status,
					fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
			};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				ComentNewResponse fToken = new ComentNewResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}




		#endregion


		[HttpPost]
		[Route("RegistrarCliente")]
		public async  Task<IHttpActionResult> RegistrarCliente(ClienteNewRequest Cli)
		{
			LClienteNew objCliente = new LClienteNew();
			//ClienteNewResponse objNViaje = new ClienteNewResponse();
			List<ClienteNewResponse> lstToken = new List<ClienteNewResponse>();
            Email objEmail = new Email();


            //var CodClienteNew = objCliente.RegistrarCliente(Cli.nombre,Cli.apetPat, Cli.apetMat, Cli.email, Cli.tipoIdioma, Cli.telefono, Cli.codPais);
            var Registro = objCliente.RegistrarCliente2(Cli.nombre, Cli.apetPat, Cli.apetMat, Cli.email, Cli.tipoIdioma,Cli.tipoDoc, Cli.NumDoc,Cli.CodPlan, Cli.Origen, Cli.CodigoCompartir, Cli.telefono, Cli.codPais);

            if (Cli.CodPlan.Equals("GOLACFREE")) {


                // Envio de correo al cliente con sus accesos

                //objEmail.EnviarCorreoSendGrid(
                //     ConstantesWeb.STR_NOMBRE_EMISOR,
                //     ConstantesWeb.STR_CORREO_EMISOR,
                //     Registro.correo,
                //     ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLAVE,
                //     Registro.correo,
                //     Registro.password,
                //     Cli.tipoIdioma.ToString(),
                //     ConstantesWeb.STR_TEMPLATE_ENVIO_ACCESOS);

                await objEmail.EnviarCorreoMailJetAsync(
                    ConstantesWeb.STR_NOMBRE_EMISOR,
                     ConstantesWeb.STR_CORREO_EMISOR,
                     Registro.correo,
                     ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLAVE,
                     Registro.correo,
                     Registro.password,
                     Cli.tipoIdioma.ToString(),
                     ConstantesWeb.STR_TEMPLATE_ENVIO_ACCESOS
                    );




                // ENVIO DE CORREO EJECUTIVA


                //objEmail.EnviarCorreoSendGrid(
                //    ConstantesWeb.STR_NOMBRE_EMISOR,
                //    ConstantesWeb.STR_CORREO_EMISOR,
                //    Registro.correoEjecutiva,
                //    ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLIENTE,
                //    Registro.codCliente,
                //    Registro.correo,
                //    Cli.tipoIdioma.ToString(),
                //    ConstantesWeb.STR_TEMPLATE_ENVIO_EJECUTIVA);

                await objEmail.EnviarCorreoMailJetAsync(
                    ConstantesWeb.STR_NOMBRE_EMISOR,
                    ConstantesWeb.STR_CORREO_EMISOR,
                    Registro.correoEjecutiva,
                    ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLIENTE,
                    Registro.codCliente,
                    Registro.correo,
                    Cli.tipoIdioma.ToString(),
                    ConstantesWeb.STR_TEMPLATE_ENVIO_EJECUTIVA
                    );


            }

			//ClienteNewResponse fcliente = new ClienteNewResponse();
			//List<ClienteNewResponse> lstNuevoCliente = new List<ClienteNewResponse>();
			//fcliente.status = "OK";
			//fcliente.codCliente = Convert.ToString(CodClienteNew);
			//lstNuevoCliente.Add(item: fcliente);
			if (Registro.status.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
			{

				ClienteNewResponse fToken = new ClienteNewResponse
				{

					status = ConstantesWeb.STR_ESTADO_SERVICIO_OK,
					msg_ES = Registro.msg_ES,
					msg_EN = Registro.msg_EN,
					codCliente = Registro.codCliente,
                    usuario = Registro.usuario,
                    password = Registro.password,
                    correo =Registro.correo,
                    urlWeb = ConfigurationManager.AppSettings["URL_APP_WEB"],
                    urlPlayStore = ConfigurationManager.AppSettings["URL_APP_ANDROID"],
                    urlAppStore = ConfigurationManager.AppSettings["URL_APP_IOS"]



                };

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				ClienteNewResponse fToken = new ClienteNewResponse
				{
					status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL,
					msg_EN = Registro.msg_EN,
					msg_ES = Registro.msg_ES,
					codCliente = Registro.codCliente,
                    usuario = Registro.usuario,
                    password = Registro.password,
                    correo = Registro.correo,
                    urlWeb = ConfigurationManager.AppSettings["URL_APP_WEB"],
                    urlPlayStore = ConfigurationManager.AppSettings["URL_APP_ANDROID"],
                    urlAppStore = ConfigurationManager.AppSettings["URL_APP_IOS"]

                };

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//    Content = new StringContent(Registro.status)
				//};

				//throw new HttpResponseException(message);
			}


			//if (lstNuevoCliente.Count() > 0)
			//{

			//	//EnviarCorreoSendGrid(Convert.ToString(CodClienteNew), "jlopez.j87@gmail.com", "jlopez.j87@gmail.com", "Prueba", "Mensaje de Prueba");

			//	return Ok(fcliente);

			//}
			//else
			//{

			//	fcliente.status = "fail";

			//	return Ok(fcliente);
			//}

		}


		/*SRC-033*/
		[HttpPost]
		[Route("ObtienePerfil")]
		public IHttpActionResult ObtienePerfil(PerfilRequest Perfil)
		{
			LPerfil objPerfil = new LPerfil();
			PerfilResponse respComent = new PerfilResponse();

			var lstPerfil = objPerfil.ObtienePerfil(Perfil.idCliente);

			//respComent.comentarios = lstComent.ToList();

			if (lstPerfil.Count() > 0)
			{
				//return Ok(respComent);
				respComent.status = "ok";
				respComent.perfil = lstPerfil.ToList();

				return Ok(respComent);
			}
			else
			{

				respComent.status = "fail";
				return Ok(respComent);


				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontra.")
				//};
				//throw new HttpResponseException(message);
			}

		}


		/*WEB*/
		[HttpPost]
		[Route("ObtienePaises")]
		public IHttpActionResult ObtienePaises()
		{
			LPaises objPais = new LPaises();
			PaisesResponse respPais = new PaisesResponse();

			var lstPerfil = objPais.ObtienePaises();

			//respComent.comentarios = lstComent.ToList();

			if (lstPerfil.Count() > 0)
			{
				//return Ok(respComent);
				respPais.status = "ok";
				respPais.paises = lstPerfil.ToList();

				return Ok(respPais);
			}
			else
			{

				respPais.status = "fail";
				return Ok(respPais);


				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontra.")
				//};
				//throw new HttpResponseException(message);
			}

		}


		/*****/
		[HttpPost]
		[Route("ActualizarPerfil")]
		public IHttpActionResult RegistrarPerfil(PerfilNewRequest Perf)


		{
			LPerfil objComent = new LPerfil();
			List<PerfilNewResponse> lstToken = new List<PerfilNewResponse>();

			var Registro = objComent.ActualizarPerfil(Perf.codCliente, Perf.nombres, Perf.apellidos, Perf.email, Perf.telefono);

			if (Registro.registro > 0)
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status,
					//fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}


		/*SRC-111*/
		[HttpPost]
		[Route("ObtienePlanes")]
		public IHttpActionResult ObtienePlanes(PlaneaRequest Planea)
		{
			LPlanea objPlanea = new LPlanea();
			PlaneaResponse respPlanea = new PlaneaResponse();

			var lstPlanea = objPlanea.ObtienePlanes(Planea.idCliente, Planea.codPais);

			//respComent.comentarios = lstComent.ToList();

			if (lstPlanea.Count() > 0)
			{
				//return Ok(respComent);
				respPlanea.status = "ok";
				respPlanea.planea = lstPlanea.ToList();

				return Ok(respPlanea);
			}
			else
			{

				respPlanea.status = "fail";
				return Ok(respPlanea);


				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontra.")
				//};
				//throw new HttpResponseException(message);
			}

		}
				

		/*WEB PASARELA*/
		[HttpPost]
		[Route("MisPlanes")]
		public IHttpActionResult MisPlanes()
		{
			LPlanes objPlanes = new LPlanes();
			PlanesResponse objEncuestaResp = new PlanesResponse();

			var lstPlanes = objPlanes.ObtienePlanesCli();

			if (lstPlanes.Count() > 0)
			{
				objEncuestaResp.status = "ok";
				objEncuestaResp.planes = lstPlanes.ToList();


				return Ok(objEncuestaResp);
			}
			else
			{

				objEncuestaResp.status = "fail";


				return Ok(objEncuestaResp);

			}

		}


        /*SRC_007*/
        [HttpPost]
        [Route("GetPropuestaViaje")]
        public IHttpActionResult GetPropuestaViaje(PropuestaRequest Pro)
        {
            LPropuesta objPropuesta = new LPropuesta();
            LoginAccess objPlantilla = new LoginAccess();
            LItinerario objItinerario = new LItinerario();
            List<EItinerario> ListServicios = new List<EItinerario>();
            EItinerario objServicio = new EItinerario();
            ItinerarioResponse objTourResponse = new ItinerarioResponse();
            List<ItinerarioViaje> ListItinerario = new List<ItinerarioViaje>();
			PropuestaPreciosAccess objPrecio = new PropuestaPreciosAccess();
			PropuestaPreciosAccess objResumen = new PropuestaPreciosAccess();

			

			var lstPropuesta = objPropuesta.LeerPropuestaViaje(Convert.ToInt32(Pro.codCliente), Pro.codEstado, Pro.idpaquete);



			if (lstPropuesta.Count() == 0)
            {
                objTourResponse.status = "fail";
            }
            else
            {
                var lstBanner = objPlantilla.LeeImage(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(Pro.idpaquete), Int32.Parse(lstPropuesta.FirstOrDefault().NroVersion));

				var lstPrecio = objPrecio.ObtienePrecios(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(lstPropuesta.FirstOrDefault().NroPropuesta));
				
				var lstItinerario = objItinerario.ObtenerListadoCabecera(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(lstPropuesta.FirstOrDefault().NroPropuesta), lstPropuesta.FirstOrDefault().NroVersion, lstPropuesta.FirstOrDefault().TipoIdioma);

				var lstResumen = objPrecio.ObtieneCaracteristicas(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(lstPropuesta.FirstOrDefault().NroPropuesta), lstPropuesta.FirstOrDefault().NroVersion, lstPropuesta.FirstOrDefault().TipoIdioma);



				var agrupacion = from p in lstItinerario group p by p.nroDia into grupo select grupo;


                if (lstPropuesta.Count() > 0)
                {
                    objTourResponse.status = "ok";

                    objTourResponse.main = lstPropuesta.ToList();
                    objTourResponse.banner = lstBanner.ToList();
					objTourResponse.precio = lstPrecio.ToList();
					objTourResponse.resumen = lstResumen.ToList();


					foreach (var item in agrupacion)
                    {
                        string nroDia = string.Empty;
                        string servDetAgrupado = string.Empty;
                        string desServicio = string.Empty;
                        string ciudad = string.Empty;
                        string horaServicio = string.Empty;

                        int i = 0;
                        int cantidad = agrupacion.Count();

                        //var ListaActividades = objItinerario.ObtenerListadoDetalle(item., pNroPropuesta, pFlagIdioma, fitinerario.nroDia);



                        var ListItinerarioGG = (from p in item
                                 where p.dia != string.Empty
                                 select new ItinerarioViaje()
                                 {
									 
                                     dia = p.dia,
                                     fchInicio = p.fchInicio,
                                     nroDia = p.nroDia,
									 desServicio = p.desServicio,
									 flagatencion = p.flagatencion,
									 Lugares = objItinerario.ObtenerListadoDetalleLugares(Int32.Parse(lstPropuesta.FirstOrDefault().NroPedido), Int32.Parse(lstPropuesta.FirstOrDefault().NroPropuesta), lstPropuesta.FirstOrDefault().TipoIdioma, p.nroDia, lstPropuesta.FirstOrDefault().NroVersion).ToList()

								 }).ToList();

                        objTourResponse.itinerario.AddRange(ListItinerarioGG);
						      

                    }


                    return Ok(objTourResponse);

                }
                else
                {
                    objTourResponse.status = "fail";

                    return Ok(objTourResponse);

                }



            }

            return Ok(objTourResponse);



        }



		[HttpPost]
		[Route("ObtienePodcats")]
		public IHttpActionResult ObtienePodcats(Destino2Request Podcats)
		{
			LPodcats objPodcats = new LPodcats();
			PodcastsResponse respComent = new PodcastsResponse();

			var lstPodcats = objPodcats.ObtienePodcats(Podcats.destino_id, Podcats.idioma);

			//respComent.comentarios = lstComent.ToList();

			if (lstPodcats.Count() > 0)
			{
				//return Ok(respComent);
				respComent.status = "ok";
				respComent.PodCasts = lstPodcats.ToList();

				return Ok(respComent);
			}
			else
			{

				respComent.status = "fail";
				return Ok(respComent);

			}

		}


		/*****/
		[HttpPost]
		[Route("ActualizarPlan")]
		public async Task<IHttpActionResult> ActualizaPlan(SuscripcionRequest Perf)
		{
			LPlanes objComent = new LPlanes();

			Email objEmail = new Email();


			List<PerfilNewResponse> lstToken = new List<PerfilNewResponse>();

			var Registro = objComent.ActualizarPlan(Perf.CodCliente, Perf.CodPlan);


			if (Registro.registro > 0)
			{


				await objEmail.EnviarCorreoMailJetAsync(
					ConstantesWeb.STR_NOMBRE_EMISOR,
					ConstantesWeb.STR_CORREO_EMISOR,
					Registro.correoEjecutiva,
					Registro.nomCliente,
					Registro.correoCliente,
					Perf.CodPlan,
					"E",
					ConstantesWeb.STR_TEMPLATE_CAMBIO_PLAN
				);



				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status,



				//fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
			};


				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}



		[HttpPost]
		[Route("WishList")]
		public IHttpActionResult WishList(WishListRequest Perf)
		{
			LPlanes objComent = new LPlanes();

			List<PerfilNewResponse> lstToken = new List<PerfilNewResponse>();

			var Registro = objComent.RegistrarPreferencia(Perf.CodCliente, Perf.Origen, Perf.ID, Perf.Estado, Perf.CodPais);

			if (Registro.registro > 0)
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status,
					//fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
				};


				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}


		[HttpPost]
		[Route("ObtieneWish")]
		public IHttpActionResult ObtieneWish(WishListRequest Wish)
		{
			LPlanes objWishList = new LPlanes();
			WishResponse respComent = new WishResponse();

			var lstWish = objWishList.ObtieneLista(Wish.CodCliente);
					

			if (lstWish.Count() > 0)
			{
				respComent.status = "ok";
				respComent.WishList = lstWish.ToList();

				return Ok(respComent);
			}
			else
			{

				respComent.status = "fail";
				return Ok(respComent);

			}

		}




		[HttpPost]
		[Route("CompatirFoto")]
		public IHttpActionResult CompatirFoto(WishListRequest Comprt)
		{
			EstandarAccess objComent = new EstandarAccess();

			List<PerfilNewResponse> lstToken = new List<PerfilNewResponse>();

			var Registro = objComent.RegistrarPublicacion(Comprt.CodCliente, Comprt.Origen);

			if (Registro.registro > 0)
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status,
					//fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
				};


				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				PerfilNewResponse fToken = new PerfilNewResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}


		/*****/
		[HttpPost]
		[Route("EnviarPromocion")]
		public async Task<IHttpActionResult> EnviarPromocion(PromocionesxCli ProC)
		{
			PromocionAccess objComent = new PromocionAccess();

			Email objEmail = new Email();


			List<PromocionesxCliResponse> lstToken = new List<PromocionesxCliResponse>();

			var Registro = objComent.InsertarPromociones(ProC.CodCliente, ProC.IdPromocion);


			if (Registro.registro > 0)
			{


				await objEmail.EnviarCorreoMailJetAsync(
					ConstantesWeb.STR_NOMBRE_EMISOR,
					ConstantesWeb.STR_CORREO_EMISOR,
					Registro.correoEjecutiva,
					Registro.nomCliente,
					Registro.correoCliente,
					Registro.nomTarifa,
					"E",
					ConstantesWeb.STR_TEMPLATE_TARIFA
				);



				PromocionesxCliResponse fToken = new PromocionesxCliResponse
				{
					status = Registro.status,



					//fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm")
				};


				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				PromocionesxCliResponse fToken = new PromocionesxCliResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}
		

		[HttpPost]
		[Route("ObtieneNotificaciones")]



		public IHttpActionResult ObtieneNotificaciones(NotificacionRequest Noti)
		{
			LNotificaciones objNotifaciones = new LNotificaciones();
			NotificacionResponse respNoti = new NotificacionResponse();

			var lstNoti = objNotifaciones.LeerNotificacion(Noti.CodCliente);

			if (lstNoti.Count() > 0)
			{
				//return Ok(respComent);
				respNoti.status = "ok";
				respNoti.notificaciones = lstNoti.ToList();

				return Ok(respNoti);
			}
			else
			{

				respNoti.status = "fail";
				return Ok(respNoti);



			}

		}


		[HttpPost]
		[Route("RegistrarVisitas")]
		public IHttpActionResult RegistrarVisitas(VisitasRequest Visitas)
		{
			LVisita objComent = new LVisita();
			List<VisitasResponse> lstToken = new List<VisitasResponse>();

			var Registro = objComent.RegistrarVisitas(Visitas.CodCliente, Visitas.IdModulo);

			if (Registro.registro > 0)
			{

				VisitasResponse fToken = new VisitasResponse
				{
					status = Registro.status,
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());


			}
			else
			{

				VisitasResponse fToken = new VisitasResponse
				{
					status = Registro.status
				};

				lstToken.Add(item: fToken);

				return Ok(lstToken.FirstOrDefault());

			}

		}

		[HttpPost]
		[Route("ObtieneDetalleHotel")]
		public IHttpActionResult ObtieneDetalleHotel(DetalleHotelRequest Pro)
		{

			LDetalleHoteles objHoteles = new LDetalleHoteles();
			DetalleHotelResponse objTourResponse = new DetalleHotelResponse();

			var lstDetalleHTL = objHoteles.LeerDetalleHotel(Int32.Parse(Pro.nroServicio));

			if (lstDetalleHTL.Count() > 0)
			{
				objTourResponse.status = "ok";
				objTourResponse.DetalleHotel = lstDetalleHTL.ToList();
				return Ok(objTourResponse);
			}
			else
			{

				objTourResponse.status = "fail";
				objTourResponse.DetalleHotel = lstDetalleHTL.ToList();
				return Ok(objTourResponse);


			}

		}

		[HttpPost]
		[Route("ObtieneDetalleHotel2")]
		public IHttpActionResult ObtieneDetalleHotel2(DetalleHotelRequest Pro)
		{

			LDetalleHoteles objHoteles = new LDetalleHoteles();
			DetalleHotelResponse objTourResponse = new DetalleHotelResponse();

			var lstDetalleHTL = objHoteles.LeerDetalleHotel2(Int32.Parse(Pro.nroServicio), Pro.idioma);

			if (lstDetalleHTL.Count() > 0)
			{
				objTourResponse.status = "ok";
				objTourResponse.DetalleHotel = lstDetalleHTL.ToList();
				return Ok(objTourResponse);
			}
			else
			{

				objTourResponse.status = "fail";
				objTourResponse.DetalleHotel = lstDetalleHTL.ToList();
				return Ok(objTourResponse);


			}

		}




		/*SRC-021*/
		[HttpPost]
		[Route("GetExplora2")]
		public IHttpActionResult GetExplora2(PaisesRequest pais)
		{
			LDestinos objDestinoLat = new LDestinos();

			DestinoLatResponsive objDestinoRsp = new DestinoLatResponsive();

			var lstDestinos = objDestinoLat.ObtenerPaisesLat2(pais.idioma);


			if (lstDestinos.Count() > 0)
			{

				objDestinoRsp.status = "ok";
				objDestinoRsp.destinos = lstDestinos.ToList();


				return Ok(objDestinoRsp);

			}
			else
			{
				objDestinoRsp.status = "fail";
				return Ok(objDestinoRsp);

				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//	Content = new StringContent("No se encontro los destinos.")
				//};
				//throw new HttpResponseException(message);
			}



		}

        [HttpPost]
        [Route("ObtienePodcastVideo")]
        public IHttpActionResult ObtienePodcastVideo(PlaneaRequest Planea)
        {
            LPlanea objPlanea = new LPlanea();
            PlaneaResponse respPlanea = new PlaneaResponse();

            var lstPlanea = objPlanea.ObtienePodcastVideo(Planea.idCliente, Planea.codPais);

            List<PodcastVideo> lstPodcastVideo = new List<PodcastVideo>();

            foreach (var item in lstPlanea)
            {

                PodcastVideo objPromPais = new PodcastVideo
                {

                    Descripcion = item.Descripcion,
                    Titulo = item.Titulo,
                    CodPais = item.CodPais,
                    URL_Video = item.URL_Video,
                    Idioma = item.Idioma,
                    Tipo = item.Tipo,
					URL_IMG = item.URL_IMG

                };

                lstPodcastVideo.Add(item: objPromPais);

            }

            if (lstPlanea.Count() > 0)
            {
                respPlanea.status = "ok";
                respPlanea.PodcastVideo = lstPodcastVideo;

                return Ok(respPlanea);
            }
            else
            {

                respPlanea.status = "fail";
                return Ok(respPlanea);

            }

        }


		/*SRC-102*/
		[HttpPost]
		[Route("RegistrarNuevoViaje2")]
		public IHttpActionResult RegistrarNuevoViaje2(NuevoViajeRequest Nev)
		{
			LNuevoViaje objNuevoViaje = new LNuevoViaje();
			NuevoViajeResponse objNViaje = new NuevoViajeResponse();
			var CodViaje = objNuevoViaje.RegistrarNuevoViaje2(Nev.user_id, Nev.rsp1, Nev.rsp2, Nev.rsp3, Nev.rsp4, Nev.ADT, Nev.CHD);
			//foreach (Incluye p in Nev.incluye.ToList())
			//{
			//	objNuevoViaje.RegistrarViajeros(Convert.ToString(CodViaje), p.nombre, p.apPaterno, p.apMaterno, p.edad);
			//}
			NuevoViajeResponse fencuesta = new NuevoViajeResponse();
			List<NuevoViajeResponse> lstNuevoViaje = new List<NuevoViajeResponse>();
			fencuesta.status = "OK";
			fencuesta.idsol = Convert.ToString(CodViaje);
			lstNuevoViaje.Add(item: fencuesta);

			if (lstNuevoViaje.Count() > 0)
			{
				return Ok(fencuesta);
			}
			else
			{



				fencuesta.status = "fail";

				return Ok(fencuesta);

			}

		}


        [HttpPost]
        [Route("ObtieneCodigoCompartir")]
        public IHttpActionResult ObtieneCodigoCompartir(CompartirRequest Planea)
        {
            LPlanea objPlanea = new LPlanea();
            CompartirResponse respCompartir = new CompartirResponse();
            //var lstPlanea = objPlanea.ObtienePodcastVideo(Planea.idCliente, Planea.codPais);
            List<PodcastVideo> lstPodcastVideo = new List<PodcastVideo>();

            respCompartir.CodigoCompartir = "XXXXXXXXXXX";
            respCompartir.Status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;

            //foreach (var item in lstPlanea)
            //{

            //    PodcastVideo objPromPais = new PodcastVideo
            //    {

            //        Descripcion = item.Descripcion,
            //        Titulo = item.Titulo,
            //        CodPais = item.CodPais,
            //        URL_Video = item.URL_Video,
            //        Idioma = item.Idioma,
            //        Tipo = item.Tipo

            //    };

            //    lstPodcastVideo.Add(item: objPromPais);

            //}

            //if (lstPlanea.Count() > 0)
            //{
            //    respPlanea.status = "ok";
            //    respPlanea.PodcastVideo = lstPodcastVideo;

            //    return Ok(respPlanea);
            //}
            //else
            //{

            //    respPlanea.status = "fail";
            return Ok(respCompartir);

            //}

        }



		[HttpPost]
		[Route("ObtieneComentPasajeros")]
		public IHttpActionResult ObtieneComentarios(ComentarioRequest Comentarios)
		{
			LComentarios objComent = new LComentarios();
			ComentariosResponse respComent = new ComentariosResponse();		

			var lstComent = objComent.ObtieneComentarios(Comentarios.idioma);

			List<comentarioPasajero> lstComentarioPasajero = new List<comentarioPasajero>();


			foreach (var item in lstComent)
			{

				comentarioPasajero objComentario = new comentarioPasajero
				{

					nombrePasajero = item.nombrePasajero,
					mensaje = item.mensaje,
					fecha = item.fecha,
					titulo = item.titulo,
					//clasificacion = item.clasificacion,
					//imgEstrella = item.imgEstrella,
					 url_img = item.url_img,
					id_coment = item.id_coment,
					codpais = item.codpais,
					nompais = item.nompais,


				};

				lstComentarioPasajero.Add(item: objComentario);		

		}

			if (lstComentarioPasajero.Count() > 0)
			{
				respComent.status = "ok";
				respComent.comentarioPasajero = lstComentarioPasajero;

				return Ok(respComent);
			}
			else
			{

				respComent.status = "fail";
				return Ok(respComent);

			}

		}



		[HttpPost]
		[Route("RegistrarComentPasajero")]
		public IHttpActionResult RegistrarNuevoComentario(ComentPasajeroNewRequest Coment)
		{
			LComentarios objNuevoComent = new LComentarios();
			ComentarioResponse objComentPas = new ComentarioResponse();


			string url_imagen = string.Empty;
			url_imagen = objNuevoComent.GuardarImagenes(Coment.URL_img);

			var Registro = objNuevoComent.RegistrarComentario(Coment.codCliente, Coment.mensaje, url_imagen);


			ComentarioResponse fcoment = new ComentarioResponse();
			List<ComentarioResponse> lstComentPas = new List<ComentarioResponse>();
			fcoment.status = "OK";
			//fencuesta.idsol = Convert.ToString(CodViaje);
			//lstNuevoViaje.Add(item: fencuesta);

			if (Registro.registro > 0)
			{
				return Ok(fcoment);
			}
			else
			{

				fcoment.status = "fail";

				return Ok(fcoment);

			}

		}



		[HttpPost]
		[Route("ObtieneHotelInfoAntes")]
		public IHttpActionResult ObtieneHotelInfoAntes(HotelInfoRequest Htl)
		{
			LHotelInfo objhtl = new LHotelInfo();

			HotelInfoResponse respComent = new HotelInfoResponse();			

			var lstHtl = objhtl.ObtieneHotelInfo(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion);
			var lstStaff = objhtl.ObtieneStaffInfoAntes(Htl.NroPedido, Htl.Idioma);
			var lstVideo = objhtl.ObtieneVideoInfoAntes(Htl.NroPedido, Htl.Idioma);
			var lstClima = objhtl.ObtieneClimaInfoAntes(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion);
			var lstViaje = objhtl.ObtieneInfoViaje(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion);
			var lstResumen = objhtl.ObtieneResumen(Htl.NroPedido, Htl.NroPropuesta);
			var lstRequerimiento = objhtl.ObtieneRequerimiento(Htl.NroPedido, Htl.Idioma);


			List<hotelInformacion> lstHtlInfo = new List<hotelInformacion>();


			var agrupacion = from p in lstViaje group p by p.NroTipoInfo into grupo select grupo;

			if (lstHtl.Count() > 0)
			{
				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
				respComent.HotelInformacion = lstHtl.ToList();
				respComent.StaffInfo = lstStaff.ToList();
				respComent.Video = lstVideo.ToList();
				respComent.ClimaInfo = lstClima.ToList();
				respComent.Resumen = lstResumen.ToList();
				respComent.ReqVisitaPeru = lstRequerimiento.ToList();

				//respComent.InfoViaje = lstViaje.ToList();


				foreach (var item in agrupacion)
				{
									   
					int i = 0;
					int cantidad = agrupacion.Count();

					var ListInfoViaje = (from p in item
											where p.NroTipoInfo != string.Empty
											select new infoViaje()
											{
												NroTipoInfo = p.NroTipoInfo,
												//OrdenTipo = p.OrdenTipo,
												//OrdenDet = p.OrdenDet,
												NomInfo = p.NomInfo,
										     	Descripcion = objhtl.ObtieneInfoViaje2(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion, p.NroTipoInfo).Where(a => a.OrdenDet != 0).ToList()
					
				}).ToList();

					respComent.InfoViaje.AddRange(ListInfoViaje);

				}
			

				return Ok(respComent);
			}
			else
			{


				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;

				return Ok(objhtl);
				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//    Content = new StringContent("No se encontra Los Beneficios registrados.")
				//};
				//throw new HttpResponseException(message);
			}



		}




		[HttpPost]
		[Route("ObtieneReservaInfo")]
		public IHttpActionResult ObtieneReservaInfo(ReservaInfoRequest Htl)
		{
			ReservaInfo objhtl = new ReservaInfo();

			ReservaInfoResponse respComent = new ReservaInfoResponse();

			var lstPas = objhtl.ObtienePasajeroInfo(Htl.NroPedido);
			var lstTerr = objhtl.ObtieneTerrestreInfo(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion,3);
			var lstAer = objhtl.ObtieneAereoInfo(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion, 1);
			var lstHotel = objhtl.ObtieneHotelInfo(Htl.NroPedido, Htl.NroPropuesta, Htl.NroVersion);

			List<hotel> lstHtlInfo = new List<hotel>();


			if (lstHotel.Count() > 0)
			{
				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
				respComent.Pasajero = lstPas.ToList();
				respComent.BoletoTerrestre = lstTerr.ToList();
				respComent.BoletoAereo = lstAer.ToList();
				respComent.Hotel = lstHotel.ToList();


				return Ok(respComent);
			}
			else
			{


				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;

				return Ok(objhtl);
				//var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
				//{
				//    Content = new StringContent("No se encontra Los Beneficios registrados.")
				//};
				//throw new HttpResponseException(message);
			}



		}




		[HttpPost]
		[Route("ObtienePasajero")]
		public IHttpActionResult ObtienePasajero(PasajeroRequest Psj)
		{
			Pasajeros objPs = new Pasajeros();

			PasajeroInfoResponse respComent = new PasajeroInfoResponse();

			var lstPas = objPs.ObtienePasajeroInfo(Psj.NroPedido);


			List<pasajeroInfo> lstPasInfo = new List<pasajeroInfo>();


			if (lstPas.Count() > 0)
			{
				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
				respComent.PasajeroInfo = lstPas.ToList();		

				return Ok(respComent);
			}
			else
			{


				respComent.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;

				return Ok(objPs);
			
			}



		}


		[HttpPost]
		[Route("RegistrarPasajeros")]
		public IHttpActionResult RegistrarPasajeros(PasajeroRequest Psjrs)
		{
			Pasajeros objNuevoPasajero = new Pasajeros();
			PasajeroInfoResponse objComentPas = new PasajeroInfoResponse();



			var Registro = objNuevoPasajero.RegistrarPasajero(Psjrs.NumPasajero, Psjrs.NombrePasajero, Psjrs.ApePasajero, Psjrs.NumPasaporte, Psjrs.FchNacimiento, Psjrs.Nacionalidad, Psjrs.NroPedido,Psjrs.TipoPasajero, Psjrs.Genero, Psjrs.Observacion);


			PasajeroInfoResponse fcoment = new PasajeroInfoResponse();
			List<PasajeroInfoResponse> lstComentPas = new List<PasajeroInfoResponse>();
			fcoment.status = "OK";
			//fencuesta.idsol = Convert.ToString(CodViaje);
			//lstNuevoViaje.Add(item: fencuesta);

			if (Registro.registro > 0)
			{
				return Ok(fcoment);
			}
			else
			{

				fcoment.status = "fail";

				return Ok(fcoment);

			}

		}






	}
}
