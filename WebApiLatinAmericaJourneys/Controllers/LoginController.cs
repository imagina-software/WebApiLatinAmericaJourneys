﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys;
using WebApiLatinAmericaJourneys.Repository.Wallet;
using WebApiLatinAmericaJourneys.Utility;

namespace WebApiLatinAmericaJourneys.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]

    [AllowAnonymous]
    [RoutePrefix("api/login")]
    public class LoginController : ApiController
    {
        [HttpGet]
        [Route("echoping")]
        public IHttpActionResult EchoPing()
        {
            return Ok(true);
        }

        [HttpGet]
        [Route("echouser")]
        public IHttpActionResult EchoUser()
        {
            var identity = Thread.CurrentPrincipal.Identity;
            return Ok($" IPrincipal-user: {identity.Name} - IsAuthenticated: {identity.IsAuthenticated}");
        }

        //[RequireHttps]
        [HttpPost]
        [Route("authenticate")]
        public IHttpActionResult Authenticate(LoginRequest login)
        {

            List<TokenResponse> lstToken = new List<TokenResponse>();


            if (login == null)
                throw new HttpResponseException(HttpStatusCode.BadRequest);

            bool isCredentialValid = (login.password == "Pentagrama2020$" || login.password=="PentagramaLocal$");
            if (isCredentialValid)
            {
                var token = TokenGenerator.GenerateTokenJwt(login.username);

                TokenResponse fToken = new TokenResponse
                {
                    tokenJWT = token
                };

                lstToken.Add(item: fToken);



                return Ok(lstToken.FirstOrDefault());
            }
            else
            {
                return Unauthorized();
            }
        }

        [HttpPost]
        [Route("GetLogin")]
        public IHttpActionResult GetLogin(LoginWRequest Acc)
        {

            if (Acc == null) {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Ingrese su usuario y clave correctos")
                };
                throw new HttpResponseException(message);
            }

            var passwordProduccion = ConfigurationManager.AppSettings["JWT_PASS_PRO"];
            var passwordLocal= ConfigurationManager.AppSettings["JWT_PASS_LOCAL"];

            Llogin objLoginW = new Llogin();



            var lstLogin = objLoginW.LeerUsuario(Acc.uid, Acc.pass, Acc.usernameJWT, Acc.passwordJWT);

            bool isCredentialValid = (Acc.passwordJWT == passwordProduccion || Acc.passwordJWT == passwordLocal);


            if (isCredentialValid)
            {
                return Ok(lstLogin);

            }
            else {

                return Unauthorized();

            } 
         

        }

        [HttpPost]
        [Route("GetAutoLogin")]
        public IHttpActionResult GetAutoLogin(LoginWRequest Acc)
        {

            if (Acc == null)
            {

                var message = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent("Ingrese su usuario y clave correctos")
                };
                throw new HttpResponseException(message);
            }

            var passwordProduccion = ConfigurationManager.AppSettings["JWT_PASS_PRO"];
            var passwordLocal = ConfigurationManager.AppSettings["JWT_PASS_LOCAL"];

            Llogin objLoginW = new Llogin();



            int cliente = Acc.uidGo.Length - (Acc.uidGo.Length - (Acc.uidGo.Length - (Acc.uidGo.Substring(0, 7).Length)));
            var codCliente = Acc.uidGo.Substring(7, cliente);


            var lstLogin = objLoginW.LeerUsuarioAutoLogin(Acc.uid, Acc.pass, Acc.usernameJWT, Acc.passwordJWT, codCliente);

            bool isCredentialValid = (Acc.passwordJWT == passwordProduccion || Acc.passwordJWT == passwordLocal);


            if (isCredentialValid)
            {
                return Ok(lstLogin);

            }
            else
            {

                return Unauthorized();

            }


        }

        [HttpPost]
        [Route("GetAcceso")]
        public async Task<IHttpActionResult> GetAccesoAsync(AccesoRequest acc)
        {
            //string Mensaje;
            ClienteResponse objClienteRS = new ClienteResponse();
            LoginAccess objAcceso = new LoginAccess();
            Email objEmail = new Email();
            LoginAccess objLogin = new LoginAccess();

            var lstCliente = objAcceso.LeerCorreo(acc.emailCliente);

            int longitud = 6;
            Guid miGuid = Guid.NewGuid();
            string token = Convert.ToBase64String(miGuid.ToByteArray());
            token = token.Replace("=", "").Replace("+", "").Replace("/", "");

            string codigoTemporal = token.Substring(0, longitud);

            if (lstCliente.Count() > 0)
            {

                objClienteRS.claveCliente = lstCliente.FirstOrDefault().claveCliente.Trim();

                acc.cuerpo = ConstantesWeb.STR_EMAIL_CUERPO_CAMBIO_CLAVE + codigoTemporal;

                var msjTmp = objLogin.ActualizarCodigoTemporal(codigoTemporal, ConstantesWeb.CHR_ESTADO_ACTIVO, acc.emailCliente);


                if (msjTmp.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
                {

                    //objEmail.EnviarCorreoSendGrid(ConstantesWeb.STR_NOMBRE_EMISOR,
                    //    ConstantesWeb.STR_CORREO_EMISOR,
                    //    acc.emailCliente,
                    //    ConstantesWeb.STR_EMAIL_ASUNTO_CAMBIO_CLAVE,
                    //    acc.emailCliente,
                    //    codigoTemporal,
                    //    lstCliente.FirstOrDefault().tipoIdioma.ToString(),
                    //    ConstantesWeb.STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE);


                    await objEmail.EnviarCorreoMailJetAsync(ConstantesWeb.STR_NOMBRE_EMISOR,
                        ConstantesWeb.STR_CORREO_EMISOR,
                        acc.emailCliente,
                        ConstantesWeb.STR_EMAIL_ASUNTO_CAMBIO_CLAVE,
                        acc.emailCliente,
                        codigoTemporal,
                        lstCliente.FirstOrDefault().tipoIdioma.ToString(),
                        ConstantesWeb.STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE);


                    objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
                    objClienteRS.msg = "ok, se envió el correo satisfactoriamente";
                }
                else
                {
                    objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                    objClienteRS.msg = msjTmp;
                }

                return Ok(objClienteRS);
            }
            else
            {
                objClienteRS.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                objClienteRS.msg = "Por favor ingrese el correo con el que hizo su suscripcion ";


                return Ok(objClienteRS);

            }
        }

    }
}
