﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using WebApiLatinAmericaJourneys.Models.Promocion;
using WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys;


namespace WebApiLatinAmericaJourneys.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [Authorize]
    [RoutePrefix("api/promocion")]
    public class PromocionController : ApiController
    {


        PromocionAccess obPromAccess = new PromocionAccess();

        PromocionResponse objPromRsp = new PromocionResponse();

        List<Promocion> lstProm = new List<Promocion>();

        [HttpPost]
        [Route("ObtienePromociones")]
        public IHttpActionResult ObtienePromociones(PromocionRequest prom) {

            var lstPromociones = obPromAccess.ObtenerListadoPromocion(prom.IdPais,prom.Idioma);

            foreach (var item in lstPromociones) {

                List<PromocionPais> lstPromp = new List<PromocionPais>();

                var lstPromPaisPrecio = obPromAccess.ObtenerListadoPromocionxPais(item.IdPromocion, prom.IdPais);

                foreach (var item2 in lstPromPaisPrecio) {


                    PromocionPais objPromPais = new PromocionPais
                    {

                        IdPlanSuperior = item2.IdPlanSuperior,
                        Plan =item2.Plan,
                        Precio = item2.Precio

                    };

                    lstPromp.Add(item: objPromPais);

                }
          
                Promocion objProm  = new Promocion
                {

                    IdPromocion = item.IdPromocion,
                    Titulo = item.Titulo,
                    Descripcion = item.Descripcion,
                    Estado = item.Estado,
                    Foto = item.Foto,
                    PrecioRegular =item.PrecioRegular,
                    Precios = lstPromp,
                    Opc1=item.Opc1,
                    Opc2 = item.Opc2,
					Ciudad = item.Ciudad,

				};

                lstProm.Add(item: objProm);

            }

            objPromRsp.Promociones.AddRange(lstProm);

            return Ok(objPromRsp);
        
        }


    }
}
