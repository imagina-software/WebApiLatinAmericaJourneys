﻿using System.Linq;
using System.Web.Http;
using System.Collections.Generic;
using WebApiLatinAmericaJourneys.Utility;
using WebApiLatinAmericaJourneys.Models.Visa;
using WebApiLatinAmericaJourneys.Repository.Visa;
using System.Configuration;
using System.Threading.Tasks;

namespace WebApiLatinAmericaJourneys.Controllers
{
    [Authorize]
    [RoutePrefix("api/visa")]
    public class VisaController : ApiController
    {

        [HttpPost]
        [Route("RegistraRespuestaVisa")]
        public  async Task<IHttpActionResult> RegistraRespuestaVisa(VisaRequest VisaRq)
        {
            VisaAccess objVisaAccess = new VisaAccess();
            VisaResponse objVisaRsp = new VisaResponse();
            Email objEmail = new Email();
            SuscripcionAccess objSuscripcion = new SuscripcionAccess();

            List<VisaComplete> lstVisaComplete = new List<VisaComplete>();

            lstVisaComplete = objVisaAccess.RegistrarRespuestaVisa(VisaRq);

            string cuerpoCorreo = string.Empty;
			string cuerpoCorreoEj = string.Empty;

			if (lstVisaComplete.FirstOrDefault().Mensaje.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK))
            {
                var lstSuscripcion = objSuscripcion.RegistrarSuscripcion(VisaRq,lstVisaComplete.FirstOrDefault().NroOrdenPago);


                if (lstSuscripcion.FirstOrDefault().Mensaje_status.Equals(ConstantesWeb.STR_ESTADO_PROCEDURE_OK)) {

                    cuerpoCorreo = ConstantesWeb.STR_EMAIL_CUERPO_ENVIO_CLAVE + lstSuscripcion.FirstOrDefault().CorreoCliente + "  " + lstSuscripcion.FirstOrDefault().Password + ",   " + "Puede ingresar a la aplicación a través de la siguiente URL : " + ConfigurationManager.AppSettings["URL_APP_WEB"];

                    //objEmail.EnviarCorreoSendGrid(ConstantesWeb.STR_NOMBRE_EMISOR,
                    //    ConstantesWeb.STR_CORREO_EMISOR,
                    //    lstSuscripcion.FirstOrDefault().CorreoCliente,
                    //    ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLAVE, 
                    //    lstSuscripcion.FirstOrDefault().CorreoCliente,
                    //    lstSuscripcion.FirstOrDefault().Password,
                    //    lstSuscripcion.FirstOrDefault().Idioma,
                    //    ConstantesWeb.STR_TEMPLATE_ENVIO_ACCESOS);

                     await objEmail.EnviarCorreoMailJetAsync(
                         ConstantesWeb.STR_NOMBRE_EMISOR,
                        ConstantesWeb.STR_CORREO_EMISOR,
                        lstSuscripcion.FirstOrDefault().CorreoCliente,
                        ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLAVE,
                        lstSuscripcion.FirstOrDefault().CorreoCliente,
                        lstSuscripcion.FirstOrDefault().Password,
                        lstSuscripcion.FirstOrDefault().Idioma,
                        ConstantesWeb.STR_TEMPLATE_ENVIO_ACCESOS
                        );

					// ENVIO DE CORREO EJECUTIVA

					cuerpoCorreoEj = ConstantesWeb.STR_EMAIL_CUERPO_EJECUTIVO + lstSuscripcion.FirstOrDefault().CodCliente + "  " + lstSuscripcion.FirstOrDefault().CorreoCliente;

                    //objEmail.EnviarCorreoSendGrid(ConstantesWeb.STR_NOMBRE_EMISOR,
                    //    ConstantesWeb.STR_CORREO_EMISOR,
                    //    lstSuscripcion.FirstOrDefault().CorreoEjecutiva,              
                    //    ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLIENTE,
                    //    VisaRq.CodCliente.ToString(),
                    //    lstSuscripcion.FirstOrDefault().CorreoCliente,
                    //    lstSuscripcion.FirstOrDefault().Idioma,
                    //    ConstantesWeb.STR_TEMPLATE_ENVIO_EJECUTIVA);


                    await objEmail.EnviarCorreoMailJetAsync(
                        ConstantesWeb.STR_NOMBRE_EMISOR,
                        ConstantesWeb.STR_CORREO_EMISOR,
                        lstSuscripcion.FirstOrDefault().CorreoEjecutiva,
                        ConstantesWeb.STR_EMAIL_ASUNTO_ENVIO_CLIENTE,
                        VisaRq.CodCliente.ToString(),
                        lstSuscripcion.FirstOrDefault().CorreoCliente,
                        lstSuscripcion.FirstOrDefault().Idioma,
                        ConstantesWeb.STR_TEMPLATE_ENVIO_EJECUTIVA
                        );


                    objVisaRsp.status = ConstantesWeb.STR_ESTADO_SERVICIO_OK;
                    objVisaRsp.msg_ES = lstSuscripcion.FirstOrDefault().Mensaje_ES;
                    objVisaRsp.msg_EN = lstSuscripcion.FirstOrDefault().Mensaje_EN;
                    //objVisaRsp.NroOrdenPago = lstVisaComplete.FirstOrDefault().NroOrdenPago.ToString();
                    objVisaRsp.UrlAppAndroid = ConfigurationManager.AppSettings["URL_APP_ANDROID"];
                    objVisaRsp.UrlAppIos = ConfigurationManager.AppSettings["URL_APP_IOS"];
                    objVisaRsp.UrlWeb = ConfigurationManager.AppSettings["URL_APP_WEB"];
                    objVisaRsp.Usuario = lstSuscripcion.FirstOrDefault().CorreoCliente;
                    objVisaRsp.Password = lstSuscripcion.FirstOrDefault().Password;
                    objVisaRsp.IdSuscripcion = lstSuscripcion.FirstOrDefault().Id.ToString().Substring(0, 8);

                    return Ok(objVisaRsp);

                }
                else
                {

                    objVisaRsp.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                    objVisaRsp.msg_ES = lstSuscripcion.FirstOrDefault().Mensaje_ES;
                    objVisaRsp.msg_EN = lstSuscripcion.FirstOrDefault().Mensaje_EN;

                    return Ok(objVisaRsp);
                }

             
            }
            else
            {

                objVisaRsp.status = ConstantesWeb.STR_ESTADO_SERVICIO_FAIL;
                objVisaRsp.msg_ES = lstVisaComplete.FirstOrDefault().Mensaje;
                objVisaRsp.msg_EN = lstVisaComplete.FirstOrDefault().Mensaje;


                return Ok(objVisaRsp);

            }

        }





    }
}