﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Entities
{
    public class PodcastVideo
    {

        public string Descripcion { get; set; }
        public string Titulo { get; set; }
        public string CodPais { get; set; }
        public string URL_Video { get; set; }
        public string Idioma { get; set; }
        public string Tipo { get; set; }
		public string URL_IMG { get; set; }

	}
}