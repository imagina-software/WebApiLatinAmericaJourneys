﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Entities
{
    public class Promocion
    {

        public int IdPromocion { get; set; }

        public string Titulo { get; set; }

        public string Descripcion { get; set; }

        public bool Estado { get; set; }

        public string Foto { get; set; }

        public decimal PrecioRegular { get; set; }

        public string Opc1 { get; set; }

        public string Opc2 { get; set; }

		public string Ciudad { get; set; }


	}

    public class PromocionPais
    {

        public int IdPlanSuperior { get; set; }

        public string Plan { get; set; }


        public decimal Precio { get; set; }

    }
}