﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class AccesoRequest
    {
        
        public string nombreEmisor { get; set; }
        public string emailEmisor { get; set; }
        public string emailCliente { get; set; }
        public string asunto { get; set; }
        public string cuerpo { get; set; }

        public string codcliente { get; set; }
    }
}