﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class ActualizarContrasenaRequest
    {
        public string CodCliente { get; set; }
        //public string ContrasenaAntigua { get; set; }
        public string ContrasenaNueva { get; set; }
        public string ContrasenaNueva2 { get; set; }
        public string CodigoTemporal { get; set; }
    }
}