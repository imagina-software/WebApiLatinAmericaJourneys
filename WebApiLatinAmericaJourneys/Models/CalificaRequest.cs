﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{

    public class CalificaRequest
    {
   
        public string nroPedido { get; set; }
        public string nroPropuesta { get; set; }
        public string nroVersion { get; set; }
        public string stars { get; set; }
        public string comment { get; set; }
            
    }
}