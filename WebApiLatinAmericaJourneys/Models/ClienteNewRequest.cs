﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class ClienteNewRequest
	{

		public string nombre { get; set; }
		public string apetPat { get; set; }
		public string apetMat { get; set; }
		public string email { get; set; }
		public string tipoIdioma { get; set; }
		public string telefono { get; set; }
		public string codPais { get; set; }
		public string tipoDoc { get; set; }
		public int NumDoc { get; set; }
		public string CodPlan { get; set; }
		public int Origen { get; set; }
		public string CodigoCompartir { get; set; }


	}

	public class ClienteMensaje
	{
		public string Mensaje { get; set; }
	}




}