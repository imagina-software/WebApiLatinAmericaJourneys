﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class ClienteNewResponse
	{

		public string status { get; set; }
		public string codCliente { get; set; }
		public string msg_ES { get; set; }
		public string msg_EN { get; set; }
		public int registro { get; set; }

		public string usuario { get; set; }
		public string password { get; set; }
		public string correo { get; set; }
		public string correoEjecutiva { get; set; }

		public string urlWeb { get; set; }

		public string urlPlayStore { get; set; }

		public string urlAppStore{ get; set; }

	}
}