﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{

    public class ClienteResponse
    {
        public bool loginSuccess { get; set; }
        public string codCliente { get; set; }
        public string nomCliente { get; set; }
        public string apePaterno { get; set; }
        public string apeMaterno { get; set; }
        public string emailCliente { get; set; }
        public char tipoIdioma { get; set; }
        public string claveCliente { get; set; }
        public string idCliente { get; set; }
		public string video_url { get; set; }

        public string status { get; set; }

        public string msg { get; set; }


    }
}