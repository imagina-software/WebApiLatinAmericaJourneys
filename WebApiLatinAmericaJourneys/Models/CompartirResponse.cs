﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class CompartirResponse
    {

        public string CodigoCompartir { get; set; }
        public string Status { get; set; }

    }
}