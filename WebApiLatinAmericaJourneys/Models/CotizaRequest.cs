﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class CotizaRequest
	{
		public string paquete_id { get; set; }
		public string nombres { get; set; }
		public string email { get; set; }
		public string telefono { get; set; }
		public string pais { get; set; }
		public string preferencias { get; set; }
		public string fecha { get; set; }
		public string adt { get; set; }
		public string chd { get; set; }

	}
}