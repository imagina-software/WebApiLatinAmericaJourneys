﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Destino
    {
        public string id { get; set; }
        public string nombre { get; set; }
        public string img_url { get; set; }
		public string descripcion2 { get; set; }
		public string titulo { get; set; }

	}
}