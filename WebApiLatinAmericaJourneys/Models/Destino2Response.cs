﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Repository.Wallet;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Destino2Response
    {

        public string status { get; set; }
        public List<Destino>  destinos{ get;set; }
    }
}