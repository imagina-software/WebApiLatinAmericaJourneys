﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class DestinoDetalleResponse
    {

        public string status { get; set; }
        public List<PaisesLat> info { get; set; }

		public List<FotosDestino> fotos { get; set; }


 

    }
}