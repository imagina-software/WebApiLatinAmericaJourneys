﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class DestinoLatResponsive
	{

		public string status { get; set; }
		public List<PaisesLat> destinos { get; set; }

	}
}