﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{


	public class DestinoResponse
	{
		public string status { get; set; }
		public List<Destinos> Destinos { get; set; }
	}

	public class Destinos
	{
		public string id { get; set; }
		public string title { get; set; }
		public string last_msg { get; set; }
		public string img_url { get; set; }
		public string estado { get; set; }


	}
}