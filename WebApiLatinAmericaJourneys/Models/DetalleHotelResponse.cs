﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class DetalleHotelResponse
	{

		public string status { get; set; }
		public List<DetalleHotel> DetalleHotel { get; set; }


	}


	public class DetalleHotel
	{
		public string Imagen1 { get; set; }
		public string Imagen2 { get; set; }
		public string Imagen3 { get; set; }
		public string DireccionHTL { get; set; }
		public string NombreHTL { get; set; }
		public string Telefono { get; set; }
		public int Valoracion { get; set; }
		public string DescripcionHTL { get; set; }
		public string DescripcionHTLI { get; set; }
		public string Flag { get; set; }
	}



}