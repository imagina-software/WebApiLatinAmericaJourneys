﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class EItinerario
    {
        public string nroServicio { get; set; }
        public string codTipoServicio { get; set; }
        public string dia { get; set; }
        public string fchInicio { get; set; }
        public string anioInicio { get; set; }
        public string mesInicio { get; set; }
        public string diaInicio { get; set; }
        public string ciudad { get; set; }
        public string horaServicio { get; set; }
        public string desServicioDet { get; set; }
        public string flagColor { get; set; }
        public string nroDia { get; set; }
        public string nroOrden { get; set; }
        public string keyReg { get; set; }
        public string codUsuario { get; set; }
        public string notificacion { get; set; }
        public string nroPedido { get; set; }
        public string nroPropuesta { get; set; }
        public string nroVersion { get; set; }
        public string desServicio { get; set; }


    }

    public class ProgramaViaje
    {
        public string FchInicio { get; set; }
        public string NroPrograma { get; set; }
        public string DesPrograma { get; set; }
        public string CantDias { get; set; }
        public string EmailVendedor { get; set; }
        public string NroPedido { get; set; }
        public string NroPropuesta { get; set; }
        public string NroVersion { get; set; }
        public string Stars { get; set; }
        public string TipoIdioma { get; set; }
		//public int NroServicio { get; set; }
	}

    public class Banner
    {
        public string StrURL { get; set; }
    }

	public class Precio
	{
		public string DesOrden { get; set; }
		public decimal PrecioxPersona { get; set; }
		public int CantPersonas { get; set; }
		public decimal PrecioTotal { get; set; }

	}

	public class ResumenIti
	{
		public string Resumen { get; set; }

	}


	public class Actividades
    {
        public string Hora { get; set; }
        public string Descripcion { get; set; }
        //public string Lugar { get; set; }
    }

    public class Lugares
    {
       // public string Titulo { get; set; }
        public string Lugar { get; set; }

        public string Hora { get; set; }
        public string Descripcion { get; set; }
		//public List<Actividades> Actividades { get; set; }
		public string NroServicio { get; set; }
		public int CodTipoServicio { get; set; }
	}

    public class ItinerarioViaje

    {
        //      public string AnioInicio { get; set; }
        //      public string MesInicio { get; set; }
        //      public string DiaInicio { get; set; }
        //public string FlagAtencion { get; set; }


        


        public string dia { get; set; }

        //public string nroServicio { get; set; }
        public string fchInicio { get; set; }

        //public string ciudad { get; set; }

        //public string horaServicio { get; set; }

        public string desServicio { get; set; }

        public string nroDia { get; set; }

		//public int nroOrden { get; set; }

		//public string desServicioDet { get; set; }

		//public string codTipoServicio { get; set; }

		public string flagatencion { get; set; }

		public List<Lugares> Lugares { get; set; }
    }

}