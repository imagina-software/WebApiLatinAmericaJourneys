﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class FotoConsultaRequest
    {

        public string nroPedido { get; set; }
        public string nroPropuesta { get; set; }
        public string nroVersion { get; set; }
        public string status { get; set; }

    }
}