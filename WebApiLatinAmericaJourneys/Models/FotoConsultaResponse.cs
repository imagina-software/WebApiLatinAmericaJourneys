﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class FotoConsultaResponse
    {
            public string status { get; set; }
            public List<Fotos> Fotos { get; set; }
    }

    public class Fotos
    {
        public string idFoto { get; set; }
        public string url { get; set; }
        public string titulo { get; set; }
        public string comentario { get; set; }
        //public string linkshare { get; set; }
    }
}