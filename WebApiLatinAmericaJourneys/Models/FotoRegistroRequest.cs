﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class FotoRegistroRequest
    {
        //public string CodCliente { get; set; }
        public string nroPedido { get; set; }
        public string nroPropuesta { get; set; }
        public string nroVersion { get; set; }
        public string fototitulo { get; set; }
        public string fotocomment { get; set; }
        public string fotocontent { get; set; }

    }
}