﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class FotoRegistroResponse
    {
        public string status { get; set; }
        public string idfoto { get; set; }

    }
}