﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class FotosDestino
    {
        public string url { get; set; }
		public string descripcion { get; set; }
		public string titulo { get; set; }
		public string id { get; set; }
		public string flagL { get; set; }
		public string flagIMG { get; set; }
	}
}