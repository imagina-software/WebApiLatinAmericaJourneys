﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class HotelInfoResponse
	{


		public HotelInfoResponse()
		{

			this.InfoViaje = new List<infoViaje>();

		}



		public string status { get; set; }

		//public string caractEspeciales { get; set; }

		//public int registro { get; set; }
		public List<resumen> Resumen { get; set; }
		public List<hotelInformacion> HotelInformacion { get; set; }
		public List<staffInfo> StaffInfo { get; set; }
		public List<videoInfo> Video { get; set; }
		public List<climaInfo> ClimaInfo { get; set; }
		public List<infoViaje> InfoViaje { get; set; }
		public List<requerimiento> ReqVisitaPeru { get; set; }


	}

	public class hotelInformacion
	{
		public string NomCiudad { get; set; }
		public string NombreHotel { get; set; }

		public string Telefono { get; set; }

		//public string StsReserva { get; set; }

		//public string HotelAlternativo { get; set; }

		//public string NomPagina { get; set; }


	}


	public class staffInfo
	{
		public string NomVendedor { get; set; }

		public string Cargo { get; set; }

		public string TelefonoEmergencia { get; set; }

	}

	public class videoInfo
	{
		public string VideoUrl { get; set; }
		public string VideoTitulo { get; set; }

	}

	public class climaInfo
	{
		public string NomCiudad { get; set; }
		public string MesAnio { get; set; }
		public string TempMinima { get; set; }
		public string TempMaxima { get; set; }
	}


	public class infoViaje
	{
		public string NroTipoInfo { get; set; }
	//	public int OrdenTipo { get; set; }
	//	public int OrdenDet { get; set; }
		public string NomInfo { get; set; }

		public List<Descripcion> Descripcion { get; set; }

	}


	public class Descripcion
	{
		public string NroTipoInfo { get; set; }
		public int OrdenTipo { get; set; }
		public int OrdenDet { get; set; }
		public string NomInfo { get; set; }

	}

	public class resumen
	{
		public string caractEspeciales { get; set; }

	}


	public class requerimiento
	{
		public string ReqVisitaPeru { get; set; }

	}


}