﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class ItinerarioResponse
    {


        public ItinerarioResponse() {

            this.itinerario = new List<ItinerarioViaje>();
        
        }



        public string status { get; set; }
        public List<ProgramaViaje> main { get; set; }
        public List<Banner> banner { get; set; }
		public List<Precio> precio { get; set; }
		public List<ResumenIti> resumen { get; set; }
		public List<ItinerarioViaje> itinerario { get; set; }
        public List<Programa> viajes { get; set; }
    }
}