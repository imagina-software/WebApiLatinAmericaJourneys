﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Mensajes
    {
        public string id { get; set; }
        public string text { get; set; }

        public string src { get; set; }

        public string src_name { get; set; }

        public string src_img { get; set; }

        public string time { get; set; }

    }
}