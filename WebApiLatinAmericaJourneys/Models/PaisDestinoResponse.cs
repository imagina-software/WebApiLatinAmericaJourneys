﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PaisDestinoResponse
	{
		public string status { get; set; }
		public List<PaisDestino> paisDestinos { get; set; }
	}

	public class PaisDestino
	{
		public string id { get; set; }
		public string nombre { get; set; }
		public string img_url { get; set; }



	}


}