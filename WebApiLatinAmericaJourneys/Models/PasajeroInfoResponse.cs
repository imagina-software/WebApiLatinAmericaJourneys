﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;
using System.IO;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PasajeroInfoResponse
	{
		public string status { get; set; }
		public List<pasajeroInfo> PasajeroInfo { get; set; }

		public int registro { get; set; }
	}


	public class pasajeroInfo
	{
		public string NomPasajero { get; set; }
		public string ApePasajero { get; set; }
		public string FchNacimiento { get; set; }
		public string FchNacimientoStr { get; set; }

		public string Pasaporte { get; set; }
		public string Pasajero { get; set; }
		public string Pais { get; set; }
		public string TipoPasajero { get; set; }


	}


}