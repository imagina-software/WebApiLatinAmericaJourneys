﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PasajeroRequest
	{

		public int NroPedido { get; set; }
		public string NombrePasajero { get; set; }
		public string ApePasajero { get; set; }
		public string NumPasaporte { get; set; }
		public string Nacionalidad { get; set; }
		public string Genero { get; set; }
		public string Observacion { get; set; }
		public string FchNacimiento { get; set; }
		public int NumPasajero { get; set; }
		public string TipoPasajero { get; set; }
	}
}