﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PerfilResponse
	{
		public string status { get; set; }
		public List<perfil> perfil { get; set; }
	}

	public class perfil
	{
	public string nombre { get; set; }
	public string paterno { get; set; }
	public string email { get; set; }
	public string telefono { get; set; }


	}

}