﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PlaneaResponse
	{

		public string status { get; set; }
		public List<planea> planea { get; set; }
		public List<PodcastVideo> PodcastVideo { get; set; }

	}

	public class planea
	{
		public string desPregunta { get; set; }
		public string respuesta { get; set; }
		public string URL_Video { get; set; }

	}
	public class PodcastVideo
	{

		public string Descripcion { get; set; }
		public string Titulo { get; set; }
		public string CodPais { get; set; }
		public string URL_Video { get; set; }
		public string Idioma { get; set; }
		public string Tipo { get; set; }
		public string URL_IMG { get; set; }


	}

}