﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class PlanesResponse
	{

		public string status { get; set; }
		public string msg { get; set; }

		public List<planes> planes { get; set; }
	}
	public class planes
		{
			public string id { get; set; }
			public string codPlan { get; set; }
			public string titulo { get; set; }
			public string tipo { get; set; }
			public double precio { get; set; }

	}
}