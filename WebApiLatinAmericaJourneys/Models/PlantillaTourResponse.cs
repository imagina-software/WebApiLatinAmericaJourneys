﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class PlantillaTourResponse
    {
        public string nroPlantilla { get; set; }
        public string idImg { get; set; }
        public string imagen { get; set; }
        public string strURL { get; set; }
        public string calificacion { get; set; }

    }

}