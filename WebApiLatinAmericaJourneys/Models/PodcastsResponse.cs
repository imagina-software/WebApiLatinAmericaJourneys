﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PodcastsResponse
	{
		public string status { get; set; }
		public List<PodCasts> PodCasts { get; set; }
	}


	public class PodCasts
	{
		public string Titulo { get; set; }
		public string Descripcion { get; set; }		
		public string Pais { get; set; }
		public string URL { get; set; }
		public string URL_IMG { get; set; }



	}



}