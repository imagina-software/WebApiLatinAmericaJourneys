﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Programa
    {

        public DateTime fchSys { get; set; }
        public string nroPrograma { get; set; }

        public string stsPrograma { get; set; }

        public string desPrograma { get; set; }

        public int cantDias { get; set; }

        public string keyReg { get; set; }

        public string resumen { get; set; }

        public string resumenComida { get; set; }


        //Propuesta Precio

        public string desOrden { get; set; }
        public decimal precioxPersona { get; set; }
        public int cantPersonas { get; set; }
        public decimal precioTotal { get; set; }
        public string emailVendedor { get; set; }
        public string nombreVendedor { get; set; }


   

    }
}