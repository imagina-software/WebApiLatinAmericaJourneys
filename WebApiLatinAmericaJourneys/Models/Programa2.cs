﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Programa2
    {

        public string id { get; set; }
        public string estado { get; set; }

        public string nombre { get; set; }

        public string img_url { get; set; }

        public string codEstado { get; set; }
    }
}