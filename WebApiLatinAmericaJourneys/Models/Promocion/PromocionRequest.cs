﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models.Promocion
{
    public class PromocionRequest
    { 

        public string IdPais { get; set; }

        public string Idioma { get; set; }

    }
}