﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PromocionesxCliRequest
	{

		public string codCliente { get; set; }
		public string nombres { get; set; }
		public string apellidos { get; set; }
		public string email { get; set; }
	}
}