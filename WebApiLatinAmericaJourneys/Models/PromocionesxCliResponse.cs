﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class PromocionesxCliResponse
	{


		public string status { get; set; }
		public int registro { get; set; }
		public string correoEjecutiva { get; set; }
		public string correoCliente { get; set; }
		public string nomCliente { get; set; }
		public string nomTarifa { get; set; }



	}
}