﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class PropuestaRequest
    {
        public string emailCliente { get; set; }
        public string passwordCliente { get; set; }
        public string zontaVenta { get; set; }
        public string codCliente { get; set; }
        public string codUsuario { get; set; }

        public string idpaquete { get; set; }

        public string codEstado { get; set; }

		public string NroVersion { get; set; }

		public string NroPropuesta { get; set; }

	}
}