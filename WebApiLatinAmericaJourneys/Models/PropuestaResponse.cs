﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class PropuestaResponse
    {
        public string fchSys { get; set; }
        public string fchInicio { get; set; }
        public string nroPrograma { get; set; }
        public string stsPrograma { get; set; }
        public string desPrograma { get; set; }
        public string cantDias { get; set; }
        public string emailVendedor { get; set; }
        public string keyReg { get; set; }
        public string nroPedido { get; set; }
        public string nroPropuesta { get; set; }
        public string nroVersion { get; set; }

    }
}