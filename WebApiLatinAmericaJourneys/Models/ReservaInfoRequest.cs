﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class ReservaInfoRequest
	{

		public int NroPedido { get; set; }
		public int NroPropuesta { get; set; }
		public int NroVersion { get; set; }
		//public string Idioma { get; set; }

	}
}