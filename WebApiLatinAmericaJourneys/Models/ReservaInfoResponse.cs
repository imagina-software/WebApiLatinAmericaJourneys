﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class ReservaInfoResponse
	{

		public string status { get; set; }

		public List<pasajero> Pasajero { get; set; }
		public List<boletoTerrestre> BoletoTerrestre { get; set; }
		public List<boletoAereo> BoletoAereo { get; set; }
		public List<hotel> Hotel { get; set; }
		


	}

	public class pasajero
	{
		public string NomPasajero { get; set; }
		public string ApePasajero { get; set; }
		public string FchNacimiento { get; set; }
		public string FchNacimientoStr { get; set; }

		public string Pasaporte { get; set; }
		public string Pasajero { get; set; }
		public string Pais { get; set; }
		public string TipoPasajero { get; set; }


	}

	public class boletoTerrestre
	{
		public string FchVuelo { get; set; }
		public string RutaVuelo { get; set; }
		public string Aerolinea { get; set; }		
		public string NroVuelo { get; set; }
		public string HoraSalida { get; set; }
		public string HoraLlegada { get; set; }
		//public string CodReserva { get; set; }		
		//public string NroDia { get; set; }
		//public string NroOrden { get; set; }
		public string DesCantidad { get; set; }
		public string CodStsReserva { get; set; }

	}

	public class boletoAereo
	{
		public string FchVuelo { get; set; }
		public string Aerolinea { get; set; }
		public string RutaVuelo { get; set; }
		public string NroVuelo { get; set; }		
		public string HoraSalida { get; set; }
		public string HoraLlegada { get; set; }
		public string CodReserva { get; set; }
		public string DesCantidad { get; set; }
		public string CodStsReserva { get; set; }
		//public string NroDia { get; set; }
		//public string NroOrden { get; set; }

	}


	public class hotel
	{
		public string NomCiudad { get; set; }
		public string Hotel { get; set; }
		public string Telefono1 { get; set; }
		public string StsReserva { get; set; }
		public string HotelAlternativo { get; set; }
		public string FchSys { get; set; }

	}


}