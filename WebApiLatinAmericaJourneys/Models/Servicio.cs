﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Servicio
    {
        public string dia { get; set; }

        public int nroServicio { get; set; }
        public DateTime fchInicio { get; set; }

        public string ciudad { get; set; }

        public string horaServicio { get; set; }

        public string desServicio { get; set; }

        public string nroDia { get; set; }

        public int nroOrden { get; set; }

        public string keyReg { get; set; }

        public string desServicioDet { get; set; }

        public int codTipoServicio { get; set; }

        public string nombreEjecutiva { get; set; }


        //Para servicios tipo Hotel


        //public List<Galeria> ListaGaleria { get; set; }

        public byte[] imagen1 { get; set; }
        public string flagImg01 { get; set; }
        public byte[] imagen2 { get; set; }
        public string flagImg02 { get; set; }
        public byte[] imagen3 { get; set; }
        public string flagImg03 { get; set; }
        public string direccionHTL { get; set; }
        public string nombreHTL { get; set; }
        public string telefono { get; set; }
        public int valoracion { get; set; }
        public string descripcionHTL { get; set; }
        public string descripcionHTLI { get; set; }
        public string resumen { get; set; }
        public string resuCaraEspe { get; set; }
        public string resuComida { get; set; }
    }
}