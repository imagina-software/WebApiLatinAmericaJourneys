﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class StaffInfoResponse
	{

		public string status { get; set; }

		//public int registro { get; set; }
		public List<staffInformacion> StaffInformacion { get; set; }


	}


	public class staffInformacion
	{
		public string NomVendedor { get; set; }
		public string Cargo { get; set; }
		public string TelefonoEmergencia { get; set; }

	}



}