﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class SuscripcionRequest
	{

		public string CodCliente { get; set; }
		public string CodPlan { get; set; }
		public string Ejecutiva { get; set; }

	}
}