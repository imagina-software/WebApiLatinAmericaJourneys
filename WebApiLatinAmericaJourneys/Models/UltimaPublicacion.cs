﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{

    public class UltimaPublicacion
    {
        public int nroPedido { get; set; }
        public int nroPropuesta { get; set; }
        public int nroVersion { get; set; }
        public char flagIdioma { get; set; }
        public int cantPropuestas { get; set; }

    }
}