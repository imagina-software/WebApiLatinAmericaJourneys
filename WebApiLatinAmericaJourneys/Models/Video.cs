﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class Video
    {

        public string IdVideo { get; set; }
       
        public string Video_img { get; set; }

        public string Titulo { get; set; }

        public string Comentario { get; set; }

        public string Embed { get; set; }

    }
}