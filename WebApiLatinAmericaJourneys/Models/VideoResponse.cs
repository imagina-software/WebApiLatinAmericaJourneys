﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
    public class VideoResponse
    {

        public string status { get; set; } 
        public List<Video> Videos { get; set; }


    }
}