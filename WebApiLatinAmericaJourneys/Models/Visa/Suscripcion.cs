﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models.Visa
{
    public class Suscripcion
    {

        public Guid Id { get; set; }

        public int CodCliente { get; set; }

        public int IdPlan { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }

        public string Mensaje_ES { get; set; }

        public string Mensaje_EN { get; set; }

        public string Mensaje_status { get; set; }

		 public string CorreoCliente { get; set; }
		public string CorreoEjecutiva { get; set; }

        public string Idioma { get; set; }


	}
}