﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models.Visa
{
    public class VisaComplete
    {

        public int NroOrdenPago { get; set; }

        public string Usuario { get; set; }

        public string Password { get; set; }

        public string Mensaje { get; set; }

        public string CorreoCliente { get; set; }

    }
}