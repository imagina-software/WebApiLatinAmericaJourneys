﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models.Visa
{
    public class VisaRequest
    {

        public int CodCliente { get; set; }
        public int CodComercio { get; set; }

        public string IdPlan { get; set; }

        public decimal MonOrdenPago { get; set; }

        public DateTime EnvOrdenPago { get; set; }

        public DateTime RptOrdenPago { get; set; }

        public char Respuesta { get; set; }

        public string Cod_accion { get; set; }

        public string Pan { get; set; }

        public char Eci { get; set; }

        public char Cod_autoriza {get;set;}

        public char Ori_Tarjeta { get; set; }

        public string Nom_emisor { get; set; }

        public string Dsc_eci { get; set; }

        public string Currency { get; set; }

        public string Adquiriente { get; set; }

        public string Action_descripction { get; set; }



    }
}