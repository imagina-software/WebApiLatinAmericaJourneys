﻿using System;
using System.Collections.Generic;
using System.EnterpriseServices.Internal;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models.Visa
{
    public class VisaResponse
    {
        public string status { get; set; }
        public string msg_ES { get; set; }
        public string msg_EN { get; set; }
        //public string NroOrdenPago { get; set; }
        public string UrlAppAndroid { get; set; }
        public string UrlAppIos { get; set; }
        public string UrlWeb { get; set; }
        public string Usuario { get; set; }
        public string Password { get; set; }
        public string IdSuscripcion { get; set; }
    }
}