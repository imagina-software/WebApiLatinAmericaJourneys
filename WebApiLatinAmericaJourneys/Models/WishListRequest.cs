﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Models
{
	public class WishListRequest
	{
		public int CodCliente { get; set; }
		public string Origen { get; set; }
		public int ID { get; set; }
		public int Estado { get; set; }
		public string CodPais { get; set; }

	}
}