﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class BeneficiosResponse
    {
        public string status { get; set; }
        public List<Beneficios> beneficios { get; set; }
        public List<BeneficiosDetalle> beneficiosdet { get; set; }

    }

    public class Beneficios
    {

        public string codbeneficio { get; set; }
        public string titulo { get; set; }
        public string descripcion { get; set; }
        //public string condicion { get; set; }

    }

    public class BeneficiosDetalle {

        public string id { get; set; }
        public string codbeneficio {get;set;}
        public string descripcion { get; set; }
		public string silver { get; set; }
		public string gold { get; set; }
		//public string id_superior { get; set; }


	}
}