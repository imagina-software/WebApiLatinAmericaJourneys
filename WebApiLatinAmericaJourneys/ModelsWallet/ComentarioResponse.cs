﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
	public class ComentarioResponse
	{

		public string status { get; set; }
		public List<comentarios> comentarios { get; set; }
	}

	public class comentarios
	{
		public string id { get; set; }
		public string titulo { get; set; }
		public string descripcion { get; set; }
		public string fecha { get; set; }

	}



}