﻿using System.Collections.Generic;


namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class EncuestaRequest
    {
        public string user_id { get; set;}
        public string pregunta { get; set;}
        public string respuesta { get; set;}
        public char idioma { get;set;}
        public List<Marcado> marcado { get; set; }

    }
    public class Marcado
    {
        public string pregunta { get; set; }
        public string respuesta { get; set; }

    }
}