﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Permissions;
using System.Web;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class EncuestaResponse
    {
        public string status { get; set; }
        public string msg { get; set; }
        public string msgIngles { get; set; }

        public string videoEncuesta { get; set; }

        //public string VideoEncuesta { get; set; }

        public List<VideoEncuesta> video { get; set; }
		public List<PreguntaEncuesta> Preguntas { get; set; }

      
    }
}