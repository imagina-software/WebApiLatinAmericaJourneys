﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class LoginWRequest
    {
        public string uid { get; set; }
        public string pass { get; set; }

        public string usernameJWT { get; set; }
        public string passwordJWT { get; set; }

        public string uidGo { get; set; }
    }
}