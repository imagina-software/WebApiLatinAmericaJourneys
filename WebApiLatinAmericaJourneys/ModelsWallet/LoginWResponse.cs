﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class LoginWResponse    
    {
        public string status { get; set; }
        public string msg { get; set; }
		public string video_URL { get; set; }
		public string foto { get; set; }
		public string video_ejecu_URL { get; set; }
        public string nombre_ejecu{ get; set; }

        public string tokenJWT { get; set; }
        public List<Perfil> perfil { get; set; }

        public string cambioClave { get; set; }
        public string usuario { get; set; }
		public string FlagVisto { get; set; }

	//	public string titulo { get; set; }
		public string DescripcionPlan { get; set; }
		
	}

    public class Perfil
    {
        public string id { get; set; }
        public string nombre { get; set; }
        public string apellidos { get; set; }
        public string saldo { get; set; }
		public string TipoIdioma { get; set; }

	}


}