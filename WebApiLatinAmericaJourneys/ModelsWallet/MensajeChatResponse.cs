﻿using System;
using System.Collections.Generic;

using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class MensajeChatResponse
    {
		public string fecha { get; set; }
		public string status { get; set; }
        public List<Mensajes> mensajes { get; set; }
    }
}