﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
	public class NotificacionResponse
	{

		public string status { get; set; }
		public List<Notificacion> notificaciones { get; set; }
			   		 	  
	}


	public class Notificacion
	{
		public int id { get; set; }
		public string titulo { get; set; }
		public string contenido { get; set; }
		public string titulo_EN { get; set; }
		public string contenido_EN { get; set; }
		public string Fecha { get; set; }

	}





}