﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class NuevoViajeRequest
    {
        public string user_id { get; set; }
        public string rsp1 { get; set; }
        public string rsp2 { get; set; }
		public string rsp3 { get; set; }
		public string rsp4 { get; set; }
		public int ADT { get; set; }
		public int CHD { get; set; }


		//    public List<Incluye> incluye { get; set; }
		//}
		//public class Incluye
		//{
		//    public string nombre { get; set; }
		//    public string apPaterno { get; set; }
		//    public string apMaterno { get; set; }
		//    public string edad { get; set; }
		//}
	}
}