﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.ModelsWallet
{
    public class WalletResponse
    {
        public string status { get; set; }
        public string saldo { get; set; }
		public string movimiento { get; set; }
		public List<Movs> movs { get; set; }
    }

    public class Movs
    {
        public string fecha { get; set; }
        public string tipo { get; set; }
        public string importe { get; set; }
        public string concepto { get; set; }
        public string Ref { get; set; }

    }
}