﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class Cotiza
	{

		public CotizaResponse RegistrarCotizacion(string pPaquete, string pNombres, string pEmail, string pTelefono, string pPais, string pPreferencias, string pFecha, string pADT, string pCHD)
		{
			string lineagg = "0";

			CotizaResponse Cali = new CotizaResponse();
			Cali.registro = 0;

			try
			{
				List<CotizaResponse> lstPropuesta = new List<CotizaResponse>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Cotizacion_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@Paquete_id", SqlDbType.Int).Value = pPaquete;
					cmd.Parameters.Add("@Nombres", SqlDbType.VarChar).Value = pNombres;
					cmd.Parameters.Add("@Email ", SqlDbType.VarChar).Value = pEmail;
					cmd.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = pTelefono;
					cmd.Parameters.Add("@Pais", SqlDbType.Char).Value = pPais;
					cmd.Parameters.Add("@Preferencias", SqlDbType.VarChar).Value = pPreferencias;
					cmd.Parameters.Add("@fecha", SqlDbType.Date).Value = pFecha;
					cmd.Parameters.Add("@ADT", SqlDbType.VarChar).Value = pADT;
					cmd.Parameters.Add("@CHD", SqlDbType.VarChar).Value = pCHD;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Cali.registro = cmd.ExecuteNonQuery();
					Cali.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Cali;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}





	}
}