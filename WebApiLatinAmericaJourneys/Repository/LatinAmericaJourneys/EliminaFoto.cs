﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class EliminaFoto
	{

		 public EliminaFotoResponse EliminarFoto(int pId)
        {
			string lineagg = "0";

			EliminaFotoResponse Eli = new EliminaFotoResponse();
			Eli.registro = 0;
					   
			try
            {
                List<EliminaFotoRequest> lstLogin = new List<EliminaFotoRequest>();

                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_EliminaFoto", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@Idfoto", SqlDbType.VarChar).Value = pId;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
                    con.Open();

					Eli.registro = cmd.ExecuteNonQuery();
					Eli.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Eli;
			}
            catch (Exception ex)
            {
				throw new Exception(ex.Message);
			}

        }




	}
}