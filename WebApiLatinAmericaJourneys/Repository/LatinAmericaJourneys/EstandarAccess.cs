﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class EstandarAccess
	{

		public PerfilNewResponse RegistrarPublicacion(int pCodCliente, string pOrigen)
		{
			string lineagg = "0";

			PerfilNewResponse Cali = new PerfilNewResponse();
			Cali.registro = 0;

			try
			{
				List<GeneralRequest> lstPropuesta = new List<GeneralRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PublicaPost_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@Origen", SqlDbType.VarChar).Value = "M3";
		

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Cali.registro = cmd.ExecuteNonQuery();
					Cali.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Cali;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



	}
}