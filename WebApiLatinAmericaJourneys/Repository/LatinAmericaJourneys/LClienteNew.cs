﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LClienteNew
	{

		public int RegistrarCliente(string nombre, string ApetPat, string ApetMat, string Email, string TipoIdioma, string Telefono, string CodPais)
		{
			string lineagg = "0";
			string Status = " ";
			int CodCliente = 0;
					

			try
			{
				List<ClienteNewRequest> lstPropuesta = new List<ClienteNewRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_NuevoCliente_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = nombre;
					cmd.Parameters.Add("@ApetPat", SqlDbType.VarChar).Value = ApetPat;
					cmd.Parameters.Add("@ApetMat", SqlDbType.VarChar).Value = ApetMat;
					cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;
					cmd.Parameters.Add("@TipoIdioma", SqlDbType.VarChar).Value = TipoIdioma;
					cmd.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = Telefono;
					cmd.Parameters.Add("@CodPais", SqlDbType.VarChar).Value = CodPais;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					cmd.ExecuteNonQuery();

					Status = cmd.Parameters["@MsgTrans"].Value.ToString();
					CodCliente = Convert.ToInt32(cmd.Parameters["@CodCliente"].Value.ToString());


					con.Close();
				}
				lineagg += ",5";


				return CodCliente;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



		public ClienteNewResponse RegistrarCliente2(string nombre, string ApetPat, string ApetMat, string Email, string TipoIdioma, string TipoDoc, int NumDoc,string CodPLan, int Origen, string CodigoCompartir , string Telefono= "", string CodPais="999")
		{

			ClienteNewResponse Cali = new ClienteNewResponse();
			Cali.registro = 0;

			try
			{
				List<ClienteNewRequest> lstClienteNew = new List<ClienteNewRequest>();
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_NuevoCliente_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = nombre;
					cmd.Parameters.Add("@ApetPat", SqlDbType.VarChar).Value = ApetPat;
					cmd.Parameters.Add("@ApetMat", SqlDbType.VarChar).Value = ApetMat;
					cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = Email;
					cmd.Parameters.Add("@TipoIdioma", SqlDbType.VarChar).Value = TipoIdioma;
					cmd.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = Telefono;
					cmd.Parameters.Add("@CodPais", SqlDbType.VarChar).Value = CodPais;
					cmd.Parameters.Add("@TipoDoc", SqlDbType.VarChar).Value = TipoDoc;
					cmd.Parameters.Add("@NumDoc", SqlDbType.Int).Value = NumDoc;
					cmd.Parameters.Add("@CodPlan", SqlDbType.VarChar,20).Value = CodPLan;
					cmd.Parameters.Add("@Origen", SqlDbType.Int).Value = Origen;
					cmd.Parameters.Add("@CodCompartir", SqlDbType.VarChar, 20).Value = CodigoCompartir;


					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@MsgTransI", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					cmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoCliente", SqlDbType.VarChar, 150).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoEjecutiva", SqlDbType.VarChar, 150).Direction = ParameterDirection.Output;



					cmd.Parameters.Add("@Msg", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					con.Open();

					Cali.registro = cmd.ExecuteNonQuery();
					Cali.msg_EN = cmd.Parameters["@MsgTransI"].Value.ToString();
					Cali.msg_ES = cmd.Parameters["@MsgTrans"].Value.ToString();
					Cali.codCliente= cmd.Parameters["@CodCliente"].Value.ToString();
					Cali.status = cmd.Parameters["@Msg"].Value.ToString();

					Cali.usuario = cmd.Parameters["@Usuario"].Value.ToString();
					Cali.password = cmd.Parameters["@Password"].Value.ToString();
					Cali.correo = cmd.Parameters["@CorreoCliente"].Value.ToString();
					Cali.correoEjecutiva = cmd.Parameters["@CorreoEjecutiva"].Value.ToString(); 
					con.Close();
				}


				return Cali;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



	}
}