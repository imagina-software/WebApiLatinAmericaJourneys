﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;
using System.IO;
using System.Web;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LComentarios
	{

		public IEnumerable<comentarioPasajero> ObtieneComentarios(string idioma)
		{
			string lineagg = "0";
			try
			{
				List<comentarioPasajero> lstComentario = new List<comentarioPasajero>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaComent_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pidioma", SqlDbType.VarChar,1).Value = idioma;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						comentarioPasajero fComent = new comentarioPasajero();


						fComent.nombrePasajero = rdr["pasajero"].ToString();
						fComent.mensaje = rdr["mensaje"].ToString();
						fComent.fecha = Convert.ToDateTime(rdr["fecha"]).ToString("dd/MM/yyyy");
						fComent.titulo = rdr["titulo"].ToString();
						//fComent.clasificacion = rdr["clasificacion"].ToString();
						//fComent.imgEstrella = rdr["imgEstrella"].ToString();
						fComent.url_img = rdr["url_img"].ToString();
						fComent.id_coment = rdr["id_coment"].ToString();
						fComent.codpais = rdr["codpais"].ToString();
						fComent.nompais = rdr["NomPais"].ToString();


						lstComentario.Add(item: fComent);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstComentario;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public string GuardarImagenes(string imagen)
		{
			string NombreImagen = string.Empty;
			string NombreExtension = string.Empty;
			string UploadPathGG = string.Empty;
			string url_imagen = string.Empty;

			//NombreExtension = Path.GetExtension(imagen);

			if (imagen != null)
			{
				//string folderRoute = "xxx";

				NombreImagen = Path.GetFileNameWithoutExtension(imagen).Replace(" ", "") + "-" + DateTime.Now.ToString("ddMMyyhhmmss") + Path.GetExtension(imagen) + ".jpg";

				if (imagen.IndexOf("data:image/jpeg;base64,") == 0)
				{
					imagen = imagen.Replace("data:image/jpeg;base64,", "");
				}

				if (imagen.IndexOf("data:image/png;base64,") == 0)
				{
					imagen = imagen.Replace("data:image/png;base64,", "");
				}

				var bytesss = Convert.FromBase64String(imagen);
				UploadPathGG = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/images/Testimonios"), NombreImagen);
				url_imagen = $"https://www.pentagrama.com/webapilatinamericaclub/images/Testimonios/{NombreImagen}";
				//imagen.SaveA(url_imagen);

				using (var imageFile = new FileStream(UploadPathGG, FileMode.Create))
				{
					imageFile.Write(bytesss, 0, bytesss.Length);
					imageFile.Flush();
				}


				return url_imagen;

			}

			return url_imagen;

		}
          



		public ComentariosResponse RegistrarComentario(string codigoCliente, string comentario, string url_img)
		{
			string lineagg = "0";
			ComentariosResponse Coment = new ComentariosResponse();
			Coment.registro = 0;

			try
			{
				List<ComentarioRequest> lstNuevoComent = new List<ComentarioRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_NewComentPax_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					//cmd.Parameters.Add("@CodViaje", SqlDbType.Int).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = codigoCliente;
					cmd.Parameters.Add("@pMsg", SqlDbType.VarChar).Value = comentario;
					cmd.Parameters.Add("@pUrl_img", SqlDbType.NVarChar).Value = url_img;


					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status =cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";

				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}




	}
}