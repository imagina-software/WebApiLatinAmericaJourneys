﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LDestino
	{

		public IEnumerable<Destinos> LeerMisDestino(int pCodCliente)
		{

			string lineagg = "0";

			try
			{

				List<Destinos> lstDestinos = new List<Destinos>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_LeeMisDestinos_S", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@User", SqlDbType.Int).Value = pCodCliente;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";
						Destinos fDestinos= new Destinos();

						fDestinos.id = rdr["ID"].ToString();
						fDestinos.title = rdr["TITLE"].ToString();
						fDestinos.last_msg = rdr["LAST_MSG"].ToString();
						fDestinos.img_url = rdr["IMG_URL"].ToString();
						fDestinos.estado = rdr["STATUS"].ToString();
						lstDestinos.Add(item: fDestinos);
					}

					lineagg += ",5";
					con.Close();
				}
				return lstDestinos;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<PaisDestino> ObtienePaisDestino(int pId_Destino)
		{

			string lineagg = "0";

			try
			{

				List<PaisDestino> lstDestinos = new List<PaisDestino>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaDestinosPaises_L", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@id_destino", SqlDbType.Int).Value = pId_Destino;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";
						PaisDestino fDestinos = new PaisDestino();

						fDestinos.id = rdr["ID"].ToString();
						fDestinos.nombre = rdr["NOMBRE"].ToString();
						fDestinos.img_url = rdr["IMG_URL"].ToString();

						lstDestinos.Add(item: fDestinos);
					}

					lineagg += ",5";
					con.Close();
				}
				return lstDestinos;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}




	}
}