﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LDetalleHoteles
	{
		public IEnumerable<DetalleHotel> LeerDetalleHotel(int pNroServicio)
		{

			string lineagg = "0";
			DetalleHotelRequest Hotel = new DetalleHotelRequest();
			Hotel.status = "";
			try
			{
				List<DetalleHotel> lsthotel = new List<DetalleHotel>();

				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.VTA_ListaIMG_S", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroServicio", SqlDbType.Int).Value = pNroServicio;
					//cmd.Parameters.Add("@Idioma", SqlDbType.VarChar).Value = pIdioma;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();

					Hotel.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";


						DetalleHotel fDetalleHotel = new DetalleHotel
						{
							Imagen1 = rdr["Imagen1"].ToString(),
							Imagen2 = rdr["Imagen2"].ToString(),
							Imagen3 = rdr["Imagen3"].ToString(),
							DireccionHTL = rdr["DireccionHTL"].ToString(),
							Telefono = rdr["Telefono"].ToString(),
							Valoracion = Convert.ToInt32(rdr["Valoracion"]),
							NombreHTL = rdr["NombreHTL"].ToString(),
							DescripcionHTL = rdr["DesHTL"].ToString(),
							DescripcionHTLI = rdr["DesHTLI"].ToString(),
							Flag = rdr["Flag"].ToString(),
						};

						lsthotel.Add(item: fDetalleHotel);

					}
					lineagg += ",5";
					con.Close();
				}

				return lsthotel;

			}
			catch (Exception ex)
			{

				throw new Exception { Source = lineagg };

			}


		}


		public IEnumerable<DetalleHotel> LeerDetalleHotel2(int pNroServicio, string pIdioma)
		{

			string lineagg = "0";
			DetalleHotelRequest Hotel = new DetalleHotelRequest();
			Hotel.status = "";
			try
			{
				List<DetalleHotel> lsthotel = new List<DetalleHotel>();

				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.TA_ListaIMG_S2", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroServicio", SqlDbType.Int).Value = pNroServicio;
					cmd.Parameters.Add("@Idioma", SqlDbType.VarChar).Value = pIdioma;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();

					Hotel.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";


						DetalleHotel fDetalleHotel = new DetalleHotel
						{
							Imagen1 = rdr["Imagen1"].ToString(),
							Imagen2 = rdr["Imagen2"].ToString(),
							Imagen3 = rdr["Imagen3"].ToString(),
							DireccionHTL = rdr["DireccionHTL"].ToString(),
							Telefono = rdr["Telefono"].ToString(),
							Valoracion = Convert.ToInt32(rdr["Valoracion"]),
							NombreHTL = rdr["NombreHTL"].ToString(),
							DescripcionHTL = rdr["DesHTL"].ToString(),
							Flag = rdr["Flag"].ToString(),
						};

						lsthotel.Add(item: fDetalleHotel);

					}
					lineagg += ",5";
					con.Close();
				}

				return lsthotel;

			}
			catch (Exception ex)
			{

				throw new Exception { Source = lineagg };

			}


		}

	}
}