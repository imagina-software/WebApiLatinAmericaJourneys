﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.Utility;
using System.ComponentModel.DataAnnotations;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
    public class LFotoRegistro
    {

        public FotoRegistroResponse RegistrarFoto(int pNropedido, int pNroPropuesta, int pNroVersion, string pFototitulo, string pFotocomment,  string pFotocontent)
        {
            string lineagg = "0";
            string url = string.Empty;

            File objFile = new File();

            url = objFile.GuardarImagenes(pFotocontent, pNropedido, pNroPropuesta, pNroVersion);

            FotoRegistroResponse Foto = new FotoRegistroResponse();
            Foto.status = "";

            try
            {
                List<FotoRegistroRequest> lstFotoRegistro = new List<FotoRegistroRequest>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ImagenAlbum_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pNroPedido", SqlDbType.Int).Value = pNropedido;
                    cmd.Parameters.Add("@pNroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
                    cmd.Parameters.Add("@pNroVersion", SqlDbType.Int).Value = pNroVersion;
                    cmd.Parameters.Add("@pFotoTitulo", SqlDbType.VarChar, 50).Value = pFototitulo;
                    cmd.Parameters.Add("@pFotoComment", SqlDbType.VarChar, 50).Value = pFotocomment;
                    //cmd.Parameters.Add("@pFotoContent", SqlDbType.VarChar, 8000).Value = pFotocontent;
                    cmd.Parameters.Add("@pUrl", SqlDbType.VarChar, 8000).Value = url;

                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@pIdFoto", SqlDbType.Int).Direction = ParameterDirection.Output;


                    lineagg += ",2";
                    con.Open();

                    cmd.ExecuteNonQuery();
                    Foto.status = cmd.Parameters["@MsgTrans"].Value.ToString();
                    Foto.idfoto= cmd.Parameters["@pIdFoto"].Value.ToString();

                    con.Close();
                }
                lineagg += ",5";


                return Foto;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

    }
}