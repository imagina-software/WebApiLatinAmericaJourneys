﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;
using System.IO;
using System.Web;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LHotelInfo
	{

		public IEnumerable<hotelInformacion> ObtieneHotelInfo(int pNroPedido, int pNroPropuesta, int pNroVersion)
		{
			string lineagg = "0";
			try
			{
				List<hotelInformacion> lstHtl = new List<hotelInformacion>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))


				{

					SqlCommand cmd = new SqlCommand();

					if (pNroVersion == 0)
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaHotel_S", con);

					}
					else
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_VersionHotel_S", con);
						cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
					}




					//SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_VersionHotel_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					//cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						hotelInformacion fHtl = new hotelInformacion();


						fHtl.NomCiudad = rdr["NomCiudad"].ToString();
						fHtl.NombreHotel = rdr["Titulo"].ToString();
						fHtl.Telefono = rdr["Telefono1"].ToString();
						//fHtl.NomPagina = rdr["titulo"].ToString();
					
						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public IEnumerable<staffInfo> ObtieneStaffInfoAntes(int pNroPedido, string pIdioma)
		{
			string lineagg = "0";
			try
			{
				List<staffInfo> lstHtl = new List<staffInfo>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_InformacionStaff_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@FlagIdioma", SqlDbType.Char).Value = pIdioma;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						staffInfo fHtl = new staffInfo();


						fHtl.NomVendedor = rdr["NomVendedor"].ToString();
						fHtl.Cargo = rdr["Cargo"].ToString();
						fHtl.TelefonoEmergencia = rdr["TfEmergencia"].ToString();
						//fHtl.NomPagina = rdr["titulo"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public IEnumerable<videoInfo> ObtieneVideoInfoAntes(int pNroPedido, string pIdioma)
		{
			string lineagg = "0";
			try
			{
				List<videoInfo> lstHtl = new List<videoInfo>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_InformacionVideo_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@FlagIdioma", SqlDbType.Char).Value = pIdioma;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						videoInfo fHtl = new videoInfo();

						fHtl.VideoUrl = rdr["VideoURL"].ToString();
						fHtl.VideoTitulo = rdr["VideoTitulo"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}




		public IEnumerable<climaInfo> ObtieneClimaInfoAntes(int pNroPedido, int pNroPropuesta, int pNroVersion)
		{
			string lineagg = "0";
			try
			{
				List<climaInfo> lstHtl = new List<climaInfo>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand();

					//SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_VersionInformacionClima_S", con);


					if (pNroVersion == 0)
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaInformacionClima_S", con);

					}
					else
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_VersionInformacionClima_S", con);
						cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
					}


					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					//cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						climaInfo fHtl = new climaInfo();

						fHtl.NomCiudad = rdr["NomCiudad"].ToString();
						fHtl.MesAnio = rdr["MesAno"].ToString();
						fHtl.TempMinima = rdr["TempMinima"].ToString();
						fHtl.TempMaxima = rdr["TempMaxima"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<infoViaje> ObtieneInfoViaje(int pNroPedido, int pNroPropuesta, int pNroVersion)
		{
			string lineagg = "0";
			try
			{
				List<infoViaje> lstHtl = new List<infoViaje>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand();

					if (pNroVersion == 0)
					{
						 cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaInformacionOtrosC_S", con);

					}
					else
					{
						 cmd = new SqlCommand("latinamericajourneys.LAJ_VersionInformacionOtrosC_S", con);
						cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
					}

					
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;					




					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						infoViaje fHtl = new infoViaje();
						
						fHtl.NroTipoInfo = rdr["NroTipoInf"].ToString();
						//fHtl.OrdenDet = Convert.ToInt32(rdr["OrdenTipo"]);
						//fHtl.OrdenDet = Convert.ToInt32(rdr["OrdenDet"]);
						fHtl.NomInfo = rdr["NomInf"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public IEnumerable<Descripcion> ObtieneInfoViaje2(int pNroPedido, int pNroPropuesta, int pNroVersion, string NroTipoInfo)
		{
			string lineagg = "0";
			try
			{
				List<Descripcion> lstHtl = new List<Descripcion>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand();

					if (pNroVersion == 0)
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaInformacionOtros_S", con);

					}
					else
					{
						cmd = new SqlCommand("latinamericajourneys.LAJ_VersionInformacionOtros_S", con);
						cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
						//cmd.Parameters.Add("@NroTipoInfo", SqlDbType.VarChar).Value = NroTipoInfo;
					}


					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					cmd.Parameters.Add("@NroTipoInfo", SqlDbType.VarChar).Value = NroTipoInfo;



					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						Descripcion fHtl = new Descripcion();

						fHtl.NroTipoInfo = rdr["NroTipoInf"].ToString();
						fHtl.OrdenDet = Convert.ToInt32(rdr["OrdenTipo"]);
						fHtl.OrdenDet = Convert.ToInt32(rdr["OrdenDet"]);
						fHtl.NomInfo = rdr["NomInf"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}




		public IEnumerable<resumen> ObtieneResumen(int pNroPedido, int pNroPropuesta)
		{
			string lineagg = "0";
			try
			{
				List<resumen> lstHtl = new List<resumen>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))


				{

					SqlCommand cmd = new SqlCommand();

			
						cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaNroPropuesta_S", con);
						cmd.CommandType = CommandType.StoredProcedure;
						cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
						cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						resumen fHtl = new resumen();


						fHtl.caractEspeciales = rdr["Resumen"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public IEnumerable<requerimiento> ObtieneRequerimiento(int pNroPedido, string pIdioma)
		{
			string lineagg = "0";
			try
			{
				List<requerimiento> lstHtl = new List<requerimiento>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))


				{

					SqlCommand cmd = new SqlCommand();


					cmd = new SqlCommand("latinamericajourneys.LAJ_InformacionDocReq_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@FlagIdioma", SqlDbType.VarChar).Value = pIdioma;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						requerimiento fHtl = new requerimiento();


						fHtl.ReqVisitaPeru = rdr["NomInf"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}






	}
}