﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LPaises
	{

		public IEnumerable<paises> ObtienePaises()
		{
			string lineagg = "0";
			try
			{
				List<paises> lstPaises = new List<paises>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPaises_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						paises fPais = new paises();


						fPais.codPais = rdr["codPais"].ToString();
						//titulo = rdr["Titulo"].ToString(),
						fPais.nomPais = rdr["nomPais"].ToString();

						lstPaises.Add(item: fPais);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPaises;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



	}
}