﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LPlanea
	{

		public IEnumerable<planea> ObtienePlanes(string pCodCliente, string pCodPais)
		{
			string lineagg = "0";
			try
			{
				List<planea> lstPlanea = new List<planea>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPlan_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@pCodPais", SqlDbType.VarChar).Value = pCodPais;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						planea fPlanea = new planea();


						fPlanea.desPregunta = rdr["desPregunta"].ToString();
						fPlanea.URL_Video = rdr["URL_Video"].ToString();
						fPlanea.respuesta = rdr["respuesta"].ToString();

						lstPlanea.Add(item: fPlanea);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPlanea;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<Entities.PodcastVideo> ObtienePodcastVideo(string pCodCliente, string pCodPais)
		{
			try
			{
				List<Entities.PodcastVideo> lstPodcastVideo = new List<Entities.PodcastVideo>();
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPodcastVideo_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@pCodPais", SqlDbType.VarChar).Value = pCodPais;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					con.Open();
					cmd.ExecuteNonQuery();

					SqlDataReader rdr = cmd.ExecuteReader();
					while (rdr.Read())
					{

						Entities.PodcastVideo fPodcastvideo = new Entities.PodcastVideo();

						fPodcastvideo.Descripcion = rdr["Descripcion"].ToString();
						fPodcastvideo.Titulo = rdr["Titulo"].ToString();
						fPodcastvideo.CodPais = rdr["CodPais"].ToString();
						fPodcastvideo.URL_Video = rdr["URL"].ToString();
						fPodcastvideo.Idioma = rdr["Idioma"].ToString();
						fPodcastvideo.Tipo = rdr["Tipo"].ToString();
						fPodcastvideo.URL_IMG = rdr["URL_IMG"].ToString();


						lstPodcastVideo.Add(item: fPodcastvideo);

					}

					con.Close();
				}
				return lstPodcastVideo;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = ex.ToString() };
			}

		}


	}
}