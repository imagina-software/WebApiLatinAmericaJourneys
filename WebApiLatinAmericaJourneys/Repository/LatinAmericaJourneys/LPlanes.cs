﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LPlanes
	{

		public IEnumerable<planes> ObtienePlanesCli()
		{
			string lineagg = "0";
			string demo1 = "";
			//string demo2 = "";
			try
			{
				List<planes> lstPlanes = new List<planes>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtienePlanes_L", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						planes fPlanes = new planes();


						fPlanes.id = rdr["id"].ToString();
						fPlanes.codPlan = rdr["CodPlan"].ToString();
						fPlanes.tipo = rdr["Tipo"].ToString();
						fPlanes.titulo = rdr["Titulo"].ToString();
						//fPlanes.titulo = rdr["precio"].ToString();
						fPlanes.precio = Convert.ToDouble(rdr["precio"]);

						lstPlanes.Add(item: fPlanes);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPlanes;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public PerfilNewResponse ActualizarPlan(string pCodCliente, string CodPlan)
		{
			string lineagg = "0";

			PerfilNewResponse Coment = new PerfilNewResponse();
			Coment.registro = 0;

			try
			{
				List<PerfilNewRequest> lstPerfil = new List<PerfilNewRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_CambioPlan_U", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar).Value = pCodCliente;
					cmd.Parameters.Add("@CodPlan", SqlDbType.VarChar).Value = CodPlan;
	
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoEjecutiva", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoCliente", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@NomCliente", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status = cmd.Parameters["@MsgTrans"].Value.ToString();
					Coment.correoEjecutiva = cmd.Parameters["@CorreoEjecutiva"].Value.ToString();
					Coment.correoCliente = cmd.Parameters["@CorreoCliente"].Value.ToString();
					Coment.nomCliente = cmd.Parameters["@NomCliente"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



		public PerfilNewResponse RegistrarPreferencia(int pCodCliente, string pOrigen, int ID, int pEstado, string pCodPais)
		{
			string lineagg = "0";

			PerfilNewResponse Cali = new PerfilNewResponse();
			Cali.registro = 0;

			try
			{
				List<WishListRequest> lstPropuesta = new List<WishListRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_WishList_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@Origen", SqlDbType.VarChar).Value = "DID";
					cmd.Parameters.Add("@ID", SqlDbType.Int).Value = ID;
					cmd.Parameters.Add("@Estado", SqlDbType.Int).Value = pEstado;
					cmd.Parameters.Add("@CodPais", SqlDbType.VarChar).Value = pCodPais;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Cali.registro = cmd.ExecuteNonQuery();
					Cali.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Cali;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}
		

		public IEnumerable<WishList> ObtieneLista(int pCodCliente)
		{
			string lineagg = "0";
			string demo1 = "";
			//string demo2 = "";
			try
			{
				List<WishList> lstPlanes = new List<WishList>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneWish_L", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						WishList fPlanes = new WishList();


						fPlanes.id = Convert.ToInt32(rdr["id"]);
				

						lstPlanes.Add(item: fPlanes);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPlanes;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}








	}
}