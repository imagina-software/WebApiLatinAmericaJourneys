﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;



namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class LPodcats
	{

		public IEnumerable<PodCasts> ObtienePodcats(string pDestino, string pIdioma)
		{
			string lineagg = "0";
			try
			{
				List<PodCasts> lstPodcasts = new List<PodCasts>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPodcats_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pDestino", SqlDbType.VarChar).Value = pDestino;
					cmd.Parameters.Add("@pIdioma", SqlDbType.VarChar).Value = pIdioma;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						PodCasts fPodcats = new PodCasts();

						fPodcats.Descripcion = rdr["Descripcion"].ToString();
						fPodcats.URL = rdr["URL"].ToString();
						fPodcats.Titulo = rdr["Titulo"].ToString();
						fPodcats.Pais = rdr["Pais"].ToString();
						fPodcats.URL_IMG = rdr["URL_IMG"].ToString();

						lstPodcasts.Add(item: fPodcats);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPodcasts;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}









	}
}