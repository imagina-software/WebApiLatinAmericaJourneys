﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.Utility;


namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
    public class LPropuesta
    {

        public IEnumerable<PropuestaResponse> LeerPropuesta(int pCodCliente, string pZonaVenta)
        {
            string lineagg = "0";

            try
            {
                List<PropuestaResponse> lstPropuesta = new List<PropuestaResponse>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Propuesta_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;
                    cmd.Parameters.Add("@CodZonaVta", SqlDbType.Char).Value = pZonaVenta;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";
                        
                            PropuestaResponse fPropuesta = new PropuestaResponse
                            {
                                fchSys         = rdr["FchSys"].ToString(),
                                fchInicio      = rdr["FchInicio"].ToString(),
                                nroPrograma    = rdr["NroPrograma"].ToString(),
                                stsPrograma    = rdr["StsPrograma"].ToString(),
                                desPrograma    = rdr["DesPrograma"].ToString(),
                                cantDias       = rdr["CantDias"].ToString(),
                                emailVendedor  = rdr["EmailVendedor"].ToString(),
                                keyReg         = rdr["KeyReg"].ToString(),
                                nroPedido      = rdr["NroPedido"].ToString(),
                                nroPropuesta   = rdr["NroPropuesta"].ToString(),
                                nroVersion     = rdr["NroVersion"].ToString(),
                            };

                        lstPropuesta.Add(item: fPropuesta);

                         
                    }
                    lineagg += ",5";
                    con.Close();
                }
                return lstPropuesta;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }


        public IEnumerable<ProgramaViaje> LeerPropuestaViaje(int pCodCliente, string pCodEstado,string pNroPropuesta)
        {
            string lineagg = "0";

            try
            {
                List<ProgramaViaje> lstPropuesta = new List<ProgramaViaje>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Propuesta2_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.VarChar, 2).Value = pNroPropuesta;
					cmd.Parameters.Add("@CodEstado", SqlDbType.VarChar, 1).Value = pCodEstado;
					


					lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";

                        ProgramaViaje fPropuesta = new ProgramaViaje
                        {

                            FchInicio = rdr["FchInicio"].ToString(),
                            NroPrograma = rdr["NroPrograma"].ToString(),
                            DesPrograma = rdr["DesPrograma"].ToString(),
                            CantDias = rdr["CantDias"].ToString(),
                            EmailVendedor = rdr["EmailVendedor"].ToString(),
                            NroPedido = rdr["NroPedido"].ToString(),
                            NroPropuesta = rdr["NroPropuesta"].ToString(),
                            NroVersion = rdr["NroVersion"].ToString(),
                            Stars = rdr["Stars"].ToString(),
                            TipoIdioma = rdr["TipoIdioma"].ToString()
							//NroServicio = Convert.ToInt32(rdr["Nroservicio"])
						};

                        lstPropuesta.Add(item: fPropuesta);


                    }
                    lineagg += ",5";
                    con.Close();
                }
                return lstPropuesta;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }

        public IEnumerable<UltimaPublicacion> LeeUltimaPublicacion(int pCodCliente)
        {

            try
            {

                List<UltimaPublicacion> lstPublicacion = new List<UltimaPublicacion>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {


                    SqlCommand cmd = new SqlCommand("peru4me_new.P4I_PublicaUltimo_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodZonaVta", SqlDbType.Char, 3).Value = "PER";
                    cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pCodCliente;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        UltimaPublicacion fpublicacion = new UltimaPublicacion
                        {

                            nroPedido = Convert.ToInt32(rdr["NroPedido"]),
                            nroPropuesta = Convert.ToInt32(rdr["NroPropuesta"]),
                            nroVersion = Convert.ToInt32(rdr["NroVersion"]),
                            flagIdioma = Convert.ToChar(rdr["FlagIdioma"].ToString()),
                            cantPropuestas = Convert.ToInt32(rdr["CantPropuestas"])

                        };

                        lstPublicacion.Add(item: fpublicacion);

                    }

                    con.Close();
                }

                return lstPublicacion;

            }
            catch (Exception ex)
            {

                throw;

            }


        }

        public IEnumerable<Programa2> ObtenerListadoPropuesta(int pNroPedido, char pFlagIdioma)
        {
            try
            {
                List<Programa2> lstfprograma = new List<Programa2>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand();

                    if (pFlagIdioma.Equals(ConstantesWeb.CHR_IDIOMA_INGLES))
                    {
                        cmd = new SqlCommand("peru4me_new.P4I_Publica_S", con);

                    }
                    else
                    {
                        cmd = new SqlCommand("peru4me_new.P4E_Publica_S", con);
                    }

         

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@CodZonaVta", SqlDbType.Char, 3).Value = "PER";
                    cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
    

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Programa2 fprograma = new Programa2
                        {

                            id = rdr["NroPrograma"].ToString().Trim(),
                            estado = rdr["StsPrograma"].ToString().Trim(),
                            nombre = rdr["DesPrograma"].ToString().Trim(),
                            codEstado = rdr["CodEstado"].ToString().Trim()



                        };

                        lstfprograma.Add(item: fprograma);
                    }

                    con.Close();
                }

                return lstfprograma;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public int ObtenerCodClientexAcceso(string pCodUsuario)
        {

            int codCliente = 0;

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneClienteAcceso_S", con);
                    cmd.CommandType = CommandType.StoredProcedure;
  
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar).Value = pCodUsuario;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Direction = ParameterDirection.Output;


                    con.Open();
                    cmd.ExecuteNonQuery();

                    codCliente = Convert.ToInt32(cmd.Parameters["@CodCliente"].Value);

           

                    con.Close();
                }

                return codCliente;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


    }
}