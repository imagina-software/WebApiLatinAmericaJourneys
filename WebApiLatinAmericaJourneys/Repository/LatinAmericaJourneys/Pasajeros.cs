﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;
using System.IO;
using System.Web;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class Pasajeros
	{

		public IEnumerable<pasajeroInfo> ObtienePasajeroInfo(int pNroPedido)
		{
			string lineagg = "0";
			try
			{
				List<pasajeroInfo> lstHtl = new List<pasajeroInfo>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Pasajeros_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;


					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						pasajeroInfo fHtl = new pasajeroInfo();


						fHtl.NomPasajero = rdr["NomPasajero"].ToString();
						fHtl.ApePasajero = rdr["ApePasajero"].ToString();
						fHtl.FchNacimiento = Convert.ToDateTime(rdr["FchNacimiento"]).ToString("dd/MM/yyyy");
						fHtl.Pasaporte = rdr["Pasaporte"].ToString();
						fHtl.Pais = rdr["Pais"].ToString();
						fHtl.Pasajero = rdr["TipoPasajero"].ToString();
						//fHtl.NomPagina = rdr["titulo"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}



		public PasajeroInfoResponse RegistrarPasajero(int pNumPasajero, string pNombre, string pApe, string pPasaporte, string pFecNac, string pNacionalidad, int pNroPedido, string pTipo, string pGenero, string pObservacion)
		{
			string lineagg = "0";
			PasajeroInfoResponse Coment = new PasajeroInfoResponse();
			Coment.registro = 0;

			try
			{
				List<PasajeroRequest> lstNuevoComent = new List<PasajeroRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Pasajero_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					//cmd.Parameters.Add("@CodViaje", SqlDbType.Int).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPasajero", SqlDbType.Int).Value = pNumPasajero;
					cmd.Parameters.Add("@NomPasajero", SqlDbType.VarChar,250).Value = pNombre;
					cmd.Parameters.Add("@ApPasajero", SqlDbType.VarChar, 250).Value = pApe;
					cmd.Parameters.Add("@Pasaporte", SqlDbType.VarChar).Value = pPasaporte;
					cmd.Parameters.Add("@CodNacionalidad", SqlDbType.VarChar).Value = pNacionalidad;
					cmd.Parameters.Add("@FchNacimiento", SqlDbType.VarChar).Value = pFecNac;
					cmd.Parameters.Add("@CodGenero", SqlDbType.VarChar).Value = pGenero;
					cmd.Parameters.Add("@TipoPasajero", SqlDbType.VarChar).Value = pTipo;
					cmd.Parameters.Add("@Observacion", SqlDbType.VarChar).Value = pObservacion;

					
					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";

				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}




	}
}