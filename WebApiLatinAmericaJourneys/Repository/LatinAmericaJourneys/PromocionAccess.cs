﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Entities;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.Utility;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
    public class PromocionAccess
    {

        public IEnumerable<Promocion> ObtenerListadoPromocion(string pIdPais, string pIdioma)
        {

            try
            {
                List<Promocion> lstPromocion = new List<Promocion>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand();                   
                    cmd = new SqlCommand("latinamericajourneys.LAJ_ObtienePromociones_S", con);
              
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@IdPais", SqlDbType.Char).Value = pIdPais;
                    cmd.Parameters.Add("@Idioma", SqlDbType.Char,1).Value = pIdioma;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;


                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Promocion fprograma = new Promocion
                        {

                            IdPromocion = Convert.ToInt32(rdr["IdPromocion"]),
                            Titulo = rdr["Titulo"].ToString().Trim(),
                            Descripcion = rdr["Descripcion"].ToString().Trim(),
                            Estado = Convert.ToBoolean(rdr["Estado"]),
                            Foto = rdr["Foto"].ToString().Trim(),
                            PrecioRegular = Convert.ToDecimal(rdr["PrecioRegular"]),
                            Opc1 = rdr["Opc1"].ToString().Trim(),
                            Opc2 = rdr["Opc2"].ToString().Trim(),
							Ciudad = rdr["Ciudad"].ToString().Trim(),

						};

                        lstPromocion.Add(item: fprograma);
                    }

                    con.Close();
                }

                return lstPromocion;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


        public IEnumerable<PromocionPais> ObtenerListadoPromocionxPais(int pIdPromocion, string pIdPais)
        {

            try
            {
                List<PromocionPais> lstPromocion = new List<PromocionPais>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd = new SqlCommand("latinamericajourneys.LAJ_ObtienePromocionesxPais_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@IdPromocion", SqlDbType.Int).Value = pIdPromocion;
                    cmd.Parameters.Add("@IdPais", SqlDbType.Char, 3).Value = pIdPais;

                                        cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;


                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        PromocionPais fpromocion = new PromocionPais
                        {

                            IdPlanSuperior = Convert.ToInt32(rdr["IdPlanSuperior"]),
                            Plan = rdr["Descripcion"].ToString(),
                            Precio = Convert.ToDecimal(rdr["Precio"])
                    
                        };

                        lstPromocion.Add(item: fpromocion);
                    }

                    con.Close();
                }

                return lstPromocion;

            }
            catch (Exception ex)
            {
                throw;
            }
        }


		public PromocionesxCliResponse InsertarPromociones(string pCodCliente, int IdPromocion)
		{
			string lineagg = "0";

			PromocionesxCliResponse Coment = new PromocionesxCliResponse();
			Coment.registro = 0;

			try
			{
				List<PromocionesxCliRequest> lstPerfil = new List<PromocionesxCliRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PromocionesxCli_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar).Value = pCodCliente;
					cmd.Parameters.Add("@IdPromocion", SqlDbType.VarChar).Value = IdPromocion;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoEjecutiva", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoCliente", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@NomCliente", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@NomPromocion", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status = cmd.Parameters["@MsgTrans"].Value.ToString();
					Coment.correoEjecutiva = cmd.Parameters["@CorreoEjecutiva"].Value.ToString();
					Coment.correoCliente = cmd.Parameters["@CorreoCliente"].Value.ToString();
					Coment.nomCliente = cmd.Parameters["@NomCliente"].Value.ToString();
					Coment.nomTarifa = cmd.Parameters["@NomPromocion"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}





	}
}