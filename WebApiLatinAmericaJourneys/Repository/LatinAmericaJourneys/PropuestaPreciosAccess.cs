﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class PropuestaPreciosAccess
	{
		public IEnumerable<Precio> ObtienePrecios(int pNroPedido, int pNroPropuesta)
		{

			string lineagg = "0";

			try
			{

				List<Precio> lstPrecio = new List<Precio>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PropuestaPrecio_S", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";


						Precio fPrecioPropuesta = new Precio  
						{
							DesOrden = rdr["DesOrden"].ToString(),
					     	PrecioxPersona = Convert.ToInt32(rdr["PrecioxPersona"]),
							CantPersonas = Convert.ToInt32(rdr["CantPersonas"]),
							PrecioTotal = Convert.ToInt32(rdr["PrecioTotal"]),
						};

						lstPrecio.Add(item: fPrecioPropuesta);

					}
					lineagg += ",5";
					con.Close();
				}

				return lstPrecio;

			}
			catch (Exception ex)
			{

				throw new Exception { Source = lineagg };

			}


		}



		public IEnumerable<ResumenIti> ObtieneCaracteristicas(int pNroPedido, int pNroPropuesta, string pNroVersion, string pIdioma)
		{

			string lineagg = "0";

			try
			{

				List<ResumenIti> lstResumen = new List<ResumenIti>();

				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Obtener_Resumen_S", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					cmd.Parameters.Add("@FlagIdioma", SqlDbType.VarChar).Value = pIdioma;
					cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;


					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",5";

						ResumenIti fResumen = new ResumenIti
						{

							Resumen = rdr["Resumen"].ToString(),
	
						};

						lstResumen.Add(item: fResumen);

					}

					lineagg += ",5";
					con.Close();
				}

				return lstResumen;

			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}




    }
}