﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Entities;
using System.IO;
using System.Web;

namespace WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys
{
	public class ReservaInfo
	{


		public IEnumerable<pasajero> ObtienePasajeroInfo(int pNroPedido)
		{
			string lineagg = "0";
			try
			{
				List<pasajero> lstHtl = new List<pasajero>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Pasajeros_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
		

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						pasajero fHtl = new pasajero();


						fHtl.NomPasajero = rdr["NomPasajero"].ToString();
						fHtl.ApePasajero = rdr["ApePasajero"].ToString();
						fHtl.FchNacimiento = Convert.ToDateTime(rdr["FchNacimiento"]).ToString("dd/MM/yyyy");
						fHtl.Pasaporte = rdr["Pasaporte"].ToString();
						fHtl.Pais = rdr["Pais"].ToString();
						fHtl.Pasajero = rdr["TipoPasajero"].ToString();
						//fHtl.NomPagina = rdr["titulo"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<boletoTerrestre> ObtieneTerrestreInfo(int pNroPedido,int pNroPropuesta, int pNroVersion, int pCodTipoServicio )
		{
			string lineagg = "0";
			try
			{
				List<boletoTerrestre> lstHtl = new List<boletoTerrestre>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ReservaAereo_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
					cmd.Parameters.Add("@CodTipoServicio", SqlDbType.Int).Value = pCodTipoServicio;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						boletoTerrestre fHtl = new boletoTerrestre();


						fHtl.FchVuelo = Convert.ToDateTime(rdr["FchVuelo"]).ToString("dd/MM/yyyy");
						fHtl.RutaVuelo = rdr["RutaVuelo"].ToString();
						fHtl.Aerolinea = rdr["Aerolinea"].ToString();
						fHtl.NroVuelo = rdr["NroVuelo"].ToString();
						fHtl.HoraSalida = rdr["HoraSalida"].ToString();
						fHtl.HoraLlegada = rdr["HoraLlegada"].ToString();
						fHtl.DesCantidad = rdr["DesCantidad"].ToString();
						fHtl.CodStsReserva = rdr["CodStsReserva"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<boletoAereo> ObtieneAereoInfo(int pNroPedido, int pNroPropuesta, int pNroVersion, int pCodTipoServicio)
		{
			string lineagg = "0";
			try
			{
				List<boletoAereo> lstHtl = new List<boletoAereo>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ReservaAereo_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;
					cmd.Parameters.Add("@CodTipoServicio", SqlDbType.Int).Value = pCodTipoServicio;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						boletoAereo fHtl = new boletoAereo();


						fHtl.FchVuelo = Convert.ToDateTime(rdr["FchVuelo"]).ToString("dd/MM/yyyy");
						fHtl.RutaVuelo = rdr["RutaVuelo"].ToString();
						fHtl.Aerolinea = rdr["Aerolinea"].ToString();
						fHtl.NroVuelo = rdr["NroVuelo"].ToString();
						fHtl.HoraSalida = rdr["HoraSalida"].ToString();
						fHtl.HoraLlegada = rdr["HoraLlegada"].ToString();
						fHtl.CodReserva = rdr["CodReserva"].ToString();
						fHtl.DesCantidad = rdr["DesCantidad"].ToString();
						fHtl.CodStsReserva = rdr["CodStsReserva"].ToString();

						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}


		public IEnumerable<hotel> ObtieneHotelInfo(int pNroPedido, int pNroPropuesta, int pNroVersion)
		{
			string lineagg = "0";
			try
			{
				List<hotel> lstHtl = new List<hotel>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ReservaHotelFact_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@NroPedido", SqlDbType.Int).Value = pNroPedido;
					cmd.Parameters.Add("@NroPropuesta", SqlDbType.Int).Value = pNroPropuesta;
					cmd.Parameters.Add("@NroVersion", SqlDbType.Int).Value = pNroVersion;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						hotel fHtl = new hotel();


						fHtl.NomCiudad = rdr["NomCiudad"].ToString();
						fHtl.Hotel = rdr["Hotel"].ToString();
						fHtl.Telefono1 = rdr["Telefono1"].ToString();
						fHtl.StsReserva = rdr["StsReserva"].ToString();
						fHtl.HotelAlternativo = rdr["HotelAlternativo"].ToString();
						fHtl.FchSys = Convert.ToDateTime(rdr["FchSys"]).ToString("dd/MM/yyyy");


						lstHtl.Add(item: fHtl);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstHtl;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}





	}
}