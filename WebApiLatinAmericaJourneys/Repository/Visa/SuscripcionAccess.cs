﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebApiLatinAmericaJourneys.Models.Visa;

namespace WebApiLatinAmericaJourneys.Repository.Visa
{
    public class SuscripcionAccess
    {
		public List<Suscripcion> RegistrarSuscripcion(VisaRequest objVisa,int pIdOrdenPago)
		{
			string Status = "";
			string Status2 = "";
			string msgStatus = "";
			string Idioma;

			int NroOrdenPago = 0;
			string Usuario = "";
			string Password = "";
			string CorreoCliente = "";
			string CorreoEjecutiva = "";
			Guid IdSuscripcion;

			try
			{
				List<Suscripcion> lstSuscripcion = new List<Suscripcion>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_RegistrarSuscripcion_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgStatus", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@MsgTransEN", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;


					cmd.Parameters.Add("@Usuario", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@Password", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoCliente", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@CorreoEjecutiva", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@Idioma", SqlDbType.Char, 1).Direction = ParameterDirection.Output;

					//cmd.Parameters.Add("@IdSuscripcion", SqlDbType.UniqueIdentifier).Direction = ParameterDirection.Output;

					cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = objVisa.CodCliente;
					cmd.Parameters.Add("@CodPlan", SqlDbType.VarChar,10).Value = objVisa.IdPlan;
					cmd.Parameters.Add("@NroOrdenPago", SqlDbType.Int).Value = pIdOrdenPago;

					//cmd.Parameters.Add("@EnvOrdenPago", SqlDbType.DateTime).Value = objVisa.EnvOrdenPago;

					con.Open();
					//cmd.ExecuteNonQuery();

					object result = cmd.ExecuteScalar();
					if (result==null)
					{
						IdSuscripcion = Guid.Empty;
					}
					else {
						IdSuscripcion = (Guid)result;


					}


					Status = cmd.Parameters["@MsgTrans"].Value.ToString();
					Status2 = cmd.Parameters["@MsgTransEN"].Value.ToString();
					msgStatus = cmd.Parameters["@MsgStatus"].Value.ToString();

					//IdSuscripcion = (Guid)cmd.Parameters["@IdSuscripcion"].Value;


					Usuario = cmd.Parameters["@Usuario"].Value.ToString();
					Password = cmd.Parameters["@Password"].Value.ToString();
					CorreoCliente = cmd.Parameters["@CorreoCliente"].Value.ToString();
					CorreoEjecutiva = cmd.Parameters["@CorreoEjecutiva"].Value.ToString();
					Idioma = cmd.Parameters["@Idioma"].Value.ToString();

					Suscripcion fSuscripcion = new Suscripcion();

					fSuscripcion.Id = IdSuscripcion;
					fSuscripcion.Usuario = Usuario;
					fSuscripcion.Password = Password;
					fSuscripcion.Mensaje_ES = Status;
					fSuscripcion.Mensaje_EN = Status2;
					fSuscripcion.Mensaje_status = msgStatus;
					fSuscripcion.CorreoCliente = CorreoCliente;
					fSuscripcion.CorreoEjecutiva = CorreoEjecutiva;
					fSuscripcion.Idioma = Idioma;


					lstSuscripcion.Add(item: fSuscripcion);


					con.Close();
				}

				return lstSuscripcion;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}


	}
}