﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Models.Visa;

namespace WebApiLatinAmericaJourneys.Repository.Visa
{
    public class VisaAccess
    {
		public List<VisaComplete> RegistrarRespuestaVisa(VisaRequest objVisa)
		{
			string Status = "";
			int NroOrdenPago = 0;
			string Usuario = "";
			string Password = "";
			string CorreoCliente = "";

			try
			{
				List<VisaComplete> lstVisaComplete = new List<VisaComplete>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{
					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_VISA_OrdenPago_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@NroOrdenPagoOut", SqlDbType.Int).Direction = ParameterDirection.Output;
					//cmd.Parameters.Add("@Usuario", SqlDbType.VarChar,50).Direction = ParameterDirection.Output;
					//cmd.Parameters.Add("@Password", SqlDbType.VarChar,50).Direction = ParameterDirection.Output;
					//cmd.Parameters.Add("@CorreoCliente", SqlDbType.VarChar, 50).Direction = ParameterDirection.Output;



					cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = objVisa.CodCliente;
					cmd.Parameters.Add("@CodComercio", SqlDbType.Int).Value = objVisa.CodComercio;
					cmd.Parameters.Add("@MonOrdenPago", SqlDbType.Money).Value = objVisa.MonOrdenPago;
					//cmd.Parameters.Add("@EnvOrdenPago", SqlDbType.DateTime).Value = objVisa.EnvOrdenPago;
					cmd.Parameters.Add("@RptOrdenPago", SqlDbType.DateTime).Value = objVisa.RptOrdenPago;
					//cmd.Parameters.Add("@Respuesta", SqlDbType.Char,1).Value = objVisa.Respuesta;
					cmd.Parameters.Add("@Cod_accion", SqlDbType.VarChar,10).Value = objVisa.Cod_accion;
					cmd.Parameters.Add("@Pan", SqlDbType.VarChar,19).Value = objVisa.Pan;
					cmd.Parameters.Add("@Eci", SqlDbType.Char,2).Value = objVisa.Eci;
					cmd.Parameters.Add("@Cod_autoriza", SqlDbType.Char, 6).Value = objVisa.Cod_autoriza;
					//cmd.Parameters.Add("@Ori_tarjeta", SqlDbType.Char, 1).Value = objVisa.Ori_Tarjeta;
					cmd.Parameters.Add("@Nom_emisor", SqlDbType.VarChar, 50).Value = objVisa.Nom_emisor;
					cmd.Parameters.Add("@Dsc_eci", SqlDbType.VarChar, 50).Value = objVisa.Dsc_eci;

					cmd.Parameters.Add("@Currency", SqlDbType.VarChar, 10).Value = objVisa.Currency;
					cmd.Parameters.Add("@Adquiriente", SqlDbType.VarChar, 10).Value = objVisa.Adquiriente;
					cmd.Parameters.Add("@Action_description", SqlDbType.VarChar, 50).Value = objVisa.Action_descripction;

					con.Open();
					cmd.ExecuteNonQuery();

					Status = cmd.Parameters["@MsgTrans"].Value.ToString();
					NroOrdenPago = Convert.ToInt32(cmd.Parameters["@NroOrdenPagoOut"].Value.ToString());

					//Usuario = cmd.Parameters["@Usuario"].Value.ToString();
					//Password = cmd.Parameters["@Password"].Value.ToString();
					//CorreoCliente = cmd.Parameters["@CorreoCliente"].Value.ToString();


					VisaComplete fVisaComplete = new VisaComplete();

					fVisaComplete.NroOrdenPago = NroOrdenPago;
					//fVisaComplete.Usuario = Usuario;
					//fVisaComplete.Password = Password;
					fVisaComplete.Mensaje = Status;
					//fVisaComplete.CorreoCliente = CorreoCliente;

					lstVisaComplete.Add(item: fVisaComplete);


					con.Close();
				}

				return lstVisaComplete;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}


	}
}