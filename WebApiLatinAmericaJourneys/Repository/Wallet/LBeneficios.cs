﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class LBeneficios
    {
        public IEnumerable<Beneficios> LeerBeneficios(string idioma)
        {
            string lineagg = "0";
            try
            {
                List<Beneficios> lstBeneficios = new List<Beneficios>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneBeneficio_S", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@idioma", SqlDbType.VarChar).Value = idioma;
                    
                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";
                        Beneficios fBeneficio = new Beneficios();

                        fBeneficio.codbeneficio = rdr["Codbeneficio"].ToString();
                        fBeneficio.titulo = rdr["TituloBeneficio"].ToString();
                        fBeneficio.descripcion = rdr["Descripcion"].ToString();
                        //fBeneficio.condicion = rdr["Condicion"].ToString();
                        lstBeneficios.Add(item: fBeneficio);
                    }

                    lineagg += ",5";
                    con.Close();
                }
                return lstBeneficios;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }


        public IEnumerable<BeneficiosDetalle> LeerBeneficiosDetalle(string idioma)
        {
            try
            {
                List<BeneficiosDetalle> lstBeneficiosDet = new List<BeneficiosDetalle>();
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneBeneficioDetalle_S", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@idioma", SqlDbType.VarChar).Value = idioma;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {
                        BeneficiosDetalle fBeneficio = new BeneficiosDetalle();

                        fBeneficio.id = rdr["Id"].ToString();
                        fBeneficio.codbeneficio = rdr["CodBeneficio"].ToString();
                        fBeneficio.descripcion = rdr["Descripcion"].ToString();
						//fBeneficio.id_superior = rdr["Id_superior"].ToString();
						fBeneficio.silver = rdr["silver"].ToString();
						fBeneficio.gold = rdr["gold"].ToString();

						lstBeneficiosDet.Add(item: fBeneficio);

                    }

                    con.Close();
                }
                return lstBeneficiosDet;
            }
            catch (Exception ex)
            {
                throw new Exception {  };
            }

        }

    }
}