﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
	public class Lcomentario
	{
		public IEnumerable<comentarios> ObtieneComentario(string pUser_id)
		{
			string lineagg = "0";
			try
			{
				List<comentarios> lstComentario = new List<comentarios>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneComent_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = pUser_id;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						comentarios fComent = new comentarios();


					     	fComent.id = rdr["Id"].ToString();
							//titulo = rdr["Titulo"].ToString(),
							fComent.descripcion = rdr["Descripcion"].ToString();
							fComent.fecha = rdr["Fecha"].ToString();

						lstComentario.Add(item: fComent);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstComentario;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}

			   
		public ComentNewResponse RegistrarComentario(string pComentario, string pIdUsuario)
		{
			string lineagg = "0";

			ComentNewResponse Coment = new ComentNewResponse();
			Coment.registro = 0;

			try
			{
				List<ComentarioRequest> lstComent = new List<ComentarioRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_NuevoComentario_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@IdUsuario", SqlDbType.VarChar).Value = pIdUsuario;
					cmd.Parameters.Add("@Comentario", SqlDbType.VarChar).Value = pComentario;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



	}
}