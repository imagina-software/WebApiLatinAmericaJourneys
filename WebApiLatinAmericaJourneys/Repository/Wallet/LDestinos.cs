﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class LDestinos
    {

        public IEnumerable<Destino> ObtenerDestinos()
        {

            try
            {

                List<Destino> lstDestino = new List<Destino>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {


                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaDestinos_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    //cmd.Parameters.Add("@CodZonaVta", SqlDbType.Char, 3).Value = "PER";
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        Destino fdestino= new Destino
                        {

                            id = rdr["Id"].ToString(),
                            nombre = rdr["Nombre"].ToString(),
                            img_url = rdr["Foto_cabecera"].ToString()

                        };

                        lstDestino.Add(item: fdestino);

                    }

                    con.Close();
                }

                return lstDestino;

            }
            catch (Exception ex)
            {

                throw;

            }
        }


        public IEnumerable<FotosDestino> ObtenerDestinosDetalle(string idDestino, string idIdioma, int CodCliente)
        {

            try
            {

                List<FotosDestino> lstDestino = new List<FotosDestino>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))

                {


                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaDestinosDetalle_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pIdDestino", SqlDbType.VarChar, 5).Value = idDestino;
					cmd.Parameters.Add("@pIdioma", SqlDbType.VarChar, 1).Value = idIdioma;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.VarChar, 25).Value = CodCliente;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    while (rdr.Read())
                    {
                        FotosDestino fdestino = new FotosDestino
                        {

                            url = rdr["Img_url"].ToString(),
							descripcion = rdr["descripcion"].ToString(),
							titulo = rdr["titulo"].ToString(),
							id = rdr["ID"].ToString(),
							flagL = rdr["FlagL"].ToString(),
							flagIMG = rdr["flagIMG"].ToString()


						};

                        lstDestino.Add(item: fdestino);

                    }

                    con.Close();
                }

                return lstDestino;

            }
            catch (Exception ex)
            {

                throw;

            }
        }


		public IEnumerable<PaisesLat> ObtenerPaises(string idDestino, string idioma )
		{

			try
			{

				List<PaisesLat> lstPaisesLat = new List<PaisesLat>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{


					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPaisesLatinA_L", con);

					cmd.CommandType = CommandType.StoredProcedure;				
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@pCodPais", SqlDbType.VarChar, 5).Value = idDestino;
					cmd.Parameters.Add("@pIdioma", SqlDbType.VarChar, 5).Value = idioma;

					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();

					while (rdr.Read())
					{
						PaisesLat fpaises = new PaisesLat
						{

							destino_id = rdr["Destino_id"].ToString().Trim(),
							nombre = rdr["Nombre"].ToString(),
							img_url = rdr["Img_url"].ToString(),
                            descripcion = rdr["Descripcion"].ToString()


                        };

						lstPaisesLat.Add(item: fpaises);

					}

					con.Close();
				}

				return lstPaisesLat;

			}
			catch (Exception ex)
			{

				throw;

			}
		}




		public IEnumerable<PaisesLat> ObtenerPaisesLat()
		{

			try
			{

				List<PaisesLat> lstPaisesLat = new List<PaisesLat>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{


					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PaisesLAT_L", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();

					while (rdr.Read())
					{
						PaisesLat fpaises = new PaisesLat
						{

							destino_id = rdr["Destino_id"].ToString().Trim(),
							nombre = rdr["Nombre"].ToString(),
							img_url = rdr["Img_url"].ToString(),
						//	descripcion = rdr["Descripcion"].ToString()


						};

						lstPaisesLat.Add(item: fpaises);

					}

					con.Close();
				}

				return lstPaisesLat;

			}
			catch (Exception ex)
			{

				throw;

			}
		}




		public IEnumerable<PaisesLat> ObtenerPaisesLat2(string idioma)
		{

			try
			{

				List<PaisesLat> lstPaisesLat = new List<PaisesLat>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{


					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PaisesLAT_L2", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
					cmd.Parameters.Add("@idioma", SqlDbType.VarChar, 5).Value = idioma;

					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();

					while (rdr.Read())
					{
						PaisesLat fpaises = new PaisesLat
						{

							destino_id = rdr["Destino_id"].ToString().Trim(),
							//nombre = rdr["Nombre"].ToString(),
							titulo = rdr["titulo"].ToString(),
							descripcion2 = rdr["descripcion2"].ToString()


						};

						lstPaisesLat.Add(item: fpaises);

					}

					con.Close();
				}

				return lstPaisesLat;

			}
			catch (Exception ex)
			{

				throw;

			}
		}




	}



}