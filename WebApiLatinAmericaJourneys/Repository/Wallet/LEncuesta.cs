﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;
using System.Drawing;
using Dropbox.Api.TeamLog;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class LEncuesta
    {
        public IEnumerable<EncuestaResponse> LeerEncuesta(string pUser_id ,char pIdioma)
        {
            string lineagg = "0";
            string demo1 = "";
            string demo2 = "";
            string demo3 = "";

            try
            {
                List<EncuestaResponse> lstEncuesta = new List<EncuestaResponse>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_LeerRespuestaPregunta_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@MsgRespuesta", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@MsgRespuestaI", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.Int).Value = pUser_id;
                    cmd.Parameters.Add("@pIdioma", SqlDbType.Char,1).Value = pIdioma;


                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    demo1 = cmd.Parameters["@MsgTrans"].Value.ToString();
                    demo2 = cmd.Parameters["@MsgRespuesta"].Value.ToString();
                    demo3 = cmd.Parameters["@MsgRespuestaI"].Value.ToString();


                    EncuestaResponse fencuesta = new EncuestaResponse();
                    fencuesta.status = demo1;
                    fencuesta.msg = demo2;
                    fencuesta.msgIngles = demo3;
                    lstEncuesta.Add(item: fencuesta);

                    lineagg += ",5";
                    con.Close();
                }
                return lstEncuesta;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }

        public string RegistrarEncuesta(string pUser_id, string pPregunta, string pRespuesta)
        {
           string lineagg = "0";
           string Status = "";
            try
            {
                List<CalificaRequest> lstPropuesta = new List<CalificaRequest>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_PreguntaRespUsuario_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = pUser_id;
                    cmd.Parameters.Add("@pIdPregunta", SqlDbType.Int).Value = pPregunta;
                    cmd.Parameters.Add("@pRespuesta", SqlDbType.Char).Value = pRespuesta;

                    lineagg += ",2";
                    con.Open();

                    cmd.ExecuteNonQuery();

                    Status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    con.Close();
                }
                lineagg += ",5";

                return Status;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


        public IEnumerable<PreguntaEncuesta> LeerEncuestaPregunta(string pIdioma)
        {
             string lineagg = "0";
            try
            {

                List<PreguntaEncuesta> lstPreguntas = new List<PreguntaEncuesta>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_LeePreguntaEncuesta_L", con);
                    cmd.Parameters.Add("@pIdioma", SqlDbType.Char,1).Value = pIdioma;

                    cmd.CommandType = CommandType.StoredProcedure;
          
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    //Foto.status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {


                        PreguntaEncuesta fPreguntas = new PreguntaEncuesta
                        {

                            Id = rdr["IdPregunta"].ToString(),
                            DesPregunta = rdr["DesPregunta"].ToString(),
                          //  VideEncuesta = rdr["VideoEncuesta"].ToString()

                        };

                        lstPreguntas.Add(item: fPreguntas);



                    }
                    con.Close();
                }

                return lstPreguntas;

            }
            catch (Exception ex)
            {

                throw new Exception { Source = lineagg };

            }

        }


		public IEnumerable<VideoEncuesta> ObtenerVideo(string pIdioma)
		{

			try
			{

				List<VideoEncuesta> lstVideoEncuesta = new List<VideoEncuesta>();

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{


					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_VideoEncuesta_L", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pIdioma", SqlDbType.VarChar).Value = pIdioma;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;

					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();

					while (rdr.Read())
					{
						VideoEncuesta fencuesta = new VideoEncuesta
						{

							videoEncuesta = rdr["VIDEOENCUESTA"].ToString()


						};

						lstVideoEncuesta.Add(item: fencuesta);

					}

					con.Close();
				}

				return lstVideoEncuesta;

			}
			catch (Exception ex)
			{

				throw;

			}
		}



		public void ActualizarValor(string CodCliente)
		{
			string Status = " ";

			try
			{

				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ActualizaEstadoEncuesta_U", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar).Value = CodCliente;


					con.Open();

					cmd.ExecuteNonQuery();

					con.Close();
				}

			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}












	}
}