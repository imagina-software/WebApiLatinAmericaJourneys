﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class LMensajeChat
    {

        public string RegistrarMensajeChat(string pCodCliente, int pViaje_id, string pMsg)
        {
            string Status = " ";

            try
            {
                //List<NuevoViajeResponse> lstNuevoViaje = new List<NuevoViajeResponse>();
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_MensajeChat_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;      
                    cmd.Parameters.Add("@pCodCliente", SqlDbType.VarChar,25).Value = pCodCliente;
                    cmd.Parameters.Add("@pCodViaje", SqlDbType.Int).Value = pViaje_id;
                    cmd.Parameters.Add("@pMsg", SqlDbType.VarChar).Value = pMsg;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    Status = cmd.Parameters["@MsgTrans"].Value.ToString();
                    //CodViaje = Convert.ToInt32(cmd.Parameters["@CodViaje"].Value.ToString());

                    //NuevoViajeResponse fencuesta = new NuevoViajeResponse();

                    //fencuesta.Status = Status;
                    //fencuesta.Idsol = CodViaje;

                    //lstNuevoViaje.Add(item: fencuesta);
                    con.Close();
                }

                return Status;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public IEnumerable<Mensajes> ListarMensajeChat(string pCodCliente, int pViaje_id)
        {

            string lineagg = "0";
            FotoConsultaRequest Foto = new FotoConsultaRequest();
            Foto.status = "";
            try
            {

                List<Mensajes> lstmensajes = new List<Mensajes>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_MensajeChat_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pCodCliente", SqlDbType.VarChar,25).Value = pCodCliente;
                    cmd.Parameters.Add("@pCodViaje", SqlDbType.Int).Value = pViaje_id;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    Foto.status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {


                        Mensajes fMensaje = new Mensajes
                        {

                            id = rdr["Id"].ToString(),
                            text = rdr["Msg"].ToString(),
                            src = rdr["Src"].ToString(),
                            src_name = rdr["Src_name"].ToString(),
                            src_img = rdr["Src_img"].ToString(),
                            time = rdr["Time"].ToString()

                        };

                        lstmensajes.Add(item: fMensaje);



                    }
                    con.Close();
                }

                return lstmensajes;

            }
            catch (Exception ex)
            {

                throw new Exception { Source = lineagg };

            }


        }



        public string RegistrarMensajeChatGeneral(string pCodCliente , string pMsg)
        {
            string Status = " ";

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_MensajeChatGeneral_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@pCodCliente", SqlDbType.VarChar, 25).Value = pCodCliente;
                    cmd.Parameters.Add("@pMsg", SqlDbType.VarChar).Value = pMsg;

                    con.Open();

                    cmd.ExecuteNonQuery();

                    Status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    con.Close();
                }

                return Status;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        public IEnumerable<Mensajes> ListarMensajeChatGeneral(string pCodCliente)
        {

            string lineagg = "0";
            FotoConsultaRequest Foto = new FotoConsultaRequest();
            Foto.status = "";
            try
            {

                List<Mensajes> lstmensajes = new List<Mensajes>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_MensajeChatGeneral_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pCodCliente", SqlDbType.VarChar, 25).Value = pCodCliente;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    Foto.status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {


                        Mensajes fMensaje = new Mensajes
                        {

                            id = rdr["Id"].ToString(),
                            text = rdr["Msg"].ToString(),
                            src = rdr["Src"].ToString(),
                            src_name = rdr["Src_name"].ToString(),
                            src_img = rdr["Src_img"].ToString(),
                            time = rdr["Time"].ToString()

                        };

                        lstmensajes.Add(item: fMensaje);



                    }
                    con.Close();
                }

                return lstmensajes;

            }
            catch (Exception ex)
            {

                throw new Exception { Source = lineagg };

            }


        }



    }
}