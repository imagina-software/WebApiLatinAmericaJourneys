﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
	public class LNotificaciones
	{

		public IEnumerable<Notificacion> LeerNotificacion(string codCliente)
		{
			string lineagg = "0";
			try
			{
				List<Notificacion> lstNotificacion = new List<Notificacion>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneNotificacion_S", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@codCliente", SqlDbType.VarChar).Value = codCliente;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";
						Notificacion fNotificacion = new Notificacion();

						fNotificacion.id = Convert.ToInt32(rdr["id"]);
						fNotificacion.titulo = rdr["titulo"].ToString();
						fNotificacion.contenido = rdr["contenido"].ToString();
						fNotificacion.titulo_EN = rdr["titulo_EN"].ToString();
						fNotificacion.contenido_EN = rdr["contenido_EN"].ToString();
						fNotificacion.Fecha = rdr["Fecha"].ToString();

						lstNotificacion.Add(item: fNotificacion);
					}

					lineagg += ",5";
					con.Close();
				}
				return lstNotificacion;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}









	}
}