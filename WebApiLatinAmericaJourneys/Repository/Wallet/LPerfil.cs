﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
	public class LPerfil
	{

		public IEnumerable<perfil> ObtienePerfil(string pCodCliente)
		{
			string lineagg = "0";
			try
			{
				List<perfil> lstPerfil = new List<perfil>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListaPerfil_L", con);
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();
					cmd.ExecuteNonQuery();
					SqlDataReader rdr = cmd.ExecuteReader();
					lineagg += ",3";
					while (rdr.Read())
					{
						lineagg += ",4";

						perfil fPerfil = new perfil();


						fPerfil.nombre = rdr["nombre"].ToString();
						//titulo = rdr["Titulo"].ToString(),
						fPerfil.paterno = rdr["paterno"].ToString();
						fPerfil.email = rdr["email"].ToString();
						fPerfil.telefono = rdr["telefono"].ToString();

						lstPerfil.Add(item: fPerfil);

					}

					lineagg += ",5";
					con.Close();
				}
				return lstPerfil;
			}
			catch (Exception ex)
			{
				throw new Exception { Source = lineagg };
			}

		}




		public PerfilNewResponse ActualizarPerfil(string pCodCliente, string pNombre, string pApellidos, string pEmail, string pTelefono)
		{
			string lineagg = "0";

			PerfilNewResponse Coment = new PerfilNewResponse();
			Coment.registro = 0;

			try
			{
				List<PerfilNewRequest> lstPerfil = new List<PerfilNewRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_Perfil_U", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar).Value = pCodCliente;
					cmd.Parameters.Add("@Nombre", SqlDbType.VarChar).Value = pNombre;
					cmd.Parameters.Add("@Apellidos", SqlDbType.VarChar).Value = pApellidos;
					cmd.Parameters.Add("@Email", SqlDbType.VarChar).Value = pEmail;
					cmd.Parameters.Add("@Telefono", SqlDbType.VarChar).Value = pTelefono;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Coment.registro = cmd.ExecuteNonQuery();
					Coment.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Coment;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}



	}
}