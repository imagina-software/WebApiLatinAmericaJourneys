﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WebApiLatinAmericaJourneys.Models;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class LVideo
    {
        public IEnumerable<Video> ListarVideos(int pUser_id)
        {

            string lineagg = "0";
            FotoConsultaRequest Foto = new FotoConsultaRequest();
            Foto.status = "";
            try
            {

                List<Video> lstvideo = new List<Video>();

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ListarVideo_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pCodUsuario", SqlDbType.Int).Value = pUser_id;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    con.Open();
                    cmd.ExecuteNonQuery();

                    Foto.status = cmd.Parameters["@MsgTrans"].Value.ToString();

                    SqlDataReader rdr = cmd.ExecuteReader();
                    while (rdr.Read())
                    {


                        Video fVideo = new Video
                        {

                            IdVideo = rdr["Id"].ToString(),
                            Video_img = rdr["Video_img"].ToString(),
                            Titulo = rdr["Titulo"].ToString(),
                            Comentario = rdr["Comentario"].ToString(),
                            Embed = rdr["Embed"].ToString()

                        };

                        lstvideo.Add(item: fVideo);



                    }
                    con.Close();
                }

                return lstvideo;

            }
            catch (Exception ex)
            {

                throw new Exception { Source = lineagg };

            }


        }

    }
}