﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.Models;
using WebApiLatinAmericaJourneys.ModelsWallet;


namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
	public class LVisita
	{


		public VisitasResponse RegistrarVisitas(int pCodCliente, int pIdModulo)
		{
			string lineagg = "0";

			VisitasResponse Visitas = new VisitasResponse();
			Visitas.registro = 0;

			try
			{
				List<VisitasRequest> lstComent = new List<VisitasRequest>();
				lineagg += ",1";
				using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
				{

					SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_NuevaVisita_I", con);

					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.Add("@pCodCliente", SqlDbType.Int).Value = pCodCliente;
					cmd.Parameters.Add("@pCodModulo", SqlDbType.Int).Value = pIdModulo;

					cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;

					lineagg += ",2";
					con.Open();

					Visitas.registro = cmd.ExecuteNonQuery();
					Visitas.status = cmd.Parameters["@MsgTrans"].Value.ToString();

					con.Close();
				}
				lineagg += ",5";


				return Visitas;
			}
			catch (Exception ex)
			{
				throw new Exception(ex.Message);
			}

		}





	}
}