﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data;
using WebApiLatinAmericaJourneys.ModelsWallet;
using WebApiLatinAmericaJourneys.Controllers;

namespace WebApiLatinAmericaJourneys.Repository.Wallet
{
    public class Llogin
    {

        public IEnumerable<LoginWResponse> LeerUsuario(string pUid, string pPass, string pUsernameJWT, string pPasswordJWT)
        {
            var token = string.Empty;
            string Msgtrans = string.Empty;
            string Status = string.Empty;

            try
            {
                List<LoginWResponse> lstLogin = new List<LoginWResponse>();


                bool isCredentialValid = (pPasswordJWT == "Pentagrama2020$" || pPasswordJWT == "PentagramaLocal$");
                if (isCredentialValid)
                {
                    token = TokenGenerator.GenerateTokenJwt(pUsernameJWT);

                }

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneAccesos_S", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar, 250).Value = pUid;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar, 25).Value = pPass;

                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                    con.Open();
                    cmd.ExecuteNonQuery();

                    Msgtrans = cmd.Parameters["@MsgTrans"].Value.ToString();
                    Status = cmd.Parameters["@Status"].Value.ToString();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (!rdr.HasRows)
                    {

                        LoginWResponse fLogin = new LoginWResponse();

                        fLogin.status = Status;
                        fLogin.msg = Msgtrans;
                        lstLogin.Add(item: fLogin);

                    }
                    else
                    {


                        while (rdr.Read())
                        {

                            LoginWResponse fLogin = new LoginWResponse();

                            fLogin.status = Status;
                            fLogin.msg = Msgtrans;
                            fLogin.perfil = new List<Perfil>();
                            fLogin.video_URL = rdr["video_URL"].ToString();
                            fLogin.tokenJWT = token;
                            fLogin.video_ejecu_URL = rdr["URLVideoVendedor"].ToString();
                            fLogin.nombre_ejecu = rdr["NomVendedor"].ToString();
                            fLogin.foto = rdr["foto"].ToString();
                            fLogin.cambioClave = rdr["CambioClave"].ToString();
                            fLogin.usuario = rdr["Usuario"].ToString();
                            fLogin.FlagVisto = rdr["FlagVisto"].ToString();
                            //fLogin.titulo = rdr["titulo"].ToString();
                            fLogin.DescripcionPlan = rdr["DescripcionPlan"].ToString();

                            var ListaPerfiles = LeerPerfil(pUid, pPass);
                            var lVisto = ActualizarValor(pUid);
                            fLogin.perfil.AddRange(ListaPerfiles);
                            lstLogin.Add(item: fLogin);
                        }



                    }

                    con.Close();
                }
                return lstLogin;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = ex.ToString() };
            }

        }

        public IEnumerable<LoginWResponse> LeerUsuarioAutoLogin(string pUid, string pPass, string pUsernameJWT, string pPasswordJWT, string codCliente)
        {
            var token = string.Empty;
            string msgtrans = string.Empty;
            string status = string.Empty;

            try
            {
                List<LoginWResponse> lstLogin = new List<LoginWResponse>();


                bool isCredentialValid = (pPasswordJWT == "Pentagrama2020$" || pPasswordJWT == "PentagramaLocal$");
                if (isCredentialValid)
                {
                    token = TokenGenerator.GenerateTokenJwt(pUsernameJWT);

                }

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneAccesosAuto_S", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar, 250).Value = codCliente;
                    //cmd.Parameters.Add("@Password", SqlDbType.VarChar, 25).Value = pPass;

                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                    con.Open();
                    cmd.ExecuteNonQuery();

                    msgtrans = cmd.Parameters["@MsgTrans"].Value.ToString();
                    status = cmd.Parameters["@Status"].Value.ToString();
                    SqlDataReader rdr = cmd.ExecuteReader();

                    if (!rdr.HasRows)
                    {

                        LoginWResponse fLogin = new LoginWResponse();

                        fLogin.status = status;
                        fLogin.msg = msgtrans;
                        lstLogin.Add(item: fLogin);

                    }
                    else
                    {


                        while (rdr.Read())
                        {

                            LoginWResponse fLogin = new LoginWResponse();

                            fLogin.status = status;
                            fLogin.msg = msgtrans;
                            fLogin.perfil = new List<Perfil>();
                            fLogin.video_URL = rdr["video_URL"].ToString();
                            fLogin.tokenJWT = token;
                            fLogin.video_ejecu_URL = rdr["URLVideoVendedor"].ToString();
                            fLogin.nombre_ejecu = rdr["NomVendedor"].ToString();
                            fLogin.foto = rdr["foto"].ToString();
                            fLogin.cambioClave = rdr["CambioClave"].ToString();
                            fLogin.usuario = rdr["Usuario"].ToString();
                            fLogin.FlagVisto = rdr["FlagVisto"].ToString();
                            //fLogin.titulo = rdr["titulo"].ToString();
                            fLogin.DescripcionPlan = rdr["DescripcionPlan"].ToString();

                            var listaPerfiles = LeerPerfilAutoLogin(codCliente);
                            //var lVisto = ActualizarValor(pUid);
                            fLogin.perfil.AddRange(listaPerfiles);
                            lstLogin.Add(item: fLogin);
                        }



                    }

                    con.Close();
                }
                return lstLogin;
            }
            catch (Exception ex)
            {
                throw new Exception { Source = ex.ToString() };
            }

        }

        public IEnumerable<Perfil> LeerPerfil(string pUid, string pPass)
        {
            string lineagg = "0";
            try
            {
                List<Perfil> lstPerfil = new List<Perfil>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneAccesos_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodUsuario", SqlDbType.VarChar, 250).Value = pUid;
                    cmd.Parameters.Add("@Password", SqlDbType.VarChar, 25).Value = pPass;


                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",5";

                        Perfil fPerfil = new Perfil
                        {
                            id = rdr["ID"].ToString(),
                            nombre = rdr["Nombre"].ToString(),
                            apellidos = rdr["Apellidos"].ToString(),
                            saldo = rdr["Saldo"].ToString(),
                            TipoIdioma = rdr["TipoIdioma"].ToString()
                        };

                        lstPerfil.Add(item: fPerfil);

                    }

                    lineagg += ",5";
                    con.Close();
                }

                return lstPerfil;

            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }

        public IEnumerable<Perfil> LeerPerfilAutoLogin(string codCliente)
        {
            string lineagg = "0";
            try
            {
                List<Perfil> lstPerfil = new List<Perfil>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ObtieneAccesosAuto_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@CodCliente", SqlDbType.VarChar, 250).Value = codCliente;
                    //cmd.Parameters.Add("@Password", SqlDbType.VarChar, 25).Value = pPass;


                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 250).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@Status", SqlDbType.VarChar, 20).Direction = ParameterDirection.Output;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",5";

                        Perfil fPerfil = new Perfil
                        {
                            id = rdr["ID"].ToString(),
                            nombre = rdr["Nombre"].ToString(),
                            apellidos = rdr["Apellidos"].ToString(),
                            saldo = rdr["Saldo"].ToString(),
                            TipoIdioma = rdr["TipoIdioma"].ToString()
                        };

                        lstPerfil.Add(item: fPerfil);

                    }

                    lineagg += ",5";
                    con.Close();
                }

                return lstPerfil;

            }
            catch (Exception ex)
            {
                throw new Exception { Source = lineagg };
            }

        }

        public string ActualizarValor(string codUsuario)
        {
            string Status = " ";

            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("latinamericajourneys.LAJ_ActualizaEstado_U", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@codUsuario", SqlDbType.VarChar).Value = codUsuario;


                    con.Open();

                    cmd.ExecuteNonQuery();

                    con.Close();
                }

                return Status;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }


    }
}