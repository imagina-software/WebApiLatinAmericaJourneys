﻿namespace WebApiLatinAmericaJourneys.Utility
{
    public class ConstantesWeb
    {
        public const char CHR_IDIOMA_INGLES = 'I';

        public const char CHR_IDIOMA_ESPAÑOL = 'E';

        public const string strSecureAppSettings = "secureAppSettings";

        public const string STR_ESTADO_PROGRAMA_I = "Sold";

        public const string STR_ESTADO_PROGRAMA_E = "Vendido";

        //public const string STR_CORREO_EMISOR = "rchahud@pentagrama-travel.com";
        public const string STR_CORREO_EMISOR = "maria.chahud@golatinamericaclub.com";


        //public const string STR_CORREO_EMISOR = "rosario@pentagrama.com";


        public const string STR_NOMBRE_EMISOR = "Go Latin America Club";

        public const string STR_EMAIL_ASUNTO_CAMBIO_CLAVE = "Cambio de clave";

        public const string STR_EMAIL_ASUNTO_ENVIO_CLAVE = "Clave para la aplicacion";

        public const string STR_EMAIL_CUERPO_CAMBIO_CLAVE = "Esta es su nueva contraseña temporal  por favor cambiala dentro de la app : ";

        public const string STR_EMAIL_CUERPO_ENVIO_CLAVE = "Las credenciales para ingresar a la aplicacion son : ";

        public const string STR_CODE_PNG = "IVBOR";

        public const string STR_CODE_JPG= "/9J/4";

        public const string STR_ESTADO_SERVICIO_OK = "ok";

        public const string STR_ESTADO_SERVICIO_FAIL = "fail";

        public const string STR_ESTADO_PROCEDURE_OK = "ok";

        public const string STR_ESTADO_PROCEDURE_FAIL = "fail";

        public const char CHR_ESTADO_ACTIVO = 'A';

        public const char CHR_ESTADO_INACTIVO = 'I';

		public const string STR_EMAIL_CUERPO_EJECUTIVO = "Se le asigno al cliente : ";

		public const string STR_EMAIL_ASUNTO_ENVIO_CLIENTE = "Cliente asignado";

        public const string STR_TEMPLATE_ENVIO_ACCESOS = "TMPACC";

        public const string STR_TEMPLATE_ENVIO_EJECUTIVA = "TMPEJE";

        public const string STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE = "TMPREC";

		public const string STR_TEMPLATE_CAMBIO_PLAN = "TMPCAM";

		public const string STR_EMAIL_ASUNTO_ENVIO_CAMBIOPLAN = "Cambio de plan";

		public const string STR_TEMPLATE_TARIFA = "TMPCTAR";



	}


}