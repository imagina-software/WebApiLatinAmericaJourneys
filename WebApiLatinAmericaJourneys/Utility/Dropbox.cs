﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dropbox.Api;
using Dropbox.Api.Files;

namespace WebApiLatinAmericaJourneys.Utility
{
    public class Dropbox
    {
        private object ocl;

        public void Conexion()
        {
            var task = Task.Run((Func<Task>)Dropbox.Run);

            task.Wait();
        }
        //static void Main(string[] args)
        //{
        //    var task = Task.Run((Func<Task>)Dropbox.Run);
        //    task.Wait();
            
        //}


        static async Task Run()
        {
            using (var dbx = new DropboxClient("hz3VmvmtJaAAAAAAAAABCtX6Ljwy1yYHgAViH1oFNAyYSIckoGF20dxCZljWbNNn"))
            {
                var full = await dbx.Users.GetCurrentAccountAsync();
                Console.WriteLine("{0} - {1}", full.Name.DisplayName, full.Email);
            }
        }

        /*Listar fotos del dropbox*/
        async Task ListRootFolder(DropboxClient dbx)
        {
            var list = await dbx.Files.ListFolderAsync(string.Empty);

            // show folders then files
            foreach (var item in list.Entries.Where(i => i.IsFolder))
            {
                Console.WriteLine("D  {0}/", item.Name);
            }

            foreach (var item in list.Entries.Where(i => i.IsFile))
            {
                Console.WriteLine("F{0,8} {1}", item.AsFile.Size, item.Name);
            }
        }


        /*Subir fotos al dropbox*/
        async Task Upload(DropboxClient dbx, string folder, string file, string content)
        {
            using (var mem = new MemoryStream(Encoding.UTF8.GetBytes(content)))
            {
                var updated = await dbx.Files.UploadAsync(
                    folder + "/" + file,
                    WriteMode.Overwrite.Instance,
                    body: mem);
                Console.WriteLine("Saved {0}/{1} rev {2}", folder, file, updated.Rev);
            }
        }


        public void UploadFile(byte[] content, string filename, string target)
        {
            


        }

    }
}