﻿using Mailjet.Client;
using Mailjet.Client.Resources;
using Newtonsoft.Json.Linq;
using System;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;
using WebApiLatinAmericaJourneys.Repository.LatinAmericaJourneys;
using ws = WebApiLatinAmericaJourneys.Ws_SendGrid;


namespace WebApiLatinAmericaJourneys.Utility
{
    public class Email
    {

        public void EnviarCorreoSendGrid(string pNombreEmisor,
            string pCorreoEmisor,
            string pDestinatarios,
            string pAsunto,
            string pUsuario,
            string pPassword,
            string pIdioma,
            string pPlantilla)
        {
            //var send = new ws.wsMailsSoapClient();

            var send = new ws.wsMails();

            var resultado = new RespuestaEmail();



            try
            {
                // *************************************************************************
                // Para envio de correos sin adjunto
                // *************************************************************************

                var enviarSinAdjunto = send.EnviarCorreo(
                    new ws.Autenticacion
                    {
                        InWebId = ws.TipoWeb.ApiGolac,
                        StUsuario = Encriptar(WebConfigurationManager.AppSettings["key_usuarioTk"].ToString()),
                        StClave = Encriptar(WebConfigurationManager.AppSettings["key_claveTk"].ToString())
                    },
                    new ws.Correo
                    {
                        NombreEmisor = pNombreEmisor,
                        CorreoEmisor = pCorreoEmisor,
                        Destinatarios = pDestinatarios,
                        Asunto = pAsunto,
                        CuerpoHtml = "",
                        CuerpoTexto = "",
                        Usuario = pUsuario,
                        Password = pPassword,
                        Idioma = pIdioma,
                        Plantilla = pPlantilla,
                        CodCliente = pUsuario,
                        Email = pPassword
                    });
                // *************************************************************************

                resultado.Valor = enviarSinAdjunto.Valor;
            }
            catch (Exception ex)
            {

                resultado.Tipo = TipoRespuesta.Error;
                resultado.Valor = ex.Message;

            }

        }

        private static string Encriptar(string cadena)
        {
            using (System.Security.Cryptography.MD5 md5Hash = MD5.Create())
            {
                byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(cadena));

                StringBuilder sBuilder = new StringBuilder();

                for (int i = 0; i < data.Length; i++)
                {
                    sBuilder.Append(data[i].ToString("x2"));
                }

                return sBuilder.ToString();
            }
        }



        public async Task EnviarCorreoMailJetAsync(
            string pNombreEmisor,
            string pCorreoEmisor,
            string pDestinatarios,
            string pAsunto,
            string pUsuario,
            string pPassword,
            string pIdioma,
            string pPlantilla)
        {


            MailjetClient client = new MailjetClient("59bcdfc9216b26c9bfd6ed5be7b355ad", "b9fcb4d3fc4a7076313e4b64d5c65f19")
            {
                Version = ApiVersion.V3_1,
            };


            if (pPlantilla.Equals(ConstantesWeb.STR_TEMPLATE_ENVIO_RECUPERACION_CLAVE))
            {

                if (pIdioma.Equals(ConstantesWeb.CHR_IDIOMA_ESPAÑOL.ToString()))
                {

                    //MailjetClient client = new MailjetClient(Environment.GetEnvironmentVariable("59bcdfc9216b26c9bfd6ed5be7b355ad"), Environment.GetEnvironmentVariable("b9fcb4d3fc4a7076313e4b64d5c65f19"))
                    //{
                    //    Version = ApiVersion.V3_1,
                    //};

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                         .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Suscriptor"}
                        }
                       }},
                      {"TemplateID", 1665135},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"usuario", pUsuario},
                               {"password", pPassword}
                       }}

                      }
                    });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }
                else
                {

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                        .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Subscriber"}
                        }
                       }},
                      {"TemplateID", 1665131},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"usuario", pUsuario},
                               {"password", pPassword}
                       }}

                      }
                   });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }

            }


			if (pPlantilla.Equals(ConstantesWeb.STR_TEMPLATE_CAMBIO_PLAN))
			{

				if (pIdioma.Equals(ConstantesWeb.CHR_IDIOMA_ESPAÑOL.ToString()))
				{

					//MailjetClient client = new MailjetClient(Environment.GetEnvironmentVariable("59bcdfc9216b26c9bfd6ed5be7b355ad"), Environment.GetEnvironmentVariable("b9fcb4d3fc4a7076313e4b64d5c65f19"))
					//{
					//    Version = ApiVersion.V3_1,
					//};

					MailjetRequest request = new MailjetRequest
					{
						Resource = Send.Resource,

					}

						 .Property(Send.Messages, new JArray {
					 new JObject {
					  {"From", new JObject {
					   {"Email", pCorreoEmisor},
					   {"Name", pNombreEmisor}
					   }},
					  {"To", new JArray {
					   new JObject {
						{"Email", pDestinatarios},
						{"Name", "Suscriptor"}
						}
					   }},
					  {"TemplateID", 1821232},
					  {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
							   {"nombreCli", pAsunto},
							   {"correoCli",pUsuario },
							   {"password", pPassword}
					   }}

					  }
					});


					MailjetResponse response = await client.PostAsync(request);

					if (response.IsSuccessStatusCode)
					{
						Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
						Console.WriteLine(response.GetData());
					}
					else
					{
						Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
						Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
						Console.WriteLine(response.GetData());
						Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
					}

				}
				else
				{

					MailjetRequest request = new MailjetRequest
					{
						Resource = Send.Resource,

					}

						.Property(Send.Messages, new JArray {
					 new JObject {
					  {"From", new JObject {
					   {"Email", pCorreoEmisor},
					   {"Name", pNombreEmisor}
					   }},
					  {"To", new JArray {
					   new JObject {
						{"Email", pDestinatarios},
						{"Name", "Subscriber"}
						}
					   }},
					  {"TemplateID", 1665131},
					  {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
							   {"usuario", pUsuario},
							   {"password", pPassword}
					   }}

					  }
				   });


					MailjetResponse response = await client.PostAsync(request);

					if (response.IsSuccessStatusCode)
					{
						Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
						Console.WriteLine(response.GetData());
					}
					else
					{
						Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
						Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
						Console.WriteLine(response.GetData());
						Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
					}

				}

			}





			if (pPlantilla.Equals(ConstantesWeb.STR_TEMPLATE_TARIFA))
			{

				if (pIdioma.Equals(ConstantesWeb.CHR_IDIOMA_ESPAÑOL.ToString()))
				{

					//MailjetClient client = new MailjetClient(Environment.GetEnvironmentVariable("59bcdfc9216b26c9bfd6ed5be7b355ad"), Environment.GetEnvironmentVariable("b9fcb4d3fc4a7076313e4b64d5c65f19"))
					//{
					//    Version = ApiVersion.V3_1,
					//};

					MailjetRequest request = new MailjetRequest
					{
						Resource = Send.Resource,

					}

						 .Property(Send.Messages, new JArray {
					 new JObject {
					  {"From", new JObject {
					   {"Email", pCorreoEmisor},
					   {"Name", pNombreEmisor}
					   }},
					  {"To", new JArray {
					   new JObject {
						{"Email", pDestinatarios},
						{"Name", "Suscriptor"}
						}
					   }},
					  {"TemplateID", 1880258},
					  {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
							   {"nombreCli", pAsunto},
							   {"correoCli",pUsuario },
							   {"idPromocion", pPassword}
					   }}

					  }
					});


					MailjetResponse response = await client.PostAsync(request);

					if (response.IsSuccessStatusCode)
					{
						Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
						Console.WriteLine(response.GetData());
					}
					else
					{
						Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
						Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
						Console.WriteLine(response.GetData());
						Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
					}

				}
				else
				{

					MailjetRequest request = new MailjetRequest
					{
						Resource = Send.Resource,

					}

						.Property(Send.Messages, new JArray {
					 new JObject {
					  {"From", new JObject {
					   {"Email", pCorreoEmisor},
					   {"Name", pNombreEmisor}
					   }},
					  {"To", new JArray {
					   new JObject {
						{"Email", pDestinatarios},
						{"Name", "Subscriber"}
						}
					   }},
					  {"TemplateID", 1665131},
					  {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
							   {"usuario", pUsuario},
							   {"password", pPassword}
					   }}

					  }
				   });


					MailjetResponse response = await client.PostAsync(request);

					if (response.IsSuccessStatusCode)
					{
						Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
						Console.WriteLine(response.GetData());
					}
					else
					{
						Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
						Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
						Console.WriteLine(response.GetData());
						Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
					}

				}

			}











			else if (pPlantilla.Equals(ConstantesWeb.STR_TEMPLATE_ENVIO_ACCESOS))
            {


                if (pIdioma.Equals(ConstantesWeb.CHR_IDIOMA_ESPAÑOL.ToString()))
                {

                    //MailjetClient client = new MailjetClient(Environment.GetEnvironmentVariable("59bcdfc9216b26c9bfd6ed5be7b355ad"), Environment.GetEnvironmentVariable("b9fcb4d3fc4a7076313e4b64d5c65f19"))
                    //{
                    //    Version = ApiVersion.V3_1,
                    //};

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                         .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Suscriptor"}
                        }
                       }},
                      {"TemplateID", 1665098},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"usuario", pUsuario},
                               {"password", pPassword}
                       }}

                      }
                    });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }
                else
                {

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                        .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Subscriber"}
                        }
                       }},
                      {"TemplateID", 1665103},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"usuario", pUsuario},
                               {"password", pPassword}
                       }}

                      }
                   });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }




            }
            else {

                if (pIdioma.Equals(ConstantesWeb.CHR_IDIOMA_ESPAÑOL.ToString()))
                {

                    //MailjetClient client = new MailjetClient(Environment.GetEnvironmentVariable("59bcdfc9216b26c9bfd6ed5be7b355ad"), Environment.GetEnvironmentVariable("b9fcb4d3fc4a7076313e4b64d5c65f19"))
                    //{
                    //    Version = ApiVersion.V3_1,
                    //};

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                         .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Ejecutiva"}
                        }
                       }},
                      {"TemplateID", 1665122},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"codcliente", pUsuario},
                               {"email", pPassword}
                       }}

                      }
                    });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }
                else
                {

                    MailjetRequest request = new MailjetRequest
                    {
                        Resource = Send.Resource,

                    }

                        .Property(Send.Messages, new JArray {
                     new JObject {
                      {"From", new JObject {
                       {"Email", pCorreoEmisor},
                       {"Name", pNombreEmisor}
                       }},
                      {"To", new JArray {
                       new JObject {
                        {"Email", pDestinatarios},
                        {"Name", "Ejecutiva"}
                        }
                       }},
                      {"TemplateID", 1665122},
                      {"TemplateLanguage", true},
                      //{"Subject", pAsunto},
                      {"Variables", new JObject {
                               {"codcliente", pUsuario},
                               {"email", pPassword}
                       }}

                      }
                   });


                    MailjetResponse response = await client.PostAsync(request);

                    if (response.IsSuccessStatusCode)
                    {
                        Console.WriteLine(string.Format("Total: {0}, Count: {1}\n", response.GetTotal(), response.GetCount()));
                        Console.WriteLine(response.GetData());
                    }
                    else
                    {
                        Console.WriteLine(string.Format("StatusCode: {0}\n", response.StatusCode));
                        Console.WriteLine(string.Format("ErrorInfo: {0}\n", response.GetErrorInfo()));
                        Console.WriteLine(response.GetData());
                        Console.WriteLine(string.Format("ErrorMessage: {0}\n", response.GetErrorMessage()));
                    }

                }

            }

        }


    }
}