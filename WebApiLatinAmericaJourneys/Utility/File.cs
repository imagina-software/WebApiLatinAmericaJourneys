﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace WebApiLatinAmericaJourneys.Utility
{
    public class File
    {

        public string GuardarImagenes(string imagen, int pNropedido, int pNroPropuesta, int pNroVersion) {

            string url_imagen = string.Empty;
            string directorioxUsuario = Convert.ToString(pNropedido) +"-"+ Convert.ToString(pNroPropuesta) + "-"+Convert.ToString(pNroVersion);

            string root = @"C:\Images-GOLAC";
            string subdir = @"C:\Images-GOLAC\" + directorioxUsuario;

            int longitud = 7;
            Guid miGuid = Guid.NewGuid();
            string token = Convert.ToBase64String(miGuid.ToByteArray());
            token = token.Replace("=", "").Replace("+", "").Replace("/","");

            try
            {

                string cleandata2= imagen.Replace("data:image/jpeg;base64,", "").Replace("data:image/jpg;base64,", "");

               
                    cleandata2 = cleandata2.Replace("data:image/png;base64,", "");
                

                var fileExtension = cleandata2.Substring(0, 5).ToUpper();

                // If directory does not exist, create it. 
                if (!Directory.Exists(root))
                {
                    Directory.CreateDirectory(root);
                }

                // Create a sub directory
                if (!Directory.Exists(subdir))
                {
                    Directory.CreateDirectory(subdir);
                }
                

                if (fileExtension.Equals(ConstantesWeb.STR_CODE_PNG))
                {

                    string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/images/" + directorioxUsuario);  //Create a Folder in your Root directory on your solution.

                    string fileName = token.Substring(0, longitud) + ".png";

                    string imagePath2 = ConfigurationManager.AppSettings["URL_PENTAGRAMA_IMG"] + directorioxUsuario + "/" + fileName;

                    string base64StringData = imagen; // Your base 64 string data

                    string cleandata = base64StringData.Replace("data:image/png;base64,", "");

                    byte[] data = System.Convert.FromBase64String(cleandata);

                    string imgPath = Path.Combine(folderPath, fileName);

                    System.IO.File.WriteAllBytes(imgPath, data);

                    url_imagen = imagePath2;

                }

                else {

                    if (fileExtension.Equals(ConstantesWeb.STR_CODE_JPG)) {

                        string folderPath = System.Web.HttpContext.Current.Server.MapPath("~/images/"+ directorioxUsuario);  //Create a Folder in your Root directory on your solution.

                        string fileName = token.Substring(0, longitud) + ".jpg";

                        string imagePath2 = ConfigurationManager.AppSettings["URL_PENTAGRAMA_IMG"] + directorioxUsuario + "/" + fileName;

                        string base64StringData = imagen; // Your base 64 string data

                        string cleandata = base64StringData.Replace("data:image/jpeg;base64,", "").Replace("data:image/jpg;base64,", "");

                        byte[] data = System.Convert.FromBase64String(cleandata);

                        string imgPath = Path.Combine(folderPath, fileName);

                        System.IO.File.WriteAllBytes(imgPath, data);

                        url_imagen = imagePath2;

                    }
                    else { 

                    
                    
                    }
                
                
                }
         

                return url_imagen;

            }
            catch (Exception ex) {

                return url_imagen = ex.ToString();
            
            
            }
       


        }


    }
}